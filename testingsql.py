import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Reflective;UID=reportinguser;PWD=SQLR3ports"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT [ReadTime], [BONumber], [L1_Thickness], [L2_Thickness] FROM RTM.[VGIS_Thickness] WHERE ReadTime > CONVERT(nvarchar, Dateadd(mi, -60, GetDate())) order by 
ReadTime;"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query),con=conn)
df.sort_values(by='BONumber')
df.to_csv('test_sqlalchemy.csv')

stop = timeit.default_timer()
print(stop-start)