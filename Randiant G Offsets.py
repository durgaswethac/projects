import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-PRDOLTP2;DATABASE=cammes1;UID=reportinguser;PWD=SQLR3ports"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT * FROM cammes.VW_lkupRadiantMateLiteOffsets """
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query),con=conn)
df.to_csv('Radiant G Offsets.csv')

stop = timeit.default_timer()
print(stop-start)