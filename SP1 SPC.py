import time
import pandas as pd
from data_pull import SP1Controls
from data_pull import pullConfig

A5 = 0
A2 = 0
SP_1 = pd.DataFrame()
while True:
    SP1 = SP1Controls()
    g = pullConfig()
    print(SP1)

    if abs(SP1.iloc[2]['Del_T2']) > 8.67:
        alarm = 1
        if SP1.iloc[2]['Del_T2'] < 0:
            if ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']) < -0.5:
                if (SP1.iloc[2]['Carrier Power'] + 0.5) > 88:
                    New_power = 88
                else:
                    New_power = SP1.iloc[2]['Carrier Power'] + 0.5
            elif -0.5 < ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']):
                if (SP1.iloc[2]['Carrier Power'] - (
                        (SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff'])) > 88:
                    New_power = 88
                else:
                    New_power = SP1.iloc[2]['Carrier Power'] - (
                                (SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff'])
        elif SP1.iloc[2]['Del_T2'] > 0:
            if ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']) > 0.5:
                if (SP1.iloc[2]['Carrier Power'] - 0.5) > 50:
                    New_power = 50
                else:
                    New_power = SP1.iloc[2]['Carrier Power'] - 0.5
            elif ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']) < 0.5:
                if (SP1.iloc[2]['Carrier Power'] - (
                        (SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff'])) < 50:
                    New_power = 50
                else:
                    New_power = SP1.iloc[2]['Carrier Power'] - (
                                (SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff'])
    elif abs(SP1.iloc[2]['Del_T2']) > 5:
        if SP1.iloc[2]['Del_T2'] < 0:
            A5 += -1
        elif SP1.iloc[2]['Del_T2'] > 0:
            A5 += 1
        if abs(A5) > 2:
            alarm = 5
            if SP1.iloc[2]['Del_T2'] < 0:
                if ((SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff']) < -0.5:
                    if (SP1.iloc[2]['Carrier Power'] + 0.5) > 88:
                        New_power = 88
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] + 0.5
                elif -0.5 < ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']):
                    if (SP1.iloc[2]['Carrier Power'] - (
                            (SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff'])) > 88:
                        New_power = 88
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - (
                                (SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff'])
            elif SP1.iloc[2]['Del_T2'] > 0:
                if ((SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff']) > 0.5:
                    if (SP1.iloc[2]['Carrier Power'] - 0.5) < 50:
                        New_power = 50
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - 0.5
                elif -0.5 < ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']):
                    if (SP1.iloc[2]['Carrier Power'] - (
                            (SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff'])) < 50:
                        New_power = 50
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - (
                                    (SP1.iloc[2]['Del_T2'] * 0.75) / SP1.iloc[2]['T2 Power Coeff'])
        else:
            alarm = 0
            New_power = 0
    elif SP1.iloc[2]['Del_T2'] != 0:
        if SP1.iloc[2]['Del_T2'] > 0:
            A2 += 1
        else:
            A2 += -1
        if abs(A2) > 7:
            alarm = 2
            if SP1.iloc[2]['Del_T2'] < 0:
                if ((SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff']) < -0.5:
                    if (SP1.iloc[2]['Carrier Power'] + 0.5) > 88:
                        New_power = 88
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] + 0.5
                elif -0.5 < ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']):
                    if (SP1.iloc[2]['Carrier Power'] - (
                            (SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff'])) > 88:
                        New_power = 88
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - (
                                    (SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff'])
            elif SP1.iloc[2]['Del_T2'] > 0:
                if ((SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff']) > 0.5:
                    if (SP1.iloc[2]['Carrier Power'] - 0.5) < 50:
                        New_power = 50
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - 0.5
                elif ((SP1.iloc[2]['Del_T2'] * 0.5) / SP1.iloc[2]['T2 Power Coeff']) < 0.5:
                    if (SP1.iloc[2]['Carrier Power'] - (
                            (SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff'])) < 50:
                        New_power = 50
                    else:
                        New_power = SP1.iloc[2]['Carrier Power'] - (
                                    (SP1.iloc[2]['Del_T2']) / SP1.iloc[2]['T2 Power Coeff'])
        else:
            alarm = 0
            New_power = 0
    else:
        alarm = 0
        New_power = 0
    if SP1.iloc[2]['Carrier Power'] == New_power:
        A5 = 0
        A2 = 0

    Alarmseries = [0,0,alarm]
    powerseries = [0, 0, New_power]
    SP1['Alarm'] = pd.Series(Alarmseries)
    SP1['New Power'] = pd.Series(powerseries)
    SP_1 = pd.concat([SP_1, SP1])
    SP_1.to_csv('SP1 controls result.csv')
    print(SP_1)
    time.sleep(600)
