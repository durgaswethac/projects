import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit

start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;UID=reportinguser;PWD=SQLR3ports;DATABASE=CoaterTrace"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT t1.SP_HTO, t1.SP_HTO_LOC, t1.[HTO_Zone], t1.[MeasureType], 
		t1.[Measure], t1.[Value], t2.[ID], t2.[LiteID], t2.[BONumber], 
		t2.[Source], t2.[GateTimer], t2.[ActualStart], t2.[CorrC1X], 
		t2.[CorrC2X], t2.[CorrC1Y], t2.[CorrC2Y], t2.[ActualStartCST]  
		FROM [dbo].EC01_HTO_FEEDBACK_LITE  t1 
		LEFT OUTER JOIN [dbo].HEADER_LITEID t2 
			ON  ( t2.ID = t1.[Header_LiteID_ID] ) 
			WHERE ActualStartCST > CONVERT(nvarchar, Dateadd(mm, -1, GetDate())) order 
by ActualStartCST"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query), con=conn)

df.to_csv('HTO_Based_ITO_Rs.csv')

stop = timeit.default_timer()
print(stop - start)
