import timeit

import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL

start = timeit.default_timer()


def CheckIG():
    connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Transmission"
    connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})
    engine = db.create_engine(connection_url)
    Query = """SELECT  [Factory_ID], [FactoryLine_ID], [Server_ID], [VGST_SID], [VGST_RID],[LiteID],
    cd.[vw_parentecid],cd.[vw_Litetype] as LiteIDAtTool,cd.[vw_lite2partnumber],cd.[vw_lite3partnumber],
    cdp.[vw_lite2partnumber] as ParentListedPart2Number,cdp.[vw_lite3partnumber] as ParentListedPart3Number, 
    case when cd.vw_litetype = 'Mate1' then cd.vw_Lite2partnumber when cd.vw_litetype = 'Mate2' then 
    cd.vw_lite3partnumber when cd.vw_litetype = 'Lami1' and cdp.vw_lite3partnumber is NULL then 
    cdp.vw_lite2partnumber when cd.vw_litetype = 'Lami2' then 'LamiChild-WrongLiteIDLoadedToPLC' else 'None' end as 
    OrderedArticleCode,[ArticleCode] as ToolArticleCode ,[ReadTime],[SerialNumber],[a*_Min] ,[a*_Max] ,[a*_Avg],
    [a*_StDev] ,[b*_Min] ,[b*_Max],[b*_Avg],[b*_StDev],[L*_Min],[L*_Max],[L*_Avg],[L*_StDev],[a*_Corrected_Min],
    [a*_Corrected_Max],[a*_Corrected_Avg],[a*_Corrected_StDev] ,[a*_Corrected_Samples],[b*_Corrected_Min] ,
    [b*_Corrected_Max],[b*_Corrected_Avg],[b*_Corrected_StDev] ,[b*_Corrected_Samples],[L*_Corrected_Min] ,
    [L*_Corrected_Max] ,[L*_Corrected_Avg] ,[L*_Corrected_StDev],[L*_Corrected_Samples] ,[L*_Med] ,[L*_Samples],
    [L*_Corrected_Med] ,[a*_Med] ,[a*_Samples],[a*_Corrected_Med],[b*_Med],[b*_Samples],[b*_Corrected_Med],
    [Result] FROM [Transmission].[fct].[Summary] s
    
      inner join [ob-vm-prdoltp2].cammes1.cammes.container c on s.liteid = c.containername
    
      inner join [ob-vm-prdoltp2].cammes1.cammes.containerdetail cd on c.containerid = cd.containerid
    
     left join [ob-vm-prdoltp2].cammes1.cammes.container cp on cd.vw_parentecid = cp.containername
    
      left join [ob-vm-prdoltp2].cammes1.cammes.containerdetail cdp on cp.containerid = cdp.containerid
    
    WHERE ReadTime >= CONVERT(nvarchar, Dateadd(mm, -6, GetDate())) AND  cdp.vw_lite3partnumber != [ArticleCode] AND cdp.vw_lite2partnumber != [ArticleCode]
    
     order by readtime desc"""
    with engine.begin() as conn:
        df = pd.read_sql_query(sql=text(Query), con=conn)

    df.to_csv('TCM results.csv')


CheckIG()
stop = timeit.default_timer()
print(stop - start)
