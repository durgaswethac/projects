import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit

start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Reflective;UID=reportinguser;PWD=SQLR3ports"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT [ServerName], [Angle], [LiteID], [Width], [Height], [Gen], [MaxDim], [Scans], [ActualStart], [MaxReadTime], 
	[TintState], [LtRdE], [MaxDE2], [A*Mid], [A*Avg], [A*Ref], [B*Mid], [B*Avg], [B*Ref], [L*Mid], [L*Avg], [L*Ref], 
	[MidDim], [MinDim], [Processed], [Status], [A*MinExcl], [A*MaxExcl], [B*MinExcl], [B*MaxExcl], 
	[L*MinExcl], [L*MaxExcl], [RefRev]
	FROM [dbo].[ReflectiveByLite_StartDate] WHERE MaxReadTime > CONVERT(nvarchar, Dateadd(dd, -1, GetDate())) order by 
MaxReadTime;"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query), con=conn)

df.to_csv('test.csv')

stop = timeit.default_timer()
print(stop - start)
