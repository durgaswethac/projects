"""
9/9/2020 Added try-catch blocks to the running of the program
"""


import os
import csv
import glob
import urllib
import urllib.request
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import peakutils
from datetime import datetime, timedelta
import datetime as dt
from functools import reduce
import six
import smtplib
import pyodbc
import os.path
from os import path
from functools import reduce
# For guessing MIME type
import mimetypes
#, "kaustubh.nadkarni@view.com","Animesh.Mondal@view.com"
# Import the email modules we'll need
import email
import email.mime.application
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
#import SQLAlchemy
from email.message import EmailMessage
from sqlalchemy import create_engine
import math


def drop_y(df):
    # list comprehension of the cols that end with '_y'
    to_drop = [x for x in df if x.endswith('_y')]
    df.drop(to_drop, axis=1, inplace=True)

def WonderTraceupdate(alarm, Error):
    Config = pd.read_csv(r'\\echromics\ob\General\PVD01\Overwatch\WonderTrace.csv')
    Config.loc[(Config['Function Name'] == 'WonderTrace'), 'Alarm'] = alarm
    Config.loc[(Config['Function Name'] == 'WonderTrace'), 'Error'] = Error
    Config.loc[(Config['Function Name'] == 'WonderTrace'), ' Time Last Ran'] = datetime.now()
    Noneya=Config.to_csv(r'\\echromics\ob\General\PVD01\Overwatch\WonderTrace.csv', index=False, header=True)


def LLBOPULL(CRO):
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    startDate = (CRO['ActualStart'].min() - timedelta(minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF
    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [EC01_LL_1110.BONumber]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 10000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['DateTime']=pd.to_datetime(Df['DateTime'])
    return Df


def supull(Config):
    startDate=(datetime.now() - timedelta(hours=6)).strftime("%m/%d/%Y %H:%M:%S")
    EndDate=(datetime.now()).strftime("%m/%d/%Y %H:%M:%S")
    Type = "Product"
    Query = f"""SELECT *
                FROM [cammes1].[cammes].[VM_JMP_ParallelCROLites]
                Where [ActualStart] >= '{startDate}'
                AND [ActualStart] <= '{EndDate}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-PRDOLTP2;"
                          "Trusted_Connection=yes;""DATABASE=cammes1;")
    df = pd.read_sql_query(Query, cnxn)
    #export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta.csv', header=True)
    cnxn.close()
    return df


def Croull(Config,df):

    #export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta.csv', header=True)
    ll=LLBOPULL(df)
    CRO=CroPull(df,ll,Config)
    LITES, LITES2 = LitePull2(df,ll,Config)
    list=[CRO,LITES, LITES2]
    return list


def Croull2(BBatch, EBatch):
    Type = "Product"
    Query = f"""SELECT 'BONumber', 'Source', 'LiteID', 'LiteWidth', 'LiteHeight', 'CarrierReleaseID', 'TestPlan', 'BOSequence', 'CarrierLayOut',  'Batch', 'ActualStart', 'Type', 'Status', 'LiteIdentity', 'LiteLoc', 'CarrierID', 'GateTimer', 'XPos'
                FROM [cammes1].[cammes].[VM_JMP_ParallelCROLites]
                Where [Batch] >= '{BBatch}'
                AND [Batch] <= '{EBatch}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-PRDOLTP2;"
                          "Trusted_Connection=yes;""DATABASE=cammes1;")
    df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    ll=LLBOPULL(df)
    CRO=CroPull(df,ll)
    LITES=LitePull2(df,ll)
    list=[CRO,LITES]
    return list



def CroPull(PULL,LL,Config):
    Df=PULL
    CRO = Df.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    if Takt == 210:
        Takt = 3.5
    elif Takt == 300:
        Takt = 5
    elif Takt == 240:
        Takt = 4
    LIST =["Batch","BOSequence","BONumber", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID", "TestPlan",
            "CarrierLayOut",  "ActualStart", "Type", "Status", "LiteIdentity", "LiteLoc", "CarrierID",
            "GateTimer"]
    Dfilter = Df.filter(LIST)
    Dfilter['CarrierLayout'] = Dfilter['CarrierLayOut']
    Dfilter['BONumber'] = Dfilter['BONumber'].astype(int)
    Dfilter['BOSequence'] = Dfilter['BOSequence'].astype(int)
    # Dfilter['Batch'] = Dfilter['Batch'].astype(int)
    Dfilter['ActualStart'] = pd.to_datetime(Dfilter['ActualStart'])
    list = ['ActualStart', 'LiteLoc', 'LiteID']
    DF = Dfilter.sort_values(list)
    DF = DF[DF.ActualStart.notnull()]
    DF = DF.drop_duplicates('LiteIdentity', keep='last')
    DF = DF.drop_duplicates('BONumber', keep='first')
    DF2=LL
    LIST2=["BONumber", "Source", "CarrierReleaseID", "TestPlan", "BOSequence", "CarrierLayOut", "Batch", "Status", "CarrierID", "GateTimer"]
    DF=DF.filter(LIST2)
    DF2.drop_duplicates(subset=['EC01_LL_1110.BONumber'], keep='first',inplace=True)
    DF2['ActualStart']=pd.to_datetime(DF2['DateTime'])
    List3=['EC01_LL_1110.BONumber','ActualStart']
    DF2=DF2.filter(List3)
    DF=DF.merge(DF2,how='left', left_on='BONumber', right_on='EC01_LL_1110.BONumber')
    DF['ActualFinish']= (pd.to_datetime(DF['ActualStart'] + timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP7timemidH'].values[0], minutes=(Config.loc[Config['Takt']==Takt, 'SP7timemidM'].values[0]+int(4*Takt)))))
    DF = DF[DF.ActualStart.notnull()]
    DF['GateTimer'] = DF['GateTimer'].astype(float)
    DF=DF.sort_values('ActualStart')
    #export_csv = DF.to_csv(r'\\echromics\ob\General\PVD01\Overwatch\CarrierPullerBeta.csv', header=True)
    df=DF
    List4=["BONumber", "Source", "CarrierReleaseID", "TestPlan", "BOSequence", "CarrierLayOut", "Batch", "Status", "CarrierID", "GateTimer", "ActualStart", "ActualFinish"]
    df['Now']=(datetime.now())
    df['SP1time'] = df['Now'] - timedelta(minutes=Config.loc[Config['Takt']==Takt, 'SP1timeexitM'].values[0])
    SP1 = df.loc[(df['ActualStart'] <= df['SP1time']) & (df['ActualStart'] > (df['SP1time'] - timedelta(minutes=30)))]
    SP1=SP1.filter(List4)
    df['SP2time'] = df['Now'] - timedelta(minutes=Config.loc[Config['Takt']==Takt, 'SP2timeexitM'].values[0])
    SP2 = df.loc[(df['ActualStart'] <= df['SP2time']) & (df['ActualStart'] > (df['SP2time'] - timedelta(minutes=30)))]
    SP2 = SP2.filter(List4)
    df['SP3time'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP3timeexitH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP3timeexitM'].values[0], seconds=Config.loc[Config['Takt']==Takt, 'SP3timeexitS'].values[0])
    SP3 = df.loc[(df['ActualStart'] <= df['SP3time']) & (df['ActualStart'] > (df['SP3time'] - timedelta(minutes=30)))]
    SP3 = SP3.filter(List4)
    df['SP4time'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP4timeexitH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP4timeexitM'].values[0])
    SP4 = df.loc[(df['ActualStart'] <= df['SP4time']) & (df['ActualStart'] > (df['SP4time'] - timedelta(minutes=30)))]
    SP4 = SP4.filter(List4)
    df['SP5time'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP5timeexitH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP5timeexitM'].values[0], seconds=Config.loc[Config['Takt']==Takt, 'SP5timeexitS'].values[0])
    SP5 = df.loc[(df['ActualStart'] <= df['SP5time']) & (df['ActualStart'] > (df['SP5time'] - timedelta(minutes=30)))]
    SP5 = SP5.filter(List4)
    df['SP6time'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP6timeexitH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP6timeexitM'].values[0])
    SP6 = df.loc[(df['ActualStart'] <= df['SP6time']) & (df['ActualStart'] > (df['SP6time'] - timedelta(minutes=30)))]
    SP6 = SP6.filter(List4)
    df['SP7time'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP7timemidH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP7timemidM'].values[0])
    SP7 = df.loc[(df['ActualStart'] <= df['SP7time']) & (df['ActualStart'] > (df['SP7time'] - timedelta(minutes=30)))]
    SP7 = SP7.filter(List4)
    ODSET = df.loc[(df['ActualFinish'] <= df['Now']) & (df['ActualFinish'] > (df['Now'] - timedelta(minutes=30)))]
    df['HTOtime'] = df['Now'] - timedelta(hours=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeH'].values[0],minutes=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeM'].values[0])
    HTO = df.loc[(df['ActualStart'] <= df['HTOtime']) & (df['ActualStart'] > (df['HTOtime'] - timedelta(minutes=30)))]
    HTO = HTO.filter(List4)
    #export_csv = ODSET.to_csv(r'C:\Users\arobinson\Desktop\ODSETBeta.csv', header=True)
    ODSET = ODSET.filter(List4)
    return [df,SP1,SP2,SP3,SP4,SP5,SP6,SP7,ODSET,HTO]

def LitePull(PULL, LL):
    df = PULL
    LIST = ["BONumber", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID", "TestPlan", "BOSequence", "CarrierLayOut",  "Batch", "ActualStart", "Type", "Status", "LiteIdentity", "LiteLoc", "CarrierID", "GateTimer", "XPos"]
    Dfilter = df.filter(LIST)
    Dfilter=Dfilter[Dfilter.LiteID!="0"]
    Dfilter['CarrierLayout'] = Dfilter['CarrierLayOut']
    Dfilter['BONumber'] = Dfilter['BONumber'].astype(int)
    Dfilter['BOSequence'] = Dfilter['BOSequence'].astype(int)
    # Dfilter['Batch'] = Dfilter['Batch'].astype(int)
    Dfilter['ActualStart'] = pd.to_datetime(Dfilter['ActualStart'])
    list = ['ActualStart', 'LiteLoc', 'LiteID']
    DF = Dfilter.sort_values(list)
    DF = DF[DF.ActualStart.notnull()]
    DF = DF[DF.Type != 'Mate1']
    DF = DF.drop_duplicates('LiteIdentity', keep='last')
    DF['C1X'] = DF['XPos'].astype(float)
    list=['ActualStart', 'C1X']
    DF = DF.sort_values(list)
    DF['RoundC1X'] = DF['C1X'].apply(np.floor)
    DF['BONumber'] = DF['BONumber'].astype(int)
    DF['IsChanged'] = (DF['RoundC1X'].diff() != 0).astype(int)
    #export_csv = DF.to_csv(r'C:\Users\arobinson\Desktop\LitePullerBeta2.csv', header=True)
    DF['CarrierColumn'] = (DF.groupby('ActualStart')['IsChanged'].cumsum()).astype(int)
    DF.loc[DF['CarrierColumn']==0,'CarrierColumn']=1
    DF.loc[DF['CarrierLayOut'] == "DC201", 'CarrierColumn'] = 2
    DF.loc[DF['CarrierLayOut'] == "DC184", 'CarrierColumn'] = 2
    DF.loc[DF['CarrierLayOut'] == "DC185", 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC186") & (DF['CarrierColumn'] == 2), 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC186") & (DF['CarrierColumn'] == 1), 'CarrierColumn'] = 2
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 3), 'CarrierColumn'] = 4
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 2), 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 1), 'CarrierColumn'] = 2
    DF['LiteID'] = DF['LiteID'].astype(str)
    DF['LiteID'] = DF['LiteID'].str.strip()
    DF2=LL
    LIST2 = ["BONumber", "CarrierColumn", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID", "TestPlan",
             "BOSequence", "CarrierLayOut", "Batch", "Type", "Status", "LiteIdentity", "LiteLoc", "CarrierID",
            "GateTimer", "XPos"]

    DF = DF.filter(LIST2)
    DF2.drop_duplicates(subset=['EC01_LL_1110.BONumber'], keep='first', inplace=True)
    DF2['ActualStart'] = pd.to_datetime(DF2['DateTime'])
    List3=['EC01_LL_1110.BONumber','ActualStart']
    DF2=DF2.filter(List3)
    DF = DF.merge(DF2, how='left', left_on='BONumber', right_on='EC01_LL_1110.BONumber')
    DF['ActualFinish'] = (pd.to_datetime(DF['ActualStart'] + timedelta(hours=3))).dt.strftime("%m/%d/%Y %H:%M:%S")
    DF = DF[DF.ActualStart.notnull()]
    DF = DF.sort_values('ActualStart')
    #export_csv = DF.to_csv(r'C:\Users\arobinson\Desktop\LitePullerBeta.csv', header=True)
    DF=DF.drop_duplicates(subset=['LiteID'])
    #print(Df.head())
    LIST3 = ["BONumber", "CarrierColumn", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID",
             "TestPlan",
             "BOSequence", "CarrierLayOut", "Batch", "Type", "Status", "LiteIdentity", "LiteLoc",
             "CarrierID",
             "GateTimer", "XPos", "ActualStart"]
    DF['Now'] = (datetime.now())
    DF['ActualFinish'] = (pd.to_datetime(DF['ActualStart'] + timedelta(hours=3)))
    DF = DF.loc[(DF['ActualFinish'] < DF['Now']) & (DF['ActualFinish'] >= (DF['Now'] - timedelta(minutes=30)))]
    DF = DF.filter(LIST3)
    return DF

def LitePull2(PULL, LL, Config):
    Df = PULL
    CRO = Df.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    if Takt == 210:
        Takt = 3.5
    elif Takt == 300:
        Takt = 5
    elif Takt == 240:
        Takt = 4
    EndBo = Df['BONumber'].max()
    StartBo = Df['BONumber'].min()
    Query = f"""SELECT * FROM dbo.lkupCarrierLiteCorrections
    WHERE dbo.lkupCarrierLiteCorrections.BoNumber >= '{StartBo}'
    AND dbo.lkupCarrierLiteCorrections.BoNumber <= '{EndBo}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLCL1D;"
                          "Trusted_Connection=yes;""DATABASE=OB-WIP;")
    df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    df=df.filter(['LiteID', 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y'])
    df['LiteID']=df['LiteID'].astype(str)
    df['LiteID'] = df['LiteID'].str.strip()
    LIST = ["BONumber", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID", "TestPlan", "BOSequence", "CarrierLayOut",  "Batch", "ActualStart", "Type", "Status", "LiteIdentity", "LiteLoc", "CarrierID", "GateTimer"]
    Dfilter = Df.filter(LIST)
    Dfilter=Dfilter[Dfilter.LiteID!="0"]
    Dfilter['CarrierLayout'] = Dfilter['CarrierLayOut']
    Dfilter['BONumber'] = Dfilter['BONumber'].astype(int)
    Dfilter['BOSequence'] = Dfilter['BOSequence'].astype(int)
    # Dfilter['Batch'] = Dfilter['Batch'].astype(int)
    Dfilter['ActualStart'] = pd.to_datetime(Dfilter['ActualStart'])
    list = ['ActualStart', 'LiteLoc', 'LiteID']
    DF = Dfilter.sort_values(list)
    DF = DF[DF.ActualStart.notnull()]
    DF = DF[DF.Type != 'Mate1']
    DF = DF.drop_duplicates('LiteIdentity', keep='last')
    DF['LiteID'] = DF['LiteID'].astype(str)
    DF['LiteID'] = DF['LiteID'].str.strip()
    DF=DF.merge(df, on='LiteID')
    DF['C1X'] = DF['CorrC1X'].astype(float)
    list=['ActualStart', 'C1X']
    DF = DF.sort_values(list)
    DF['RoundC1X'] = DF['C1X'].apply(np.floor)
    DF['BONumber'] = DF['BONumber'].astype(int)
    DF['IsChanged'] = (DF['RoundC1X'].diff() != 0).astype(int)
    #export_csv = DF.to_csv(r'C:\Users\arobinson\Desktop\LitePullerBeta2.csv', header=True)
    DF['CarrierColumn'] = (DF.groupby('ActualStart')['IsChanged'].cumsum()).astype(int)
    DF.loc[DF['CarrierColumn']==0,'CarrierColumn']=1
    DF.loc[(DF['CarrierLayOut'] == "DC201"), 'CarrierColumn'] = 2
    DF.loc[(DF['CarrierLayOut'] == "DC184"), 'CarrierColumn'] = 2
    DF.loc[(DF['CarrierLayOut'] == "DC185"), 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC186") & (DF['CarrierColumn'] == 2), 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC186") & (DF['CarrierColumn'] == 1), 'CarrierColumn'] = 2
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 3), 'CarrierColumn'] = 4
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 2), 'CarrierColumn'] = 3
    DF.loc[(DF['CarrierLayOut'] == "DC200") & (DF['CarrierColumn'] == 1), 'CarrierColumn'] = 2
    DF2=LL
    LIST2 = ["BONumber", "CarrierColumn", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID", "TestPlan",
             "BOSequence", "CarrierLayOut", "Batch", "Type", "Status", "LiteIdentity", "LiteLoc", "CarrierID",
            "GateTimer", 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y']

    DF = DF.filter(LIST2)
    DF2.drop_duplicates(subset=['EC01_LL_1110.BONumber'], keep='first', inplace=True)
    DF2['ActualStart'] = pd.to_datetime(DF2['DateTime'])
    List3=['EC01_LL_1110.BONumber','ActualStart']
    DF2=DF2.filter(List3)
    DF = DF.merge(DF2, how='left', left_on='BONumber', right_on='EC01_LL_1110.BONumber')
    DF['ActualFinish'] = (pd.to_datetime(DF['ActualStart'] + timedelta(hours=3))).dt.strftime("%m/%d/%Y %H:%M:%S")
    DF = DF[DF.ActualStart.notnull()]
    DF = DF.sort_values('ActualStart')
    #export_csv = DF.to_csv(r'C:\Users\arobinson\Desktop\LitePullerBeta.csv', header=True)
    DF=DF.drop_duplicates(subset=['LiteID'])
    #print(Df.head())
    LIST3 = ["BONumber", "CarrierColumn", "Source", "LiteID", "LiteWidth", "LiteHeight", "CarrierReleaseID",
             "TestPlan",
             "BOSequence", "CarrierLayOut", "Batch", "Type", "Status", "LiteIdentity", "LiteLoc",
             "CarrierID",
             "GateTimer", 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y', "ActualStart"]
    DF['Now'] = (datetime.now())
    DF['ActualFinish'] = (pd.to_datetime(DF['ActualStart'] + timedelta(hours=(Config.loc[Config['Takt']==Takt, 'SP7timemidH'].values[0]), minutes=(Config.loc[Config['Takt']==Takt, 'SP7timemidM'].values[0]+int(4*Takt)))))
    DF3 = DF.loc[(DF['ActualFinish'] < DF['Now']-timedelta(minutes=90)) & (DF['ActualFinish'] >= (DF['Now'] - timedelta(minutes=120)))]
    #DF3 = DF
    DF = DF.loc[(DF['ActualFinish'] < DF['Now']) & (DF['ActualFinish'] >= (DF['Now'] - timedelta(minutes=30)))]
    DF = DF.filter(LIST3)
    DF3 = DF3.filter(LIST3)
    #export_csv = DF.to_csv(r'C:\Users\arobinson\Desktop\LitePosBeta2.csv', header=True)
    return DF,DF3


def LitePos(BBatch, EBatch):
    Query = f"""SELECT * FROM dbo.lkupCarrierLiteCorrections
    WHERE dbo.lkupCarrierLiteCorrections.BoNumber >= '{BBatch}'
    AND dbo.lkupCarrierLiteCorrections.BoNumber <= '{EBatch}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLCL1D;"
                          "Trusted_Connection=yes;""DATABASE=OB-WIP;")
    df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    #df['ActualStart'] = df['ActualStart'].astype('datetime64[ns]')
    #df['ActualFinish'] = df['ActualFinish'].astype('datetime64[ns]')
    Df=df.filter(['LiteID', 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y'])
    Df['LiteID']=Df['LiteID'].astype(str)
    Df['LiteID'] = Df['LiteID'].str.strip()
    Df.dtypes
    #export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\LitePosBeta.csv', header=True)
    #print(Df.head())
    return Df

def UpdateCro(Cro2, RecipeId):
    LIST4 = ["BONumber", "Source", "CarrierReleaseID", "TestPlan", "BOSequence", "CarrierLayOut", "Batch",
             "CarrierID", "GateTimer", "ActualStart"]
    Cro= Cro2.filter(LIST4)
    Cro['Recipe_ID'] = RecipeId
    startDate = (Cro['BONumber'].min())
    EndDate = (Cro['BONumber'].max())
    Query = f"""SELECT [BONumber], CONVERT(datetime2, [ActualStart], 1) as ActualStart 
                FROM [dbo].HEADER_BONUMBER
                WHERE [BONumber] >= '{startDate}'
                AND [BONumber] <= '{EndDate}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
    df = pd.read_sql_query(Query, cnxn)
    #export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta.csv', header=True)
    cnxn.close()
    Cro=Cro.loc[~((Cro.BONumber.isin(df['BONumber']))),:]
    Cro['ActualStart']=pd.to_datetime(Cro['ActualStart'])
    Cro.ActualStart = Cro.ActualStart.dt.tz_localize('US/Central').dt.tz_convert('UTC')
    #export_csv = Cro.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
    params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
    engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    Cro.to_sql('HEADER_BONUMBER', con=engine, if_exists='append', index=False)


def UpdateLITES(LITES2, RecipeID):
    LIST3 = ["BONumber", "CarrierColumn", "Source", "LiteID", "CarrierReleaseID",
             "TestPlan",
             "BOSequence", "CarrierLayOut", "Batch", "LiteIdentity", "LiteLoc",
             "CarrierID",
             "GateTimer", 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y', "ActualStart"]

    LITES = LITES2.filter(LIST3)
    LITES['Recipe_ID'] = RecipeID
    startDate = (LITES['BONumber'].min())
    EndDate = (LITES['BONumber'].max())
    Query = f"""SELECT [LiteID], CONVERT(datetime2, [ActualStart], 1)  as ActualStart 
                   FROM [dbo].HEADER_LITEID
                   WHERE [BONumber] >= '{startDate}'
                   AND [BONumber] <= '{EndDate}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
    df = pd.read_sql_query(Query, cnxn)
    #export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta.csv', header=True)
    cnxn.close()
    LITES = LITES.loc[~((LITES.LiteID.isin(df['LiteID']))), :]
    LITES['ActualStart']=pd.to_datetime(LITES['ActualStart'])
    #export_csv = LITES.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
    LITES.ActualStart = LITES.ActualStart.dt.tz_localize('US/Central').dt.tz_convert('UTC')
    params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
    engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    LITES.to_sql('HEADER_LITEID', con=engine, if_exists='append', index=False)
    #export_csv = LITES.to_csv(r'C:\Users\arobinson\Desktop\LITESUpload.csv', header=True, index=False)


def superlistdir(path):
    list1=os.listdir(path)
    list2=[]
    for i in list1:
        list2.append(os.path.join(path,i))
    return list2


def UpdateTimes(Cro):
    Config = pd.read_csv(r'\\echromics\ob\General\PVD01\Overwatch\CoaterToLite.csv')
    Config = Config.astype(float)
    items_counts = Cro['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    FilterMedia = ['BONumber', 'ActualStart']
    Times = Cro.filter(FilterMedia)
    Times.ActualStart = Times.ActualStart.dt.tz_localize('US/Central').dt.tz_convert('UTC')
    startDate = (Times['ActualStart'].min() - timedelta(hours = 6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (Times['ActualStart'].max() + timedelta(hours = 6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    Query = f"""SELECT ID, [BONumber]
                FROM [dbo].HEADER_BONUMBER
                WHERE [ActualStart] >= '{startDate}'
                AND [ActualStart] <= '{EndDate}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
    df = pd.read_sql_query(Query, cnxn)

    cnxn.close()

    Times = Times.merge(df, on='BONumber', how='inner', suffixes=('', '_y'))
    Times = Times.dropna(subset=['ID'])

    drop_y(Times)

    Times = Times.rename({'ID': 'Header_BoNumber_ID'}, axis='columns')
    if len(Times.index) > 1:
        startID = Times['Header_BoNumber_ID'].min()
        EndID = Times['Header_BoNumber_ID'].max()
        Query = f"""SELECT  [Header_BoNumber_ID]
                    FROM [dbo].EC01_HTO_FEEDBACK_TIMES
                    WHERE [Header_BoNumber_ID] >= '{startID}'
                    AND [Header_BoNumber_ID] <= '{EndID}';"""
        cnxn = pyodbc.connect("Driver={SQL Server};"
                              "Server=OB-VM-SQLDW02;"
                              "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
        df2 = pd.read_sql_query(Query, cnxn)
        Times = Times.loc[~((Times.Header_BoNumber_ID.isin(df2['Header_BoNumber_ID']))), :]

        Times['SP1_Start'] = Times['ActualStart'] + timedelta(
            minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeentryM'].values[0])
        Times['SP1_End'] = Times['SP1_Start'] + timedelta(minutes=(int(2.5 * Takt)))
        Times['SP2_Start'] = Times['ActualStart'] + timedelta(
            minutes=Config.loc[Config['Takt'] == Takt, 'SP2timeentryM'].values[0])
        Times['SP2_End'] = Times['SP2_Start'] + timedelta(minutes=(int(1 * Takt)))
        Times['SP3_Start'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'SP3timeentryH'].values[0],
            minutes=Config.loc[Config['Takt'] == Takt, 'SP3timeentryM'].values[0],
            seconds=Config.loc[Config['Takt'] == Takt, 'SP3timeentryS'].values[0])
        Times['SP3_End'] = Times['SP3_Start'] + timedelta(minutes=(int(1 * Takt)))
        Times['SP4_Start'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'SP4timeentryH'].values[0],
            minutes=Config.loc[Config['Takt'] == Takt, 'SP4timeentryM'].values[0])
        Times['SP4_End'] = Times['SP4_Start'] + timedelta(minutes=(int(2.5 * Takt)))
        Times['SP5_Start'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'SP5timeentryH'].values[0],
            minutes=Config.loc[Config['Takt'] == Takt, 'SP5timeentryM'].values[0],
            seconds=Config.loc[Config['Takt'] == Takt, 'SP5timeentryS'].values[0])
        Times['SP5_End'] = Times['SP5_Start'] + timedelta(minutes=(int(1.5 * Takt)))
        Times['SP6_Start'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'SP6timeentryH'].values[0],
            minutes=Config.loc[Config['Takt'] == Takt, 'SP6timeentryM'].values[0])
        Times['SP6_End'] = Times['SP6_Start'] + timedelta(minutes=(int(1 * Takt)))
        Times['SP7_Start'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'SP7timeexitH'].values[0],
            minutes=(Config.loc[Config['Takt'] == Takt, 'SP7timeexitM'].values[0] + int(2 * Takt)))
        Times['SP7_End'] = Times['SP7_Start'] + timedelta(minutes=(int(1.5 * Takt)))

        Times['HTO2_Zone_4_End'] = Times['ActualStart'] + timedelta(
            hours=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeH'].values[0],
            minutes=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeM'].values[0])
        Times['HTO1_Zone_1_Start'] = Times['HTO2_Zone_4_End'] - timedelta(hours=1, minutes=30)
        Times['HTO1_Zone_1_End'] = Times['HTO2_Zone_4_End'] - timedelta(hours=1, minutes=15)
        Times['HTO1_Zone_2_Start'] = Times['HTO2_Zone_4_End'] - timedelta(hours=1, minutes=15)
        Times['HTO1_Zone_2_End'] = Times['HTO2_Zone_4_End'] - timedelta(hours=1)
        Times['HTO1_Zone_3_Start'] = Times['HTO2_Zone_4_End'] - timedelta(hours=1)
        Times['HTO1_Zone_3_End'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=45)
        Times['HTO2_Zone_1_Start'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=45)
        Times['HTO2_Zone_1_End'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=33, seconds=45)
        Times['HTO2_Zone_2_Start'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=33, seconds=45)
        Times['HTO2_Zone_2_End'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=22, seconds=30)
        Times['HTO2_Zone_3_Start'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=22, seconds=30)
        Times['HTO2_Zone_3_End'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=11, seconds=15)
        Times['HTO2_Zone_4_Start'] = Times['HTO2_Zone_4_End'] - timedelta(minutes=11, seconds=15)
        Times = Times.drop(['ActualStart', 'BONumber'], axis=1)

        params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
        engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
        Times.to_sql('EC01_HTO_FEEDBACK_TIMES', con=engine, if_exists='append', index=False)
        #export_csv = Times.to_csv(r'C:\Users\arobinson\Desktop\TIMESUpload.csv', header=True, index=False)


def UpdateFeedback(Df, ZoneNumber, SPHTO):
    ListofCols = Df.columns
    Keeper=['BONumber']
    Pressure = [x for x in ListofCols if "Pressure" in x]
    Power = [x for x in ListofCols if "Power" in x]
    Voltage = [x for x in ListofCols if "Volt" in x]
    Frga = [x for x in ListofCols if "FRGA" in x]
    Torque = [x for x in ListofCols if "Torque" in x]
    Speed = [x for x in ListofCols if "Speed" in x]
    MFC = [x for x in ListofCols if "MFC" in x]
    TMP = [x for x in ListofCols if "TMP" in x]
    Temperature = [x for x in ListofCols if "Temp" in x]
    FilterMedia=Keeper+Pressure+Power+Voltage+Frga+Torque+Speed+MFC+TMP+Temperature
    PivotList=Pressure+Power+Voltage+Frga+Torque+Speed+MFC+TMP+Temperature
    Df=Df.filter(FilterMedia)
    Beginning = pd.melt(Df, id_vars=['BONumber'], value_vars=PivotList, var_name='Measure', value_name='Value')
    Beginning['SP_HTO']=SPHTO
    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta7.csv', header=True)
    if SPHTO=="SP":
        Beginning['SP_HTO_LOC']=ZoneNumber
    else:
        Beginning.loc[Beginning['Measure'].str.contains("HTO01"), 'SP_HTO_LOC'] = 1
        Beginning.loc[Beginning['Measure'].str.contains("HTO02"), 'SP_HTO_LOC'] = 2
        Beginning.loc[Beginning['Measure'].str.contains("Zone1"), 'HTO_Zone'] = 1
        Beginning.loc[Beginning['Measure'].str.contains("Zone2"), 'HTO_Zone'] = 2
        Beginning.loc[Beginning['Measure'].str.contains("Zone3"), 'HTO_Zone'] = 3
        Beginning.loc[Beginning['Measure'].str.contains("Zone4"), 'HTO_Zone'] = 4

    Beginning.loc[Beginning['Measure'].str.contains("Pressure"), 'MeasureType'] = "Pressure"
    Beginning.loc[Beginning['Measure'].str.contains("Power"), 'MeasureType'] = "Power"
    Beginning.loc[Beginning['Measure'].str.contains("Volt"), 'MeasureType'] = "Volt"
    Beginning.loc[Beginning['Measure'].str.contains("FRGA"), 'MeasureType'] = "FRGA"
    Beginning.loc[Beginning['Measure'].str.contains("Torque"), 'MeasureType'] = "Torque"
    Beginning.loc[Beginning['Measure'].str.contains("Speed"), 'MeasureType'] = "Cathode Speed"
    Beginning.loc[Beginning['Measure'].str.contains("MFC"), 'MeasureType'] = "MFC"
    Beginning.loc[Beginning['Measure'].str.contains("TMP"), 'MeasureType'] = "TMP"
    Beginning.loc[Beginning['Measure'].str.contains("Temp"), 'MeasureType'] = "Temp"

    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta0.csv', header=True)


    startBO = Beginning['BONumber'].min()
    EndBO = Beginning['BONumber'].max()
    Query = f"""SELECT ID, [BONumber]
                FROM [dbo].HEADER_BONUMBER
                WHERE [BONumber] >= '{startBO}'
                AND [BONumber] <= '{EndBO}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
    df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Beginning = Beginning.merge(df, on='BONumber', how='inner', suffixes=('', '_y'))
    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta1.csv', header=True)
    Beginning= Beginning.dropna(subset=['ID'])
    drop_y(Beginning)
    Beginning = Beginning.drop('BONumber', axis=1)
    Beginning = Beginning.rename({'ID': 'Header_BoNumber_ID'}, axis='columns')
    if len(Beginning.index)>1:
        startID = Beginning['Header_BoNumber_ID'].min()
        EndID = Beginning['Header_BoNumber_ID'].max()
        Query = f"""SELECT  [Header_BoNumber_ID],[Measure]
                       FROM [dbo].EC01_HTO_FEEDBACK

                       WHERE [Header_BoNumber_ID] >= '{startID}'
                       AND [Header_BoNumber_ID] <= '{EndID}';"""
        cnxn = pyodbc.connect("Driver={SQL Server};"
                              "Server=OB-VM-SQLDW02;"
                              "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
        df2 = pd.read_sql_query(Query, cnxn)
        #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
        Beginning = Beginning.loc[~((Beginning.Header_BoNumber_ID.isin(df2['Header_BoNumber_ID'])) & (Beginning.loc[(Beginning.Header_BoNumber_ID.isin(df2['Header_BoNumber_ID'])), :].Measure.isin(df2['Measure']))), :]
        #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta10.csv', header=True)
        params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
        engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
        Beginning = Beginning.round({'Value': 12})
        Beginning=Beginning.loc[Beginning['Value'] <= 99999999.999999999999]
        Beginning.to_sql('EC01_HTO_FEEDBACK', con=engine, if_exists='append', index=False)


def UpdateODs(ODs, Zone, Wavelength):
    if len(ODs.index) > 0:
        items_counts = ODs['GateTimer'].value_counts()
        Takt = items_counts.idxmax()
        if Takt == 210:
            Takt = 3.5
        elif Takt == 300:
            Takt = 5
        elif Takt == 240:
            Takt = 4
        ListofCols = ODs.columns
        LiteID = ['LiteID']
        Y1_Mean = [x for x in ListofCols if "meanY1" in x]
        Y1_StdDev = [x for x in ListofCols if "stdevY1" in x]
        Y2_Mean = [x for x in ListofCols if "meanY2" in x]
        Y2_StdDev = [x for x in ListofCols if "stdevY2" in x]
        Y3_Mean = [x for x in ListofCols if "meanY3" in x]
        Y3_StdDev = [x for x in ListofCols if "stdevY3" in x]
        Y4_Mean = [x for x in ListofCols if "meanY4" in x]
        Y4_StdDev = [x for x in ListofCols if "stdevY4" in x]
        Y5_Mean = [x for x in ListofCols if "meanY5" in x]
        Y5_StdDev = [x for x in ListofCols if "stdevY5" in x]
        Y6_Mean = [x for x in ListofCols if "meanY6" in x]
        Y6_StdDev = [x for x in ListofCols if "stdevY6" in x]
        Y7_Mean = [x for x in ListofCols if "meanY7" in x]
        Y7_StdDev = [x for x in ListofCols if "stdevY7" in x]
        OD_Mean = [x for x in ListofCols if "OD Mean" in x]
        StartTime = [x for x in ListofCols if "Measurement Start Time" in x]
        EndTime = [x for x in ListofCols if "Measurement End Time" in x]
        FilterMedia = LiteID + Y1_Mean + Y1_StdDev + Y2_Mean + Y2_StdDev + Y3_Mean + Y3_StdDev + Y4_Mean + Y4_StdDev + Y5_Mean + Y5_StdDev + Y6_Mean + Y6_StdDev + Y7_Mean + Y7_StdDev + OD_Mean + StartTime + EndTime
        FilterMedia.append('ActualStart')
        NewDF = ODs.filter(FilterMedia)
        NewDF['ActualStart'] = pd.to_datetime(NewDF['ActualStart'])
        Fillin = [Y1_Mean, Y1_StdDev, Y2_Mean, Y2_StdDev, Y3_Mean, Y3_StdDev, Y4_Mean, Y4_StdDev, Y5_Mean, Y5_StdDev,
                  Y6_Mean, Y6_StdDev, Y7_Mean, Y7_StdDev]
        ColumnNames = ["Y1_Mean", "Y1_StdDev", "Y2_Mean", "Y2_StdDev", "Y3_Mean", "Y3_StdDev", "Y4_Mean", "Y4_StdDev",
                       "Y5_Mean", "Y5_StdDev", "Y6_Mean", "Y6_StdDev", "Y7_Mean", "Y7_StdDev"]
        index = 0
        for x in Fillin:
            if len(x) == 0:
                NewDF[ColumnNames[index]] = 0
            else:
                NewDF.rename({x[0]: ColumnNames[index]}, axis='columns', inplace=True)

            index += 1
        NewDF.rename({StartTime[0]: 'StartTime', EndTime[0]: 'EndTime', OD_Mean[0]: 'OD_Mean'}, axis='columns',
                     inplace=True)
        NewDF['StartTime'] = pd.to_datetime(NewDF['StartTime'])
        NewDF['EndTime'] = pd.to_datetime(NewDF['EndTime'])
        NewDF.fillna(0, inplace=True)
        # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer.csv', header=True, index=False)
        NewDF['SP'] = Zone
        NewDF = NewDF.loc[~(NewDF['StartTime'] == 0)]
        if len(NewDF.index) > 1:
            NewDF['Distance'] = (NewDF['EndTime'] - NewDF['StartTime']).dt.seconds
            NewDF['Distance'] = NewDF['Distance'] * (3451 / (Takt * 60 * 25.4))
            NewDF.StartTime = NewDF.StartTime.dt.tz_localize('US/Central').dt.tz_convert('UTC')
            NewDF.EndTime = NewDF.EndTime.dt.tz_localize('US/Central').dt.tz_convert('UTC')
            NewDF['Wavelength'] = Wavelength
            # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer2.csv', header=True, index=False)

            startDate = (NewDF['ActualStart'].min() - timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
            EndDate = (NewDF['ActualStart'].max() + timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
            Query = f"""SELECT ID, [LiteID]
                        FROM [dbo].HEADER_LITEID
                        WHERE [ActualStart] >= '{startDate}'
                        AND [ActualStart] <= '{EndDate}';"""
            cnxn = pyodbc.connect("Driver={SQL Server};"
                                  "Server=OB-VM-SQLDW02;"
                                  "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
            df = pd.read_sql_query(Query, cnxn)

            cnxn.close()

            NewDF = NewDF.merge(df, on='LiteID', how='inner', suffixes=('', '_y'))
            NewDF = NewDF.dropna(subset=['ID'])
            drop_y(NewDF)
            NewDF.rename({'ID': 'Header_LiteID_ID'}, axis='columns', inplace=True)
            # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer3.csv', header=True, index=False)
            if len(NewDF.index) > 1:
                startID = NewDF['Header_LiteID_ID'].min()
                EndID = NewDF['Header_LiteID_ID'].max()
                Query = f"""SELECT  [Header_LiteID_ID],[SP],[Wavelength],[Distance]
                               FROM [dbo].EC01_OD_TRACE

                               WHERE [Header_LiteID_ID] >= '{startID}'
                               AND [Header_LiteID_ID] <= '{EndID}';"""
                cnxn = pyodbc.connect("Driver={SQL Server};"
                                      "Server=OB-VM-SQLDW02;"
                                      "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
                df2 = pd.read_sql_query(Query, cnxn)
                # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
                NewDF = NewDF.merge(df2, how='left', on=['Header_LiteID_ID', 'SP', 'Wavelength'], suffixes=('', '_y'))
                # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta9.csv', header=True)
                NewDF = NewDF.loc[NewDF['Distance_y'].isnull()]
                # export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta10.csv', header=True)
                drop_y(NewDF)
                params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
                engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
                NewDF.drop(['LiteID', 'ActualStart'], axis=1, inplace=True)
                NewDF.to_sql('EC01_OD_TRACE', con=engine, if_exists='append', index=False)


def UpdatePYROs(ODs, Zone, LOC_ID):
    if len(ODs.index) > 0:
        items_counts = ODs['GateTimer'].value_counts()
        Takt = items_counts.idxmax()
        if Takt == 210:
            Takt = 3.5
        elif Takt == 300:
            Takt = 5
        elif Takt == 240:
            Takt = 4
        ListofCols = ODs.columns
        LiteID = ['BONumber', 'CarrierColumn']
        Y1_Mean = [x for x in ListofCols if "Y1 mean" in x]
        Y1_StdDev = [x for x in ListofCols if "Y1 stdev" in x]
        Y2_Mean = [x for x in ListofCols if "Y2 mean" in x]
        Y2_StdDev = [x for x in ListofCols if "Y2 stdev" in x]
        Y3_Mean = [x for x in ListofCols if "Y3 mean" in x]
        Y3_StdDev = [x for x in ListofCols if "Y3 stdev" in x]
        Y4_Mean = [x for x in ListofCols if "Y4 mean" in x]
        Y4_StdDev = [x for x in ListofCols if "Y4 stdev" in x]
        Y5_Mean = [x for x in ListofCols if "Y5 mean" in x]
        Y5_StdDev = [x for x in ListofCols if "Y5 stdev" in x]
        Y6_Mean = [x for x in ListofCols if "Y6 mean" in x]
        Y6_StdDev = [x for x in ListofCols if "Y6 stdev" in x]
        Y7_Mean = [x for x in ListofCols if "Y7 mean" in x]
        Y7_StdDev = [x for x in ListofCols if "Y7 stdev" in x]
        StartTime = [x for x in ListofCols if "Measurement Start Time" in x]
        EndTime = [x for x in ListofCols if "Measurement End Time" in x]
        FilterMedia = LiteID + Y1_Mean + Y1_StdDev + Y2_Mean + Y2_StdDev + Y3_Mean + Y3_StdDev + Y4_Mean + Y4_StdDev + Y5_Mean + Y5_StdDev + Y6_Mean + Y6_StdDev + Y7_Mean + Y7_StdDev + StartTime + EndTime
        FilterMedia.append('ActualStart')
        NewDF = ODs.filter(FilterMedia)
        NewDF['ActualStart'] = pd.to_datetime(NewDF['ActualStart'])
        Fillin = [Y1_Mean, Y1_StdDev, Y2_Mean, Y2_StdDev, Y3_Mean, Y3_StdDev, Y4_Mean, Y4_StdDev, Y5_Mean, Y5_StdDev,
                  Y6_Mean, Y6_StdDev, Y7_Mean, Y7_StdDev]
        ColumnNames = ["Y1_Mean", "Y1_StdDev", "Y2_Mean", "Y2_StdDev", "Y3_Mean", "Y3_StdDev", "Y4_Mean", "Y4_StdDev",
                       "Y5_Mean", "Y5_StdDev", "Y6_Mean", "Y6_StdDev", "Y7_Mean", "Y7_StdDev"]
        index = 0
        for x in Fillin:
            if len(x) == 0:
                NewDF[ColumnNames[index]] = 0
            else:
                NewDF.rename({x[0]: ColumnNames[index]}, axis='columns', inplace=True)

            index += 1
        NewDF.rename({StartTime[0]: 'StartTime', EndTime[0]: 'EndTime'}, axis='columns',
                     inplace=True)
        NewDF['StartTime'] = pd.to_datetime(NewDF['StartTime'])
        NewDF['EndTime'] = pd.to_datetime(NewDF['EndTime'])
        NewDF.fillna(0, inplace=True)
        #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer.csv', header=True, index=False)
        NewDF['SP'] = Zone
        NewDF['Distance'] = (NewDF['EndTime'] - NewDF['StartTime']).dt.seconds
        NewDF['Distance'] = NewDF['Distance'] * (3451 / (Takt * 60 * 25.4))
        NewDF.StartTime = NewDF.StartTime.dt.tz_localize('US/Central').dt.tz_convert('UTC')
        NewDF.EndTime = NewDF.EndTime.dt.tz_localize('US/Central').dt.tz_convert('UTC')
        NewDF['LOC_ID'] = LOC_ID
        #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer2.csv', header=True, index=False)

        startDate = (NewDF['ActualStart'].min() - timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
        EndDate = (NewDF['ActualStart'].max() + timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
        Query = f"""SELECT ID, [LiteID], [BONumber], [CarrierColumn]
                    FROM [dbo].HEADER_LITEID
                    WHERE [ActualStart] >= '{startDate}'
                    AND [ActualStart] <= '{EndDate}';"""
        cnxn = pyodbc.connect("Driver={SQL Server};"
                              "Server=OB-VM-SQLDW02;"
                              "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
        df = pd.read_sql_query(Query, cnxn)

        cnxn.close()

        NewDF = NewDF.merge(df, on=['BONumber', 'CarrierColumn'], how='inner', suffixes=('', '_y'))
        NewDF = NewDF.dropna(subset=['ID'])
        drop_y(NewDF)
        NewDF.rename({'ID': 'Header_LiteID_ID'}, axis='columns', inplace=True)
        #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\ODRenamer3.csv', header=True, index=False)
        if len(NewDF.index) > 1:
            startID = NewDF['Header_LiteID_ID'].min()
            EndID = NewDF['Header_LiteID_ID'].max()
            Query = f"""SELECT  [Header_LiteID_ID],[SP],[LOC_ID],[Distance]
                           FROM [dbo].EC01_PYRO_TRACE

                           WHERE [Header_LiteID_ID] >= '{startID}'
                           AND [Header_LiteID_ID] <= '{EndID}';"""
            cnxn = pyodbc.connect("Driver={SQL Server};"
                                  "Server=OB-VM-SQLDW02;"
                                  "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
            df2 = pd.read_sql_query(Query, cnxn)
            #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
            NewDF = NewDF.merge(df2, how='left', on=['Header_LiteID_ID', 'SP', 'LOC_ID'], suffixes=('', '_y'))
            #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta9.csv', header=True)
            NewDF = NewDF.loc[NewDF['Distance_y'].isnull()]
            #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta10.csv', header=True)
            drop_y(NewDF)
            #export_csv = NewDF.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta10.csv', header=True)
            params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
            engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
            NewDF.drop(['BONumber', 'CarrierColumn', 'ActualStart', 'LiteID'], axis=1, inplace=True)
            NewDF.to_sql('EC01_PYRO_TRACE', con=engine, if_exists='append', index=False)



pyro_store_path = r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\Pyro files\\'


def unique(list1):
    # insert the list to the set
    list_set = set(list1)
    # convert the set to the list
    unique_list = (list(list_set))
    return (unique_list)

def UpdatePyroFeedbacklite(Df):
    ListofCols = Df.columns
    Keeper=['LiteID', 'ActualStart']
    triggerwords=["Mean", "Avg", "CoG"]
    Pressure = [x for x in ListofCols if any(s in x for s in triggerwords)]
    print(Df.head().to_string())
    FilterMedia=Keeper+Pressure
    PivotList=Pressure
    Df=Df.filter(FilterMedia)
    Beginning = pd.melt(Df, id_vars=['LiteID', 'ActualStart'], value_vars=PivotList, var_name='Measure', value_name='Value')
    print(Beginning.head().to_string())
    Beginning['SP_HTO']='HTO'
    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta7.csv', header=True)

    Beginning['SP_HTO_LOC'] = 1

    Beginning['HTO_Zone'] = 3


    Beginning['MeasureType'] = "PYRORs"


    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta0.csv', header=True)

    startDate = (Beginning['ActualStart'].min() - timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (Beginning['ActualStart'].max() + timedelta(hours=6, minutes=30)).strftime("%m/%d/%Y %H:%M:%S")
    Query = f"""SELECT ID, [LiteID]
                        FROM [dbo].HEADER_LITEID
                        WHERE [ActualStart] >= '{startDate}'
                        AND [ActualStart] <= '{EndDate}';"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
    df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Beginning = Beginning.merge(df, on='LiteID', how='inner', suffixes=('', '_y'))
    #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta1.csv', header=True)
    Beginning= Beginning.dropna(subset=['ID'])
    drop_y(Beginning)
    Beginning = Beginning.rename({'ID': 'Header_LiteID_ID'}, axis='columns')
    if len(Beginning.index)>1:
        startID = Beginning['Header_LiteID_ID'].min()
        EndID = Beginning['Header_LiteID_ID'].max()
        Query = f"""SELECT  [Header_LiteID_ID],[Measure]
                       FROM [dbo].EC01_HTO_FEEDBACK_LITE

                       WHERE [Header_LiteID_ID] >= '{startID}'
                       AND [Header_LiteID_ID] <= '{EndID}';"""
        cnxn = pyodbc.connect("Driver={SQL Server};"
                              "Server=OB-VM-SQLDW02;"
                              "Trusted_Connection=yes;""DATABASE=CoaterTrace;")
        df2 = pd.read_sql_query(Query, cnxn)
        #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta2.csv', header=True)
        Beginning = Beginning.loc[~((Beginning.Header_LiteID_ID.isin(df2['Header_LiteID_ID'])) & (Beginning.loc[(Beginning.Header_LiteID_ID.isin(df2['Header_LiteID_ID'])), :].Measure.isin(df2['Measure']))), :]
        #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\FromTheTapBeta10.csv', header=True)
        params = urllib.parse.quote_plus("DRIVER={SQL Server};SERVER=OB-VM-SQLDW02;DATABASE=CoaterTrace;")
        engine = create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
        Beginning = Beginning.round({'Value': 12})
        Beginning=Beginning.loc[Beginning['Value'] <= 99999999.999999999999]
        Beginning.drop(['LiteID', 'ActualStart'], axis=1, inplace=True)
        #export_csv = Beginning.to_csv(r'C:\Users\arobinson\Desktop\Vigneshrtu.csv', header=True)
        Beginning.to_sql('EC01_HTO_FEEDBACK_LITE', con=engine, if_exists='append', index=False)

def get_path_ordnerlos(df_results):
    scn_tools = ['HTO01_SCN02', 'HTO01_SCN03']
    list = superlistdir(r'\\echromics\ob\ME-Data\HTO01_SCN02')
    for i in range(0, len(df_results['BONumber'])):
        bo_number = df_results['BONumber'].iloc[i]
        BON=str(bo_number)

        for tool in scn_tools:
            try:
                conn = pyodbc.connect(
                    'DSN=OB-VM-SQLDW03;Description=EDW;UID=reportinguser;PWD=SQLR3ports;DATABASE=Ordnerlos')
                cursor = conn.cursor()
                query_string = f"SELECT S3_URL FROM [Ordnerlos].[cld].[DataFiles] WHERE DB_Tool =  '{tool}'  AND FS_Extension = '.txt' AND DB_BONumber = {BON}"
                cursor.execute(query_string)

                temp_path = [x for x in cursor]
                # print (temp_path[0][0])
                filename = temp_path[0][0].split('/')[-1]

                with open(pyro_store_path + filename, "w") as copied_file:
                    file = urllib.request.urlopen(temp_path[0][0])
                    for line in file:
                        decoded_line = line.decode("ansi")
                        copied_file.write(decoded_line[:-1])
                        # print (decoded_line)
                cursor.close()
                conn.close()

                if tool == 'HTO01_SCN02':
                    df_results['SCN02_Data_Found'].iloc[i] = 'Yes'
                    df_results['SCN02_Path'].iloc[i] = temp_path[0][0]
                    df_results['SCN02_LocalPath'].iloc[i] = pyro_store_path + filename
                else:
                    df_results['SCN03_Data_Found'].iloc[i] = 'Yes'
                    df_results['SCN03_Path'].iloc[i] = temp_path[0][0]
                    df_results['SCN03_LocalPath'].iloc[i] = pyro_store_path + filename

                    datetime_string = filename.split('_')[1] + ' ' + filename.split('_')[2]
                    df_results['SCN03_TimeStamp'].iloc[i] = datetime.strptime(datetime_string, '%Y-%m-%d %H-%M-%S')

                # print (bo_number, 'Done')

            except Exception as e:
                # print (e)
                # print (bo_number, "Not Found")

                if tool == 'HTO01_SCN02':
                    df_results['SCN02_Data_Found'].iloc[i] = 'No'
                else:
                    df_results['SCN03_Data_Found'].iloc[i] = 'No'

    return (df_results)


def get_path_ordnerlos2(df_results):
    scn_tools = ['HTO01_SCN02', 'HTO01_SCN03']
    DirList = superlistdir(r'\\echromics\ob\ME-Data\HTO01_SCN02')
    DirList2 = superlistdir(r'\\echromics\ob\ME-Data\HTO01_SCN03')
    DirList = [x for x in DirList if '.txt' in x]
    DirList2 = [x for x in DirList2 if '.txt' in x]
    for i in range(0, len(df_results['BONumber'])):
        bo_number = df_results['BONumber'].iloc[i]
        BON=str(bo_number)

        for tool in scn_tools:
            try:

                if tool == 'HTO01_SCN02':
                    hitlist = [x for x in DirList if BON in x]
                    if len(hitlist)>0:
                        df_results['SCN02_Data_Found'].iloc[i] = 'Yes'
                        df_results['SCN02_Path'].iloc[i] = hitlist[0]
                        df_results['SCN02_LocalPath'].iloc[i] = hitlist[0]
                    else:
                        df_results['SCN02_Data_Found'].iloc[i] = 'No'

                else:
                    hitlist = [x for x in DirList2 if BON in x]
                    if len(hitlist) > 0:
                        df_results['SCN03_Data_Found'].iloc[i] = 'Yes'
                        df_results['SCN03_Path'].iloc[i] = hitlist[0]
                        df_results['SCN03_LocalPath'].iloc[i] = hitlist[0]
                        df_results['SCN03_TimeStamp'].iloc[i] = datetime.fromtimestamp(os.path.getctime(hitlist[0])).strftime('%Y-%m-%d %H:%M:%S')

                    else:
                        df_results['SCN03_Data_Found'].iloc[i] = 'No'


                # print (bo_number, 'Done')

            except Exception as e:
                print (e)
                print (bo_number, "Not Found")

                if tool == 'HTO01_SCN02':
                    df_results['SCN02_Data_Found'].iloc[i] = 'No'
                else:
                    df_results['SCN03_Data_Found'].iloc[i] = 'No'

    return (df_results)


def extract_pyro_data(df_results, df_master, df_bylite, df_centers, origin_x, origin_y, x_scale, y_scale, scn_type):
    df = pd.read_csv(df_results.iloc[0][scn_type], skiprows=11, sep='\t', header=None)
    df.drop(labels=652, axis=1, inplace=True)
    df_stacked = df.stack()
    df_stacked = df_stacked.reset_index()
    for i in range(0, len(df_results)):
        liteid1 = df_results.iloc[i]['LiteID']

        try:
            # origin_x = 8
            # origin_y = 28

            xlim_1 = origin_x + df_results.iloc[i]['CorrC1X']
            xlim_2 = origin_x + df_results.iloc[i]['CorrC2X']
            ylim_1 = origin_y + df_results.iloc[i]['CorrC1Y']
            ylim_2 = origin_y + df_results.iloc[i]['CorrC2Y']

            df_stacked['Lines'] = df_stacked['level_0'] + 1
            df_stacked['Pixels'] = df_stacked['level_1'] + 1
            df_stacked['Pyro Reading (C)'] = df_stacked[0] / 10
            # df_stacked['X'] = df_stacked['Lines']*120/655.0
            # df_stacked['Y'] = df_stacked['Pixels']*72/385.0
            df_stacked['X'] = df_stacked['Lines'] * 120 / x_scale
            df_stacked['Y'] = df_stacked['Pixels'] * 72 / y_scale

            df_stacked.drop(columns=['level_0', 'level_1', 0], inplace=True)
            df_stacked['LiteID'] = [liteid1] * len(df_stacked)
            df_stacked['BONumber'] = [df_results.iloc[i]['BONumber']] * len(df_stacked)
            df_master = pd.concat([df_master, df_stacked])

            df_stacked = df_stacked[
                (df_stacked['X'] >= xlim_1) & (df_stacked['X'] <= xlim_2) & (df_stacked['Y'] >= ylim_1) & (
                            df_stacked['Y'] <= ylim_2)]
            df_bylite = pd.concat([df_bylite, df_stacked])

            # print (xlim_1, xlim_2, ylim_1, ylim_2)
            cog_x = (xlim_1 + xlim_2) / 2
            cog_y = (ylim_1 + ylim_2) / 2
            # print (cog_x, cog_y)

            mb_size = 3  # inches measurement box
            df_centers_temp = df_stacked[
                (df_stacked['X'] >= (cog_x - mb_size / 2)) & (df_stacked['X'] <= (cog_x + mb_size / 2)) & (
                            df_stacked['Y'] >= (cog_y - mb_size / 2)) & (
                            df_stacked['Y'] <= (cog_y + mb_size / 2))].copy()
            df_centers = pd.concat([df_centers, df_centers_temp])
            print(df_results.iloc[i]['BONumber'], 'Done')
        except:
            print(df_results.iloc[i]['BONumber'], 'Did not work')
    return (df_master, df_bylite, df_centers)


def process_scn03_centers(df_results, df_centers):
    df_centers.head()
    df_centers['Flip Lines'] = 751 - df_centers['Lines']
    df_centers['Exclude_Non_Lite'] = ['No'] * len(df_centers['Lines'])
    df_centers.loc[
        (df_centers['Pixels'] >= 530.0) | (df_centers['Pixels'] <= 160.0) | (df_centers['Flip Lines'] <= 60.0) | (
                    df_centers['Flip Lines'] >= 690.0), 'Exclude_Non_Lite'] = 'Yes'

    df_centers['Exclude_Separators'] = ['No'] * len(df_centers['Lines'])
    df_centers.loc[
        (df_centers['Pyro Reading (C)'] <= 220) | (df_centers['Pyro Reading (C)'] >= 340), 'Exclude_Separators'] = 'Yes'

    df_centers['Exclude_Sensor_Band'] = ['No'] * len(df_centers['Lines'])
    df_centers.loc[(df_centers['Pixels'] >= 235) & (df_centers['Pixels'] <= 280), 'Exclude_Sensor_Band'] = 'Yes'

    df_centers_filtered = df_centers[
        (df_centers['Exclude_Non_Lite'] == 'No') & (df_centers['Exclude_Separators'] == 'No') & (
                    df_centers['Exclude_Sensor_Band'] == 'No')].copy()
    df_scn03_centers_means = df_centers_filtered[['LiteID', 'Pyro Reading (C)']].groupby(by='LiteID').mean()
    df_scn03_centers_means.rename(columns={'Pyro Reading (C)': 'Mean_SCN03_CoG'}, inplace=True)


    df_results = pd.merge(df_results, df_scn03_centers_means, how='left', on='LiteID')

    return (df_results)


def process_scn03_bylite(df_results, df_bylite):
    df_bylite.head()
    df_bylite['Flip Lines'] = 751 - df_bylite['Lines']
    df_bylite['Exclude_Non_Lite'] = ['No'] * len(df_bylite['Lines'])
    df_bylite.loc[
        (df_bylite['Pixels'] >= 530.0) | (df_bylite['Pixels'] <= 160.0) | (df_bylite['Flip Lines'] <= 60.0) | (
                    df_bylite['Flip Lines'] >= 690.0), 'Exclude_Non_Lite'] = 'Yes'
    #     print (len(df_bylite[df_bylite['Exclude_Non_Lite'] == 'Yes']))

    df_bylite['Exclude_Separators'] = ['No'] * len(df_bylite['Lines'])
    df_bylite.loc[
        (df_bylite['Pyro Reading (C)'] <= 220) | (df_bylite['Pyro Reading (C)'] >= 340), 'Exclude_Separators'] = 'Yes'

    df_bylite['Exclude_Sensor_Band'] = ['No'] * len(df_bylite['Lines'])
    df_bylite.loc[(df_bylite['Pixels'] >= 235) & (df_bylite['Pixels'] <= 280), 'Exclude_Sensor_Band'] = 'Yes'

    df_bylite_filtered = df_bylite[
        (df_bylite['Exclude_Non_Lite'] == 'No') & (df_bylite['Exclude_Separators'] == 'No') & (
                    df_bylite['Exclude_Sensor_Band'] == 'No')].copy()
    #     print (len(df_bylite), len(df_bylite_filtered))
    df_scn03_avg_means = df_bylite_filtered[['LiteID', 'Pyro Reading (C)']].groupby(by='LiteID').mean()
    df_scn03_avg_means.rename(columns={'Pyro Reading (C)': 'Mean_SCN03_Avg'}, inplace=True)

    df_results = pd.merge(df_results, df_scn03_avg_means, how='left', on='LiteID')

    return (df_results)


def process_scn02_bycarrier(df_results, df_master_scn02):
    print(df_master_scn02.head().to_string())
    df_master_scn02['Flip Lines'] = 751 - df_master_scn02['Lines']
    df_master_scn02['Exclude_Non_Lite'] = ['No'] * len(df_master_scn02['Lines'])
    df_master_scn02.loc[(df_master_scn02['Pixels'] >= 565.0) | (df_master_scn02['Pixels'] <= 90.0) | (
                df_master_scn02['Flip Lines'] <= 65.0) | (
                                    df_master_scn02['Flip Lines'] >= 705.0), 'Exclude_Non_Lite'] = 'Yes'

    df_master_scn02['Exclude_Separators'] = ['No'] * len(df_master_scn02['Lines'])
    df_master_scn02.loc[(df_master_scn02['Pyro Reading (C)'] <= 250), 'Exclude_Separators'] = 'Yes'

    df_master_scn02_filtered = df_master_scn02[
        (df_master_scn02['Exclude_Non_Lite'] == 'No') & (df_master_scn02['Exclude_Separators'] == 'No')].copy()
    df_master_scn02_medians = df_master_scn02_filtered[['BONumber', 'Pyro Reading (C)']].groupby(by='BONumber').median()
    df_master_scn02_medians.rename(columns={'Pyro Reading (C)': 'Median_SCN02_Carrier'}, inplace=True)


    df_results = pd.merge(df_results, df_master_scn02_medians, how='left', on='BONumber')

    return (df_results)


def process_scn02_centers(df_results, df_centers_scn02):
    df_centers_scn02.head()
    df_centers_scn02['Flip Lines'] = 751 - df_centers_scn02['Lines']
    df_centers_scn02['Exclude_Non_Lite'] = ['No'] * len(df_centers_scn02['Lines'])
    df_centers_scn02.loc[(df_centers_scn02['Pixels'] >= 565.0) | (df_centers_scn02['Pixels'] <= 90.0) | (
                df_centers_scn02['Flip Lines'] <= 65.0) | (
                                     df_centers_scn02['Flip Lines'] >= 705.0), 'Exclude_Non_Lite'] = 'Yes'

    df_centers_scn02['Exclude_Separators'] = ['No'] * len(df_centers_scn02['Lines'])
    df_centers_scn02.loc[(df_centers_scn02['Pyro Reading (C)'] <= 250), 'Exclude_Separators'] = 'Yes'

    df_centers_scn02_filtered = df_centers_scn02[
        (df_centers_scn02['Exclude_Non_Lite'] == 'No') & (df_centers_scn02['Exclude_Separators'] == 'No')].copy()
    df_scn02_centers_means = df_centers_scn02_filtered[['LiteID', 'Pyro Reading (C)']].groupby(by='LiteID').mean()
    df_scn02_centers_means.rename(columns={'Pyro Reading (C)': 'Mean_SCN02_CoG'}, inplace=True)


    df_results = pd.merge(df_results, df_scn02_centers_means, how='left', on='LiteID')

    return (df_results)


def process_scn02_bylite(df_results, df_bylite_scn02):
    print(df_bylite_scn02.head().to_string())
    df_bylite_scn02['Flip Lines'] = 751 - df_bylite_scn02['Lines']

    df_bylite_scn02['Exclude_Non_Lite'] = ['No'] * len(df_bylite_scn02['Lines'])
    df_bylite_scn02.loc[(df_bylite_scn02['Pixels'] >= 565.0) | (df_bylite_scn02['Pixels'] <= 90.0) | \
                        (df_bylite_scn02['Flip Lines'] <= 65.0) | (
                                    df_bylite_scn02['Flip Lines'] >= 705.0), 'Exclude_Non_Lite'] = 'Yes'

    df_bylite_scn02['Exclude_Separators'] = ['No'] * len(df_bylite_scn02['Lines'])
    df_bylite_scn02.loc[(df_bylite_scn02['Pyro Reading (C)'] <= 250), 'Exclude_Separators'] = 'Yes'

    df_bylite_scn02_filtered = df_bylite_scn02[
        (df_bylite_scn02['Exclude_Non_Lite'] == 'No') & (df_bylite_scn02['Exclude_Separators'] == 'No')].copy()
    #     print (len(df_bylite), len(df_bylite_filtered))
    df_scn02_avg_means = df_bylite_scn02_filtered[['LiteID', 'Pyro Reading (C)']].groupby(by='LiteID').mean()
    df_scn02_avg_means.rename(columns={'Pyro Reading (C)': 'Mean_SCN02_Avg'}, inplace=True)

    df_results = pd.merge(df_results, df_scn02_avg_means, how='left', on='LiteID')

    return (df_results)



def FromHTOtoITORs(CRO):
    CRO.head()
    print("HTOtoITORS Running")


    List=["BONumber", "LiteID", "ActualStart", 'CorrC1X', 'CorrC2X', 'CorrC1Y', 'CorrC2Y', "LiteHeight", "LiteWidth", "LiteLoc"]
    df_result=CRO.filter(List)
    df_result['ActualStart_Datetime'] = pd.to_datetime(df_result['ActualStart'], format = '%Y-%m-%d %H:%M:%S.%f')
    df_result['SCN02_Data_Found'] = np.NaN
    df_result['SCN03_Data_Found'] = np.NaN
    df_result['SCN02_Path'] = np.NaN
    df_result['SCN03_Path'] = np.NaN
    df_result['SCN02_LocalPath'] = np.NaN
    df_result['SCN03_LocalPath'] = np.NaN
    df_result['SCN03_TimeStamp'] = np.NaN
    df_result = get_path_ordnerlos2(df_result)
    df_result = df_result.loc[((df_result['SCN02_Data_Found']=='Yes') & (df_result['SCN03_Data_Found']=='Yes')), :]
    if len(df_result.index)>0:
        ListofBOs=df_result["BONumber"].tolist()
        ListofBOs=unique(ListofBOs)
        x= len(df_result.index)
        NumberofPartions = math.ceil(x/20)
        df_result2=np.split(df_result, [NumberofPartions], axis=1)
        for BO in ListofBOs:
            df_results=df_result.loc[(df_result['BONumber']==BO), :]
            df_results.to_csv(r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\df_ordnerlos.csv', index=False)
            df_results['SCN03_TimeStamp'] = pd.to_datetime(df_results['SCN03_TimeStamp'])

            df_results['Timedelta'] = (
                        df_results['SCN03_TimeStamp'] - df_results['ActualStart_Datetime']).dt.total_seconds().div(60)
            df_results['Timedelta'].fillna(value=-1, inplace=True)
            df_results['Timedelta'] = df_results['Timedelta'].astype('int')
            df_results['Exclude_for_Timedelta'] = np.NaN
            df_results.loc[(df_results['Timedelta'] < 215) | (df_results['Timedelta'] > 235)|(df_results['Timedelta']<275), 'Exclude_for_Timedelta'] = 'Exclude'

            # For Scanner2
            df_master_scn02 = pd.DataFrame({})
            df_bylite_scn02 = pd.DataFrame({})
            df_centers_scn02 = pd.DataFrame({})
            if path.exists(df_results.iloc[0]['SCN02_LocalPath']):
                (df_master_scn02, df_bylite_scn02, df_centers_scn02) = extract_pyro_data(df_results, df_master_scn02,
                                                                                         df_bylite_scn02, df_centers_scn02,
                                                                                         origin_x=6, origin_y=14, x_scale=667.0,
                                                                                         y_scale=476.0, scn_type='SCN02_LocalPath')

                df_bylite_scn02.to_csv(
                    r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\df_bylite_scn02.csv',
                    index=False)
                df_centers_scn02.to_csv(
                    r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\df_centers_scn02.csv',
                    index=False)



                # For Scanner3
                df_master = pd.DataFrame({})
                df_bylite = pd.DataFrame({})
                df_centers = pd.DataFrame({})
                if path.exists(df_results.iloc[0]['SCN03_LocalPath']):
                    [df_master, df_bylite, df_centers] = extract_pyro_data(df_results, df_master, df_bylite, df_centers, origin_x=8,origin_y=28, x_scale=655.0, y_scale=385.0,scn_type='SCN03_LocalPath')

                    # Save bylite and centers csv files for testing

                    # df_master.to_csv(r'C:\Users\vsundar\Documents\Projects\Python scripts\ITO Rs Model\df_master.csv', index = False)
                    df_bylite.to_csv(
                        r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\df_bylite_scn03.csv',
                        index=False)
                    df_centers.to_csv(
                        r'\\echromics\ob\General\PVD01\WonderTrace\Program for Production\df_centers_scn03.csv',
                        index=False)

                    df_results = process_scn03_centers(df_results, df_centers)
                    df_results = process_scn03_bylite(df_results, df_bylite)
                    df_results = process_scn02_bycarrier(df_results, df_master_scn02)
                    df_results = process_scn02_centers(df_results, df_centers_scn02)
                    df_results = process_scn02_bylite(df_results, df_bylite_scn02)

                    df_results['Pred_ITORs_Avg_Lin'] = (-7.8017) + (0.1724 * df_results["Mean_SCN03_Avg"]) + (
                                -0.0760 * df_results["Median_SCN02_Carrier"]) + \
                                                       ((df_results["Median_SCN02_Carrier"] - 383.9037) * (
                                                                   df_results["Mean_SCN03_Avg"] - 274.9611) * 0.0020)

                    df_results['Pred_ITORs_Avg_Quad'] = (-7.9026) + (0.1656 * df_results["Mean_SCN03_Avg"]) + (
                                -0.0710 * df_results["Median_SCN02_Carrier"]) + \
                                                        (df_results["Mean_SCN03_Avg"] - 274.9611) ** 2 * 0.0006

                    df_results['Pred_ITORs_CoG_Lin'] = (-14.6544) + (0.1383 * df_results["Mean_SCN03_CoG"]) + (
                                -0.0352 * df_results["Median_SCN02_Carrier"]) + \
                                                       (df_results["Median_SCN02_Carrier"] - 383.9037) * (
                                                                   df_results["Mean_SCN03_CoG"] - 269.3186) * 0.0048

                    df_results['Pred_ITORs_CoG_Quad'] = (-12.3901) + (0.1250 * df_results["Mean_SCN03_CoG"]) + (
                                -0.0319 * df_results["Median_SCN02_Carrier"]) + \
                                                        (df_results["Mean_SCN03_CoG"] - 269.3186) ** 2 * 0.0011

                    List2=["LiteID", "ActualStart" ,"Pred_ITORs_Avg_Lin", "Pred_ITORs_Avg_Quad", "Pred_ITORs_CoG_Lin", "Pred_ITORs_CoG_Quad", "Mean_SCN03_CoG", "Mean_SCN03_Avg", "Mean_SCN02_CoG", "Mean_SCN02_Avg"]
                    df_results=df_results.filter(List2)
                    UpdatePyroFeedbacklite(df_results)



def BigPullSP1(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

            Select *
            FROM OPENQUERY(INSQL, "SELECT 
            [CA041.AC041_MF_Power],[CA042.AC042_MF_Power],[CA043.AC043_MF_Power],[CA044.AC044_MF_Power],[CA045.AC045_MF_Power],[CA046.AC046_MF_Power],[CA047.AC047_MF_Power],[CA048.AC048_MF_Power],[RCEX1.CA1PowerFeedback],[RCEX1.CA2PowerFeedback],[RCEX1.CA3PowerFeedback],[CA041.TMP0411_Rotation],[CA041.TMP0412_Rotation],[CA041.TMP0413_Rotation],[CA042.TMP0421_Rotation],[CA042.TMP0422_Rotation],[CA042.TMP0423_Rotation],[CA043.TMP0431_Rotation],[CA043.TMP0432_Rotation],[CA043.TMP0433_Rotation],[CA044.TMP0441_Rotation],[CA044.TMP0442_Rotation],[CA044.TMP0443_Rotation],[CA045.TMP0451_Rotation],[CA045.TMP0452_Rotation],[CA045.TMP0453_Rotation],[CA046.TMP0461_Rotation],[CA046.TMP0462_Rotation],[CA046.TMP0463_Rotation],[CA047.TMP0471_Rotation],[CA047.TMP0472_Rotation],[CA047.TMP0473_Rotation],[CA048.TMP0481_Rotation],[CA048.TMP0482_Rotation],[CA048.TMP0483_Rotation],[RCEX1.TMP11Feedback],[RCEX1.TMP12Feedback],[RCEX1.TMP13Feedback],[RCEX1.TMP21Feedback],[RCEX1.TMP22Feedback],[RCEX1.TMP23Feedback],[RCEX1.TMP31Feedback],[RCEX1.TMP32Feedback],[RCEX1.TMP33Feedback],[CA041.MFC0411_FlowRate],[CA041.MFC0412_FlowRate],[CA041.MFC0413_FlowRate],[CA041.MFC0414_FlowRate],[CA041.MFC0415_FlowRate],[CA041.MFC0416_FlowRate],[CA041.MFC0417_FlowRate],[CA042.MFC0421_FlowRate],[CA042.MFC0422_FlowRate],[CA042.MFC0423_FlowRate],[CA042.MFC0424_FlowRate],[CA042.MFC0425_FlowRate],[CA042.MFC0426_FlowRate],[CA042.MFC0427_FlowRate],[CA043.MFC0431_FlowRate],[CA043.MFC0432_FlowRate],[CA043.MFC0433_FlowRate],[CA043.MFC0434_FlowRate],[CA043.MFC0435_FlowRate],[CA043.MFC0436_FlowRate],[CA043.MFC0437_FlowRate],[CA044.MFC0441_FlowRate],[CA044.MFC0442_FlowRate],[CA044.MFC0443_FlowRate],[CA044.MFC0444_FlowRate],[CA044.MFC0445_FlowRate],[CA044.MFC0446_FlowRate],[CA044.MFC0447_FlowRate],[CA045.MFC0451_FlowRate],[CA045.MFC0452_FlowRate],[CA045.MFC0453_FlowRate],[CA045.MFC0454_FlowRate],[CA045.MFC0455_FlowRate],[CA045.MFC0456_FlowRate],[CA045.MFC0457_FlowRate],[CA046.MFC0461_FlowRate],[CA046.MFC0462_FlowRate],[CA046.MFC0463_FlowRate],[CA046.MFC0464_FlowRate],[CA046.MFC0465_FlowRate],[CA046.MFC0466_FlowRate],[CA046.MFC0467_FlowRate],[CA047.MFC0471_FlowRate],[CA047.MFC0472_FlowRate],[CA047.MFC0473_FlowRate],[CA047.MFC0474_FlowRate],[CA047.MFC0475_FlowRate],[CA047.MFC0476_FlowRate],[CA047.MFC0477_FlowRate],[CA048.MFC0481_FlowRate],[CA048.MFC0482_FlowRate],[CA048.MFC0483_FlowRate],[CA048.MFC0484_FlowRate],[CA048.MFC0485_FlowRate],[CA048.MFC0486_FlowRate],[CA048.MFC0487_FlowRate],[RCEX1.MFC11Feedback],[RCEX1.MFC12Feedback],[RCEX1.MFC21Feedback],[RCEX1.MFC22Feedback],[RCEX1.MFC31Feedback],[RCEX1.MFC32Feedback],[COMMON_1.DG041_Pressure],[COMMON_1.PiG041_Pressure],[CA041.AC041_MF_Volt],[CA042.AC042_MF_Volt],[CA043.AC043_MF_Volt],[CA044.AC044_MF_Volt],[CA045.AC045_MF_Volt],[CA046.AC046_MF_Volt],[CA047.AC047_MF_Volt],[CA048.AC048_MF_Volt],[RCEX1.CA1VoltageFeedback],[RCEX1.CA2VoltageFeedback],[RCEX1.CA3VoltageFeedback],[EC01_SP1_FRGA.AMU_45],[EC01_SP1_FRGA.Argon_20],[EC01_SP1_FRGA.CarbonDioxide_44],[EC01_SP1_FRGA.MPOil_43],[EC01_SP1_FRGA.Nitro_28],[EC01_SP1_FRGA.Oxygen_32],[EC01_SP1_FRGA.Water_17],[EC01_SP1_FRGA.Water_18],[CA041.MR0411_ActualTorqueValue],[CA041.MR0412_ActualTorqueValue],[CA042.MR0421_ActualTorqueValue],[CA042.MR0422_ActualTorqueValue],[CA043.MR0431_ActualTorqueValue],[CA043.MR0432_ActualTorqueValue],[CA044.MR0441_ActualTorqueValue],[CA044.MR0442_ActualTorqueValue],[CA045.MR0451_ActualTorqueValue],[CA045.MR0452_ActualTorqueValue],[CA046.MR0461_ActualTorqueValue],[CA046.MR0462_ActualTorqueValue],[CA047.MR0471_ActualTorqueValue],[CA047.MR0472_ActualTorqueValue],[CA048.MR0481_ActualTorqueValue],[CA048.MR0482_ActualTorqueValue],[RCEX1.MR11TorqueFeedback],[RCEX1.MR12TorqueFeedback],[RCEX1.MR21TorqueFeedback],[RCEX1.MR22TorqueFeedback],[RCEX1.MR31TorqueFeedback],[RCEX1.MR32TorqueFeedback],[CA041.MR0411_ActualSpeedValue],[CA041.MR0412_ActualSpeedValue],[CA042.MR0421_ActualSpeedValue],[CA042.MR0422_ActualSpeedValue],[CA043.MR0431_ActualSpeedValue],[CA043.MR0432_ActualSpeedValue],[CA044.MR0441_ActualSpeedValue],[CA044.MR0442_ActualSpeedValue],[CA045.MR0451_ActualSpeedValue],[CA045.MR0452_ActualSpeedValue],[CA046.MR0461_ActualSpeedValue],[CA046.MR0462_ActualSpeedValue],[CA047.MR0471_ActualSpeedValue],[CA047.MR0472_ActualSpeedValue],[CA048.MR0481_ActualSpeedValue],[CA048.MR0482_ActualSpeedValue],[RCEX1.MR11SpeedFeedback],[RCEX1.MR12SpeedFeedback],[RCEX1.MR21SpeedFeedback],[RCEX1.MR22SpeedFeedback],[RCEX1.MR31SpeedFeedback],[RCEX1.MR32SpeedFeedback]
            ,StartDateTime
            ,DateTime
            FROM WideHistory
            WHERE wwRetrievalMode = 'Cyclic'
            AND wwResolution = 3000

            AND DateTime >= '{startDate}'
            AND DateTime <='{endDate}'")Hist
            """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA041.AC041_MF_Power', 'CA042.AC042_MF_Power', 'CA043.AC043_MF_Power',
                     'CA044.AC044_MF_Power', 'CA045.AC045_MF_Power', 'CA046.AC046_MF_Power', 'CA047.AC047_MF_Power',
                     'CA048.AC048_MF_Power', 'RCEX1.CA1PowerFeedback', 'RCEX1.CA2PowerFeedback',
                     'RCEX1.CA3PowerFeedback', 'CA041.TMP0411_Rotation', 'CA041.TMP0412_Rotation',
                     'CA041.TMP0413_Rotation', 'CA042.TMP0421_Rotation', 'CA042.TMP0422_Rotation',
                     'CA042.TMP0423_Rotation', 'CA043.TMP0431_Rotation', 'CA043.TMP0432_Rotation',
                     'CA043.TMP0433_Rotation', 'CA044.TMP0441_Rotation', 'CA044.TMP0442_Rotation',
                     'CA044.TMP0443_Rotation', 'CA045.TMP0451_Rotation', 'CA045.TMP0452_Rotation',
                     'CA045.TMP0453_Rotation', 'CA046.TMP0461_Rotation', 'CA046.TMP0462_Rotation',
                     'CA046.TMP0463_Rotation', 'CA047.TMP0471_Rotation', 'CA047.TMP0472_Rotation',
                     'CA047.TMP0473_Rotation', 'CA048.TMP0481_Rotation', 'CA048.TMP0482_Rotation',
                     'CA048.TMP0483_Rotation', 'RCEX1.TMP11Feedback', 'RCEX1.TMP12Feedback', 'RCEX1.TMP13Feedback',
                     'RCEX1.TMP21Feedback', 'RCEX1.TMP22Feedback', 'RCEX1.TMP23Feedback', 'RCEX1.TMP31Feedback',
                     'RCEX1.TMP32Feedback', 'RCEX1.TMP33Feedback', 'CA041.MFC0411_FlowRate', 'CA041.MFC0412_FlowRate',
                     'CA041.MFC0413_FlowRate', 'CA041.MFC0414_FlowRate', 'CA041.MFC0415_FlowRate',
                     'CA041.MFC0416_FlowRate', 'CA041.MFC0417_FlowRate', 'CA042.MFC0421_FlowRate',
                     'CA042.MFC0422_FlowRate', 'CA042.MFC0423_FlowRate', 'CA042.MFC0424_FlowRate',
                     'CA042.MFC0425_FlowRate', 'CA042.MFC0426_FlowRate', 'CA042.MFC0427_FlowRate',
                     'CA043.MFC0431_FlowRate', 'CA043.MFC0432_FlowRate', 'CA043.MFC0433_FlowRate',
                     'CA043.MFC0434_FlowRate', 'CA043.MFC0435_FlowRate', 'CA043.MFC0436_FlowRate',
                     'CA043.MFC0437_FlowRate', 'CA044.MFC0441_FlowRate', 'CA044.MFC0442_FlowRate',
                     'CA044.MFC0443_FlowRate', 'CA044.MFC0444_FlowRate', 'CA044.MFC0445_FlowRate',
                     'CA044.MFC0446_FlowRate', 'CA044.MFC0447_FlowRate', 'CA045.MFC0451_FlowRate',
                     'CA045.MFC0452_FlowRate', 'CA045.MFC0453_FlowRate', 'CA045.MFC0454_FlowRate',
                     'CA045.MFC0455_FlowRate', 'CA045.MFC0456_FlowRate', 'CA045.MFC0457_FlowRate',
                     'CA046.MFC0461_FlowRate', 'CA046.MFC0462_FlowRate', 'CA046.MFC0463_FlowRate',
                     'CA046.MFC0464_FlowRate', 'CA046.MFC0465_FlowRate', 'CA046.MFC0466_FlowRate',
                     'CA046.MFC0467_FlowRate', 'CA047.MFC0471_FlowRate', 'CA047.MFC0472_FlowRate',
                     'CA047.MFC0473_FlowRate', 'CA047.MFC0474_FlowRate', 'CA047.MFC0475_FlowRate',
                     'CA047.MFC0476_FlowRate', 'CA047.MFC0477_FlowRate', 'CA048.MFC0481_FlowRate',
                     'CA048.MFC0482_FlowRate', 'CA048.MFC0483_FlowRate', 'CA048.MFC0484_FlowRate',
                     'CA048.MFC0485_FlowRate', 'CA048.MFC0486_FlowRate', 'CA048.MFC0487_FlowRate',
                     'RCEX1.MFC11Feedback', 'RCEX1.MFC12Feedback', 'RCEX1.MFC21Feedback', 'RCEX1.MFC22Feedback',
                     'RCEX1.MFC31Feedback', 'RCEX1.MFC32Feedback', 'COMMON_1.DG041_Pressure',
                     'COMMON_1.PiG041_Pressure', 'CA041.AC041_MF_Volt', 'CA042.AC042_MF_Volt', 'CA043.AC043_MF_Volt',
                     'CA044.AC044_MF_Volt', 'CA045.AC045_MF_Volt', 'CA046.AC046_MF_Volt', 'CA047.AC047_MF_Volt',
                     'CA048.AC048_MF_Volt', 'RCEX1.CA1VoltageFeedback','RCEX1.CA2VoltageFeedback','RCEX1.CA3VoltageFeedback','EC01_SP1_FRGA.AMU_45', 'EC01_SP1_FRGA.Argon_20',
                     'EC01_SP1_FRGA.CarbonDioxide_44', 'EC01_SP1_FRGA.MPOil_43', 'EC01_SP1_FRGA.Nitro_28',
                     'EC01_SP1_FRGA.Oxygen_32', 'EC01_SP1_FRGA.Water_17', 'EC01_SP1_FRGA.Water_18','CA041.MR0411_ActualTorqueValue','CA041.MR0412_ActualTorqueValue','CA042.MR0421_ActualTorqueValue','CA042.MR0422_ActualTorqueValue','CA043.MR0431_ActualTorqueValue','CA043.MR0432_ActualTorqueValue','CA044.MR0441_ActualTorqueValue','CA044.MR0442_ActualTorqueValue','CA045.MR0451_ActualTorqueValue','CA045.MR0452_ActualTorqueValue','CA046.MR0461_ActualTorqueValue','CA046.MR0462_ActualTorqueValue','CA047.MR0471_ActualTorqueValue','CA047.MR0472_ActualTorqueValue','CA048.MR0481_ActualTorqueValue','CA048.MR0482_ActualTorqueValue','RCEX1.MR11TorqueFeedback','RCEX1.MR12TorqueFeedback','RCEX1.MR21TorqueFeedback','RCEX1.MR22TorqueFeedback','RCEX1.MR31TorqueFeedback','RCEX1.MR32TorqueFeedback','CA041.MR0411_ActualSpeedValue','CA041.MR0412_ActualSpeedValue','CA042.MR0421_ActualSpeedValue','CA042.MR0422_ActualSpeedValue','CA043.MR0431_ActualSpeedValue','CA043.MR0432_ActualSpeedValue','CA044.MR0441_ActualSpeedValue','CA044.MR0442_ActualSpeedValue','CA045.MR0451_ActualSpeedValue','CA045.MR0452_ActualSpeedValue','CA046.MR0461_ActualSpeedValue','CA046.MR0462_ActualSpeedValue','CA047.MR0471_ActualSpeedValue','CA047.MR0472_ActualSpeedValue','CA048.MR0481_ActualSpeedValue','CA048.MR0482_ActualSpeedValue','RCEX1.MR11SpeedFeedback','RCEX1.MR12SpeedFeedback','RCEX1.MR21SpeedFeedback','RCEX1.MR22SpeedFeedback','RCEX1.MR31SpeedFeedback','RCEX1.MR32SpeedFeedback']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 2.5
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP1 Measurement Start Time']=np.roll(Df['DateTime'], RollingSize)
    Df['SP1 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP1 Measurement Start Time', 'SP1 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP1 Measurement Start Time', 'SP1 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    #export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\Cresist3.csv', header=True)
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP2(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    CarrierLength = 1
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

               Select *
               FROM OPENQUERY(INSQL, "SELECT
               [CA061.AC061_MF_Power],[CA062.AC062_MF_Power],[CA063.AC063_MF_Power],[CA064.AC064_MF_Power],[CA061.TMP0611_Rotation],[CA061.TMP0612_Rotation],[CA061.TMP0613_Rotation],[CA062.TMP0621_Rotation],[CA062.TMP0622_Rotation],[CA062.TMP0623_Rotation],[CA063.TMP0631_Rotation],[CA063.TMP0632_Rotation],[CA063.TMP0633_Rotation],[CA064.TMP0641_Rotation],[CA064.TMP0642_Rotation],[CA064.TMP0643_Rotation],[CA061.MFC0611_FlowRate],[CA061.MFC0612_FlowRate],[CA061.MFC0613_FlowRate],[CA061.MFC0614_FlowRate],[CA061.MFC0615_FlowRate],[CA061.MFC0616_FlowRate],[CA061.MFC0617_FlowRate],[CA062.MFC0621_FlowRate],[CA062.MFC0622_FlowRate],[CA062.MFC0623_FlowRate],[CA062.MFC0624_FlowRate],[CA062.MFC0625_FlowRate],[CA062.MFC0626_FlowRate],[CA062.MFC0627_FlowRate],[CA063.MFC0631_FlowRate],[CA063.MFC0632_FlowRate],[CA063.MFC0633_FlowRate],[CA063.MFC0634_FlowRate],[CA063.MFC0635_FlowRate],[CA063.MFC0636_FlowRate],[CA063.MFC0637_FlowRate],[CA064.MFC0641_FlowRate],[CA064.MFC0642_FlowRate],[CA064.MFC0643_FlowRate],[CA064.MFC0644_FlowRate],[CA064.MFC0645_FlowRate],[CA064.MFC0646_FlowRate],[CA064.MFC0647_FlowRate],[COMMON_2.DG061_Pressure],[COMMON_2.PiG061_Pressure],[CA061.AC061_MF_Volt],[CA062.AC062_MF_Volt],[CA063.AC063_MF_Volt],[CA064.AC064_MF_Volt],[EC01_SP2_FRGA.AMU_45],[EC01_SP2_FRGA.Argon_20],[EC01_SP2_FRGA.CarbonDioxide_44],[EC01_SP2_FRGA.MPOil_43],[EC01_SP2_FRGA.Nitro_28],[EC01_SP2_FRGA.Oxygen_32],[EC01_SP2_FRGA.Water_17],[EC01_SP2_FRGA.Water_18],[CA061.MR0611_ActualTorqueValue],[CA061.MR0612_ActualTorqueValue],[CA062.MR0621_ActualTorqueValue],[CA062.MR0622_ActualTorqueValue],[CA063.MR0631_ActualTorqueValue],[CA063.MR0632_ActualTorqueValue],[CA061.MR0611_ActualSpeedValue],[CA061.MR0612_ActualSpeedValue],[CA062.MR0621_ActualSpeedValue],[CA062.MR0622_ActualSpeedValue],[CA063.MR0631_ActualSpeedValue],[CA063.MR0632_ActualSpeedValue],[CA064.MR0641_ActualSpeedValue],[CA064.MR0642_ActualSpeedValue]
               ,StartDateTime
               ,DateTime
               FROM WideHistory
               WHERE wwRetrievalMode = 'Cyclic'
               AND wwResolution = 3000

               AND DateTime >= '{startDate}'
               AND DateTime <='{endDate}'")Hist
               """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA061.AC061_MF_Power', 'CA062.AC062_MF_Power', 'CA063.AC063_MF_Power',
                     'CA064.AC064_MF_Power', 'CA061.TMP0611_Rotation', 'CA061.TMP0612_Rotation',
                     'CA061.TMP0613_Rotation', 'CA062.TMP0621_Rotation', 'CA062.TMP0622_Rotation',
                     'CA062.TMP0623_Rotation', 'CA063.TMP0631_Rotation', 'CA063.TMP0632_Rotation',
                     'CA063.TMP0633_Rotation', 'CA064.TMP0641_Rotation', 'CA064.TMP0642_Rotation',
                     'CA064.TMP0643_Rotation', 'CA061.MFC0611_FlowRate', 'CA061.MFC0612_FlowRate',
                     'CA061.MFC0613_FlowRate', 'CA061.MFC0614_FlowRate', 'CA061.MFC0615_FlowRate',
                     'CA061.MFC0616_FlowRate', 'CA061.MFC0617_FlowRate', 'CA062.MFC0621_FlowRate',
                     'CA062.MFC0622_FlowRate', 'CA062.MFC0623_FlowRate', 'CA062.MFC0624_FlowRate',
                     'CA062.MFC0625_FlowRate', 'CA062.MFC0626_FlowRate', 'CA062.MFC0627_FlowRate',
                     'CA063.MFC0631_FlowRate', 'CA063.MFC0632_FlowRate', 'CA063.MFC0633_FlowRate',
                     'CA063.MFC0634_FlowRate', 'CA063.MFC0635_FlowRate', 'CA063.MFC0636_FlowRate',
                     'CA063.MFC0637_FlowRate', 'CA064.MFC0641_FlowRate', 'CA064.MFC0642_FlowRate',
                     'CA064.MFC0643_FlowRate', 'CA064.MFC0644_FlowRate', 'CA064.MFC0645_FlowRate',
                     'CA064.MFC0646_FlowRate', 'CA064.MFC0647_FlowRate', 'COMMON_2.DG061_Pressure',
                     'COMMON_2.PiG061_Pressure', 'CA061.AC061_MF_Volt', 'CA062.AC062_MF_Volt', 'CA063.AC063_MF_Volt',
                     'CA064.AC064_MF_Volt', 'EC01_SP2_FRGA.AMU_45', 'EC01_SP2_FRGA.Argon_20',
                     'EC01_SP2_FRGA.CarbonDioxide_44', 'EC01_SP2_FRGA.MPOil_43', 'EC01_SP2_FRGA.Nitro_28',
                     'EC01_SP2_FRGA.Oxygen_32', 'EC01_SP2_FRGA.Water_17', 'EC01_SP2_FRGA.Water_18',
                     'CA061.MR0611_ActualTorqueValue', 'CA061.MR0612_ActualTorqueValue',
                     'CA062.MR0621_ActualTorqueValue', 'CA062.MR0622_ActualTorqueValue',
                     'CA063.MR0631_ActualTorqueValue', 'CA063.MR0632_ActualTorqueValue',
                     'CA061.MR0611_ActualSpeedValue', 'CA061.MR0612_ActualSpeedValue', 'CA062.MR0621_ActualSpeedValue',
                     'CA062.MR0622_ActualSpeedValue', 'CA063.MR0631_ActualSpeedValue', 'CA063.MR0632_ActualSpeedValue',
                     'CA064.MR0641_ActualSpeedValue', 'CA064.MR0642_ActualSpeedValue']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 1
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP2 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP2 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP2 Measurement Start Time', 'SP2 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP2 Measurement Start Time', 'SP2 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP3(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

            Select *
            FROM OPENQUERY(INSQL, "SELECT
            [CA081.MF_Power],[CA082.MF_Power],[CA083.MF_Power],[CA084.MF_Power],[CA081.TMP1],[CA081.TMP2],[CA081.TMP3],[CA082.TMP1],[CA082.TMP2],[CA082.TMP3],[CA083.TMP1],[CA083.TMP2],[CA083.TMP3],[CA084.TMP1],[CA084.TMP2],[CA084.TMP3],[CA081.MFC1],[CA081.MFC2],[CA081.MFC3],[CA081.MFC4],[CA081.MFC5],[CA081.MFC6],[CA081.MFC7],[CA082.MFC1],[CA082.MFC2],[CA082.MFC3],[CA082.MFC4],[CA082.MFC5],[CA082.MFC6],[CA082.MFC7],[CA083.MFC1],[CA083.MFC2],[CA083.MFC3],[CA083.MFC4],[CA083.MFC5],[CA083.MFC6],[CA083.MFC7],[CA084.MFC1],[CA084.MFC2],[CA084.MFC3],[CA084.MFC4],[CA084.MFC5],[CA084.MFC6],[CA084.MFC7],[COMMON_3.DG081_Pressure],[COMMON_3.PiG081_Pressure],[CA081.MF_Volt],[CA082.MF_Volt],[CA083.MF_Volt],[CA084.MF_Volt],[EC01_SP3_FRGA.AMU_45],[EC01_SP3_FRGA.Argon_20],[EC01_SP3_FRGA.CarbonDioxide_44],[EC01_SP3_FRGA.MPOil_43],[EC01_SP3_FRGA.Nitro_28],[EC01_SP3_FRGA.Oxygen_32],[EC01_SP3_FRGA.Water_17],[EC01_SP3_FRGA.Water_18],[CA081.MR1_Actual_Torque],[CA081.MR2_Actual_Torque],[CA082.MR1_Actual_Torque],[CA082.MR2_Actual_Torque],[CA083.MR1_Actual_Torque],[CA083.MR2_Actual_Torque],[CA084.MR1_Actual_Torque],[CA084.MR2_Actual_Torque],[CA081.MR1_Actual_Speed],[CA081.MR2_Actual_Speed],[CA082.MR1_Actual_Speed],[CA082.MR2_Actual_Speed],[CA083.MR1_Actual_Speed],[CA083.MR2_Actual_Speed],[CA084.MR1_Actual_Speed],[CA084.MR2_Actual_Speed]
            ,StartDateTime
            ,DateTime
            FROM WideHistory
            WHERE wwRetrievalMode = 'Cyclic'
            AND wwResolution = 3000

            AND DateTime >= '{startDate}'
            AND DateTime <='{endDate}'")Hist
            """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA081.MF_Power', 'CA082.MF_Power', 'CA083.MF_Power', 'CA084.MF_Power', 'CA081.TMP1',
                     'CA081.TMP2', 'CA081.TMP3', 'CA082.TMP1', 'CA082.TMP2', 'CA082.TMP3', 'CA083.TMP1', 'CA083.TMP2',
                     'CA083.TMP3', 'CA084.TMP1', 'CA084.TMP2', 'CA084.TMP3', 'CA081.MFC1', 'CA081.MFC2', 'CA081.MFC3',
                     'CA081.MFC4', 'CA081.MFC5', 'CA081.MFC6', 'CA081.MFC7', 'CA082.MFC1', 'CA082.MFC2', 'CA082.MFC3',
                     'CA082.MFC4', 'CA082.MFC5', 'CA082.MFC6', 'CA082.MFC7', 'CA083.MFC1', 'CA083.MFC2', 'CA083.MFC3',
                     'CA083.MFC4', 'CA083.MFC5', 'CA083.MFC6', 'CA083.MFC7', 'CA084.MFC1', 'CA084.MFC2', 'CA084.MFC3',
                     'CA084.MFC4', 'CA084.MFC5', 'CA084.MFC6', 'CA084.MFC7', 'COMMON_3.DG081_Pressure',
                     'COMMON_3.PiG081_Pressure', 'CA081.MF_Volt', 'CA082.MF_Volt', 'CA083.MF_Volt', 'CA084.MF_Volt',
                     'EC01_SP3_FRGA.AMU_45', 'EC01_SP3_FRGA.Argon_20', 'EC01_SP3_FRGA.CarbonDioxide_44',
                     'EC01_SP3_FRGA.MPOil_43', 'EC01_SP3_FRGA.Nitro_28', 'EC01_SP3_FRGA.Oxygen_32',
                     'EC01_SP3_FRGA.Water_17', 'EC01_SP3_FRGA.Water_18','CA081.MR1_Actual_Torque','CA081.MR2_Actual_Torque','CA082.MR1_Actual_Torque','CA082.MR2_Actual_Torque','CA083.MR1_Actual_Torque','CA083.MR2_Actual_Torque','CA084.MR1_Actual_Torque','CA084.MR2_Actual_Torque','CA081.MR1_Actual_Speed','CA081.MR2_Actual_Speed','CA082.MR1_Actual_Speed','CA083.MR1_Actual_Speed','CA083.MR2_Actual_Speed','CA084.MR1_Actual_Speed','CA084.MR2_Actual_Speed']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 1
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP3 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP3 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP3 Measurement Start Time', 'SP3 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP3 Measurement Start Time', 'SP3 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP4(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

            Select *
            FROM OPENQUERY(INSQL, "SELECT
            [CA101.AC101_MF_Power],[CA102.AC102_MF_Power],[CA103.AC103_MF_Power],[CA104.AC104_MF_Power],[CA105.AC105_MF_Power],[CA106.AC106_MF_Power],[CA107.AC107_MF_Power],[CA108.AC108_MF_Power],[RCEN4.CA1PowerFeedback],[RCEN4.CA2PowerFeedback],[RCEN4.CA3PowerFeedback],[CA101.TMP1011_Rotation],[CA101.TMP1012_Rotation],[CA101.TMP1013_Rotation],[CA102.TMP1021_Rotation],[CA102.TMP1022_Rotation],[CA102.TMP1023_Rotation],[CA103.TMP1031_Rotation],[CA103.TMP1032_Rotation],[CA103.TMP1033_Rotation],[CA104.TMP1041_Rotation],[CA104.TMP1042_Rotation],[CA104.TMP1043_Rotation],[CA105.TMP1051_Rotation],[CA105.TMP1052_Rotation],[CA105.TMP1053_Rotation],[CA106.TMP1061_Rotation],[CA106.TMP1062_Rotation],[CA106.TMP1063_Rotation],[CA107.TMP1071_Rotation],[CA107.TMP1072_Rotation],[CA107.TMP1073_Rotation],[CA108.TMP1081_Rotation],[CA108.TMP1082_Rotation],[CA108.TMP1083_Rotation],[RCEN4.TMP11Feedback],[RCEN4.TMP12Feedback],[RCEN4.TMP13Feedback],[RCEN4.TMP21Feedback],[RCEN4.TMP22Feedback],[RCEN4.TMP23Feedback],[RCEN4.TMP31Feedback],[RCEN4.TMP32Feedback],[RCEN4.TMP33Feedback],[CA101.MFC1011_FlowRate],[CA101.MFC1012_FlowRate],[CA101.MFC1013_FlowRate],[CA101.MFC1014_FlowRate],[CA101.MFC1015_FlowRate],[CA101.MFC1016_FlowRate],[CA101.MFC1017_FlowRate],[CA102.MFC1021_FlowRate],[CA102.MFC1022_FlowRate],[CA102.MFC1023_FlowRate],[CA102.MFC1024_FlowRate],[CA102.MFC1025_FlowRate],[CA102.MFC1026_FlowRate],[CA102.MFC1027_FlowRate],[CA103.MFC1031_FlowRate],[CA103.MFC1032_FlowRate],[CA103.MFC1033_FlowRate],[CA103.MFC1034_FlowRate],[CA103.MFC1035_FlowRate],[CA103.MFC1036_FlowRate],[CA103.MFC1037_FlowRate],[CA104.MFC1041_FlowRate],[CA104.MFC1042_FlowRate],[CA104.MFC1043_FlowRate],[CA104.MFC1044_FlowRate],[CA104.MFC1045_FlowRate],[CA104.MFC1046_FlowRate],[CA104.MFC1047_FlowRate],[CA105.MFC1051_FlowRate],[CA105.MFC1052_FlowRate],[CA105.MFC1053_FlowRate],[CA105.MFC1054_FlowRate],[CA105.MFC1055_FlowRate],[CA105.MFC1056_FlowRate],[CA105.MFC1057_FlowRate],[CA106.MFC1061_FlowRate],[CA106.MFC1062_FlowRate],[CA106.MFC1063_FlowRate],[CA106.MFC1064_FlowRate],[CA106.MFC1065_FlowRate],[CA106.MFC1066_FlowRate],[CA106.MFC1067_FlowRate],[CA107.MFC1071_FlowRate],[CA107.MFC1072_FlowRate],[CA107.MFC1073_FlowRate],[CA107.MFC1074_FlowRate],[CA107.MFC1075_FlowRate],[CA107.MFC1076_FlowRate],[CA107.MFC1077_FlowRate],[CA108.MFC1081_FlowRate],[CA108.MFC1082_FlowRate],[CA108.MFC1083_FlowRate],[CA108.MFC1084_FlowRate],[CA108.MFC1085_FlowRate],[CA108.MFC1086_FlowRate],[CA108.MFC1087_FlowRate],[COMMON_4.DG101_Pressure],[COMMON_4.PiG101_Pressure],[CA101.AC101_MF_Volt],[CA102.AC102_MF_Volt],[CA103.AC103_MF_Volt],[CA104.AC104_MF_Volt],[CA105.AC105_MF_Volt],[CA106.AC106_MF_Volt],[CA107.AC107_MF_Volt],[CA108.AC108_MF_Volt],[RCEN4.CA1VoltageFeedback],[RCEN4.CA2VoltageFeedback],[RCEN4.CA3VoltageFeedback],[EC01_SP4_FRGA.AMU_45],[EC01_SP4_FRGA.Argon_20],[EC01_SP4_FRGA.CarbonDioxide_44],[EC01_SP4_FRGA.MPOil_43],[EC01_SP4_FRGA.Nitro_28],[EC01_SP4_FRGA.Oxygen_32],[EC01_SP4_FRGA.Water_17],[EC01_SP4_FRGA.Water_18],[CA101.MFC1011_ActualTorqueValue],[CA101.MFC1012_ActualTorqueValue],[CA102.MFC1021_ActualTorqueValue],[CA102.MFC1022_ActualTorqueValue],[CA103.MFC1031_ActualTorqueValue],[CA103.MFC1032_ActualTorqueValue],[CA104.MFC1041_ActualTorqueValue],[CA104.MFC1042_ActualTorqueValue],[CA105.MFC1051_ActualTorqueValue],[CA105.MFC1052_ActualTorqueValue],[CA106.MFC1061_ActualTorqueValue],[CA106.MFC1062_ActualTorqueValue],[CA107.MFC1071_ActualTorqueValue],[CA107.MFC1072_ActualTorqueValue],[CA108.MFC1081_ActualTorqueValue],[CA108.MFC1082_ActualTorqueValue],[RCEN4.MR11TorqueFeedback],[RCEN4.MR12TorqueFeedback],[RCEN4.MR21TorqueFeedback],[RCEN4.MR22TorqueFeedback],[RCEN4.MR31TorqueFeedback],[RCEN4.MR32TorqueFeedback],[CA101.MR1011_ActualSpeedValue],[CA101.MR1012_ActualSpeedValue],[CA102.MR1021_ActualSpeedValue],[CA102.MR1022_ActualSpeedValue],[CA103.MR1031_ActualSpeedValue],[CA103.MR1032_ActualSpeedValue],[CA104.MR1041_ActualSpeedValue],[CA104.MR1042_ActualSpeedValue],[CA105.MR1051_ActualSpeedValue],[CA105.MR1052_ActualSpeedValue],[CA106.MR1061_ActualSpeedValue],[CA106.MR1062_ActualSpeedValue],[CA107.MR1071_ActualSpeedValue],[CA107.MR1072_ActualSpeedValue],[CA108.MR1081_ActualSpeedValue],[CA108.MR1082_ActualSpeedValue],[RCEN4.MR11SpeedFeedback],[RCEN4.MR12SpeedFeedback],[RCEN4.MR21SpeedFeedback],[RCEN4.MR22SpeedFeedback],[RCEN4.MR31SpeedFeedback],[RCEN4.MR32SpeedFeedback]            ,StartDateTime
            ,DateTime
            FROM WideHistory
            WHERE wwRetrievalMode = 'Cyclic'
            AND wwResolution = 3000

            AND DateTime >= '{startDate}'
            AND DateTime <='{endDate}'")Hist
            """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA101.AC101_MF_Power', 'CA102.AC102_MF_Power', 'CA103.AC103_MF_Power',
                     'CA104.AC104_MF_Power', 'CA105.AC105_MF_Power', 'CA106.AC106_MF_Power', 'CA107.AC107_MF_Power',
                     'CA108.AC108_MF_Power', 'RCEN4.CA1PowerFeedback', 'RCEN4.CA2PowerFeedback',
                     'RCEN4.CA3PowerFeedback', 'CA101.TMP1011_Rotation', 'CA101.TMP1012_Rotation',
                     'CA101.TMP1013_Rotation', 'CA102.TMP1021_Rotation', 'CA102.TMP1022_Rotation',
                     'CA102.TMP1023_Rotation', 'CA103.TMP1031_Rotation', 'CA103.TMP1032_Rotation',
                     'CA103.TMP1033_Rotation', 'CA104.TMP1041_Rotation', 'CA104.TMP1042_Rotation',
                     'CA104.TMP1043_Rotation', 'CA105.TMP1051_Rotation', 'CA105.TMP1052_Rotation',
                     'CA105.TMP1053_Rotation', 'CA106.TMP1061_Rotation', 'CA106.TMP1062_Rotation',
                     'CA106.TMP1063_Rotation', 'CA107.TMP1071_Rotation', 'CA107.TMP1072_Rotation',
                     'CA107.TMP1073_Rotation', 'CA108.TMP1081_Rotation', 'CA108.TMP1082_Rotation',
                     'CA108.TMP1083_Rotation', 'RCEN4.TMP11Feedback', 'RCEN4.TMP12Feedback', 'RCEN4.TMP13Feedback',
                     'RCEN4.TMP21Feedback', 'RCEN4.TMP22Feedback', 'RCEN4.TMP23Feedback', 'RCEN4.TMP31Feedback',
                     'RCEN4.TMP32Feedback', 'RCEN4.TMP33Feedback', 'CA101.MFC1011_FlowRate', 'CA101.MFC1012_FlowRate',
                     'CA101.MFC1013_FlowRate', 'CA101.MFC1014_FlowRate', 'CA101.MFC1015_FlowRate',
                     'CA101.MFC1016_FlowRate', 'CA101.MFC1017_FlowRate', 'CA102.MFC1021_FlowRate',
                     'CA102.MFC1022_FlowRate', 'CA102.MFC1023_FlowRate', 'CA102.MFC1024_FlowRate',
                     'CA102.MFC1025_FlowRate', 'CA102.MFC1026_FlowRate', 'CA102.MFC1027_FlowRate',
                     'CA103.MFC1031_FlowRate', 'CA103.MFC1032_FlowRate', 'CA103.MFC1033_FlowRate',
                     'CA103.MFC1034_FlowRate', 'CA103.MFC1035_FlowRate', 'CA103.MFC1036_FlowRate',
                     'CA103.MFC1037_FlowRate', 'CA104.MFC1041_FlowRate', 'CA104.MFC1042_FlowRate',
                     'CA104.MFC1043_FlowRate', 'CA104.MFC1044_FlowRate', 'CA104.MFC1045_FlowRate',
                     'CA104.MFC1046_FlowRate', 'CA104.MFC1047_FlowRate', 'CA105.MFC1051_FlowRate',
                     'CA105.MFC1052_FlowRate', 'CA105.MFC1053_FlowRate', 'CA105.MFC1054_FlowRate',
                     'CA105.MFC1055_FlowRate', 'CA105.MFC1056_FlowRate', 'CA105.MFC1057_FlowRate',
                     'CA106.MFC1061_FlowRate', 'CA106.MFC1062_FlowRate', 'CA106.MFC1063_FlowRate',
                     'CA106.MFC1064_FlowRate', 'CA106.MFC1065_FlowRate', 'CA106.MFC1066_FlowRate',
                     'CA106.MFC1067_FlowRate', 'CA107.MFC1071_FlowRate', 'CA107.MFC1072_FlowRate',
                     'CA107.MFC1073_FlowRate', 'CA107.MFC1074_FlowRate', 'CA107.MFC1075_FlowRate',
                     'CA107.MFC1076_FlowRate', 'CA107.MFC1077_FlowRate', 'CA108.MFC1081_FlowRate',
                     'CA108.MFC1082_FlowRate', 'CA108.MFC1083_FlowRate', 'CA108.MFC1084_FlowRate',
                     'CA108.MFC1085_FlowRate', 'CA108.MFC1086_FlowRate', 'CA108.MFC1087_FlowRate',
                     'COMMON_4.DG101_Pressure', 'COMMON_4.PiG101_Pressure', 'CA101.AC101_MF_Volt',
                     'CA102.AC102_MF_Volt', 'CA103.AC103_MF_Volt', 'CA104.AC104_MF_Volt', 'CA105.AC105_MF_Volt',
                     'CA106.AC106_MF_Volt', 'CA107.AC107_MF_Volt', 'CA108.AC108_MF_Volt','RCEN4.CA1VoltageFeedback','RCEN4.CA2VoltageFeedback', 'RCEN4.CA3VoltageFeedback', 'EC01_SP4_FRGA.AMU_45',
                     'EC01_SP4_FRGA.Argon_20', 'EC01_SP4_FRGA.CarbonDioxide_44', 'EC01_SP4_FRGA.MPOil_43',
                     'EC01_SP4_FRGA.Nitro_28', 'EC01_SP4_FRGA.Oxygen_32', 'EC01_SP4_FRGA.Water_17',
                     'EC01_SP4_FRGA.Water_18','CA101.MFC1011_ActualTorqueValue','CA101.MFC1012_ActualTorqueValue','CA102.MFC1021_ActualTorqueValue','CA102.MFC1022_ActualTorqueValue','CA103.MFC1031_ActualTorqueValue','CA103.MFC1032_ActualTorqueValue','CA104.MFC1041_ActualTorqueValue','CA104.MFC1042_ActualTorqueValue','CA105.MFC1051_ActualTorqueValue','CA105.MFC1052_ActualTorqueValue','CA106.MFC1061_ActualTorqueValue','CA106.MFC1062_ActualTorqueValue','CA107.MFC1071_ActualTorqueValue','CA107.MFC1072_ActualTorqueValue','CA108.MFC1081_ActualTorqueValue','CA108.MFC1082_ActualTorqueValue','RCEN4.MR11TorqueFeedback','RCEN4.MR12TorqueFeedback','RCEN4.MR21TorqueFeedback','RCEN4.MR22TorqueFeedback','RCEN4.MR31TorqueFeedback','RCEN4.MR32TorqueFeedback','CA101.MR1011_ActualSpeedValue','CA101.MR1012_ActualSpeedValue','CA102.MR1021_ActualSpeedValue','CA102.MR1022_ActualSpeedValue','CA103.MR1031_ActualSpeedValue','CA103.MR1032_ActualSpeedValue','CA104.MR1041_ActualSpeedValue','CA104.MR1042_ActualSpeedValue','CA105.MR1051_ActualSpeedValue','CA105.MR1052_ActualSpeedValue','CA106.MR1061_ActualSpeedValue','CA106.MR1062_ActualSpeedValue','CA107.MR1071_ActualSpeedValue','CA107.MR1072_ActualSpeedValue','CA108.MR1081_ActualSpeedValue','CA108.MR1082_ActualSpeedValue','RCEN4.MR11SpeedFeedback','RCEN4.MR12SpeedFeedback','RCEN4.MR21SpeedFeedback','RCEN4.MR22SpeedFeedback','RCEN4.MR31SpeedFeedback','RCEN4.MR32SpeedFeedback']

    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 2.5
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP4 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP4 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP4 Measurement Start Time', 'SP4 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP4 Measurement Start Time', 'SP4 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP5(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

            Select *
            FROM OPENQUERY(INSQL, "SELECT
            [CA121.DC121_Power],[CA124.DC124_Power],[CA125.DC125_Power],[CA126.DC126_Power],[CA127.DC127_Power],[CA128.DC128_Power],[CA129.CA_kW_FB],[CA130.CA_kW_FB],[CA131.CA_kW_FB],[CA132.CA_kW_FB],[COMMON_5.TMP1211_Rotation],[COMMON_5.TMP1212_Rotation],[COMMON_5.TMP1213_Rotation],[COMMON_5.TMP1214_Rotation],[COMMON_5.TMP1221_Rotation],[COMMON_5.TMP1222_Rotation],[COMMON_5.TMP1223_Rotation],[COMMON_5.TMP1224_Rotation],[PVD01_MFC1211.MFC01],[PVD01_MFC1211.MFC02],[PVD01_MFC1211.MFC03],[PVD01_MFC1211.MFC04],[PVD01_MFC1211.MFC05],[PVD01_MFC1211.MFC06],[PVD01_MFC1211.MFC07],[PVD01_MFC1211.MFC08],[PVD01_MFC1211.MFC09],[PVD01_MFC1211.MFC10],[PVD01_MFC1212.MFC01],[PVD01_MFC1212.MFC02],[PVD01_MFC1212.MFC03],[PVD01_MFC1212.MFC04],[PVD01_MFC1212.MFC05],[PVD01_MFC1212.MFC06],[PVD01_MFC1212.MFC07],[PVD01_MFC1212.MFC08],[PVD01_MFC1212.MFC09],[PVD01_MFC1212.MFC10],[PVD01_MFC1213.MFC01],[PVD01_MFC1213.MFC02],[PVD01_MFC1213.MFC03],[PVD01_MFC1213.MFC04],[PVD01_MFC1213.MFC05],[PVD01_MFC1213.MFC06],[PVD01_MFC1213.MFC07],[PVD01_MFC1213.MFC08],[PVD01_MFC1213.MFC09],[PVD01_MFC1213.MFC10],[PVD01_MFC1214.MFC01],[PVD01_MFC1214.MFC02],[PVD01_MFC1214.MFC03],[PVD01_MFC1214.MFC04],[PVD01_MFC1214.MFC05],[PVD01_MFC1214.MFC06],[PVD01_MFC1214.MFC07],[PVD01_MFC1214.MFC08],[PVD01_MFC1214.MFC09],[PVD01_MFC1214.MFC10],[COMMON_5.MFC1213_FlowRate],[COMMON_5.MFC1214_FlowRate],[COMMON_5.DG121_Pressure_Torr],[COMMON_5.PiG121_Pressure],[CA121.DC121_Volt],[CA124.DC124_Volt],[CA125.DC125_Volt],[CA126.DC126_Volt],[CA127.DC127_Volt],[CA128.DC128_Volt],[CA129.CA_Volt_FB], [CA130.CA_Volt_FB],[CA131.CA_Volt_FB],[CA132.CA_Volt_FB],[EC01_SP5_FRGA.AMU_45],[EC01_SP5_FRGA.Argon_20],[EC01_SP5_FRGA.CarbonDioxide_44],[EC01_SP5_FRGA.MPOil_43],[EC01_SP5_FRGA.Nitro_28],[EC01_SP5_FRGA.Oxygen_32],[EC01_SP5_FRGA.Water_17],[EC01_SP5_FRGA.Water_18],[EC01_SP5_FRGA2.AMU_45],[EC01_SP5_FRGA2.Argon_20],[EC01_SP5_FRGA2.CarbonDioxide_44],[EC01_SP5_FRGA2.MPOil_43],[EC01_SP5_FRGA2.Nitro_28],[EC01_SP5_FRGA2.Oxygen_32],[EC01_SP5_FRGA2.Water_17],[EC01_SP5_FRGA2.Water_18]
            ,StartDateTime
            ,DateTime
            FROM WideHistory
            WHERE wwRetrievalMode = 'Cyclic'
            AND wwResolution = 3000

            AND DateTime >= '{startDate}'
            AND DateTime <='{endDate}'")Hist
            """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA121.DC121_Power', 'CA124.DC124_Power', 'CA125.DC125_Power', 'CA126.DC126_Power',
                     'CA127.DC127_Power', 'CA128.DC128_Power', 'CA129.CA_kW_FB', 'CA130.CA_kW_FB', 'CA131.CA_kW_FB',
                     'CA132.CA_kW_FB', 'COMMON_5.TMP1211_Rotation', 'COMMON_5.TMP1212_Rotation',
                     'COMMON_5.TMP1213_Rotation', 'COMMON_5.TMP1214_Rotation', 'COMMON_5.TMP1221_Rotation',
                     'COMMON_5.TMP1222_Rotation', 'COMMON_5.TMP1223_Rotation', 'COMMON_5.TMP1224_Rotation',
                     'PVD01_MFC1211.MFC01', 'PVD01_MFC1211.MFC02', 'PVD01_MFC1211.MFC03', 'PVD01_MFC1211.MFC04',
                     'PVD01_MFC1211.MFC05', 'PVD01_MFC1211.MFC06', 'PVD01_MFC1211.MFC07', 'PVD01_MFC1211.MFC08',
                     'PVD01_MFC1211.MFC09', 'PVD01_MFC1211.MFC10', 'PVD01_MFC1212.MFC01', 'PVD01_MFC1212.MFC02',
                     'PVD01_MFC1212.MFC03', 'PVD01_MFC1212.MFC04', 'PVD01_MFC1212.MFC05', 'PVD01_MFC1212.MFC06',
                     'PVD01_MFC1212.MFC07', 'PVD01_MFC1212.MFC08', 'PVD01_MFC1212.MFC09', 'PVD01_MFC1212.MFC10',
                     'PVD01_MFC1213.MFC01', 'PVD01_MFC1213.MFC02', 'PVD01_MFC1213.MFC03', 'PVD01_MFC1213.MFC04',
                     'PVD01_MFC1213.MFC05', 'PVD01_MFC1213.MFC06', 'PVD01_MFC1213.MFC07', 'PVD01_MFC1213.MFC08',
                     'PVD01_MFC1213.MFC09', 'PVD01_MFC1213.MFC10', 'PVD01_MFC1214.MFC01', 'PVD01_MFC1214.MFC02',
                     'PVD01_MFC1214.MFC03', 'PVD01_MFC1214.MFC04', 'PVD01_MFC1214.MFC05', 'PVD01_MFC1214.MFC06',
                     'PVD01_MFC1214.MFC07', 'PVD01_MFC1214.MFC08', 'PVD01_MFC1214.MFC09', 'PVD01_MFC1214.MFC10',
                     'COMMON_5.MFC1213_FlowRate', 'COMMON_5.MFC1214_FlowRate', 'COMMON_5.DG121_Pressure_Torr',
                     'COMMON_5.PiG121_Pressure', 'CA121.DC121_Volt', 'CA124.DC124_Volt', 'CA125.DC125_Volt',
                     'CA126.DC126_Volt', 'CA127.DC127_Volt', 'CA128.DC128_Volt', 'CA129.CA_Volt_FB', 'CA130.CA_Volt_FB','CA131.CA_Volt_FB','CA132.CA_Volt_FB', 'EC01_SP5_FRGA.AMU_45',
                     'EC01_SP5_FRGA.Argon_20', 'EC01_SP5_FRGA.CarbonDioxide_44', 'EC01_SP5_FRGA.MPOil_43',
                     'EC01_SP5_FRGA.Nitro_28', 'EC01_SP5_FRGA.Oxygen_32', 'EC01_SP5_FRGA.Water_17',
                     'EC01_SP5_FRGA.Water_18','EC01_SP5_FRGA2.AMU_45','EC01_SP5_FRGA2.Argon_20','EC01_SP5_FRGA2.CarbonDioxide_44','EC01_SP5_FRGA2.MPOil_43','EC01_SP5_FRGA2.Nitro_28','EC01_SP5_FRGA2.Oxygen_32','EC01_SP5_FRGA2.Water_17','EC01_SP5_FRGA2.Water_18']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 1.5
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP5 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP5 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP5 Measurement Start Time', 'SP5 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP5 Measurement Start Time', 'SP5 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP6(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5

    Query = f"""SET QUOTED_IDENTIFIER OFF

                Select *
                FROM OPENQUERY(INSQL, "SELECT
                [CA143.AC143MF_Power],[CA144.AC144MF_Power],[CA141.TMP1411_Rotation],[CA141.TMP1412_Rotation],[CA141.TMP1413_Rotation],[CA142.TMP1421_Rotation],[CA142.TMP1422_Rotation],[CA142.TMP1423_Rotation],[CA143.TMP1431_Rotation],[CA143.TMP1432_Rotation],[CA143.TMP1433_Rotation],[CA144.TMP1441_Rotation],[CA144.TMP1442_Rotation],[CA144.TMP1443_Rotation],[CA141.MFC1411_FlowRate],[CA141.MFC1412_FlowRate],[CA141.MFC1413_FlowRate],[CA141.MFC1414_FlowRate],[CA141.MFC1415_FlowRate],[CA141.MFC1416_FlowRate],[CA141.MFC1417_FlowRate],[CA142.MFC1421_FlowRate],[CA142.MFC1422_FlowRate],[CA142.MFC1423_FlowRate],[CA142.MFC1424_FlowRate],[CA142.MFC1425_FlowRate],[CA142.MFC1426_FlowRate],[CA142.MFC1427_FlowRate],[CA143.MFC1431_FlowRate],[CA143.MFC1432_FlowRate],[CA143.MFC1433_FlowRate],[CA143.MFC1434_FlowRate],[CA143.MFC1435_FlowRate],[CA143.MFC1436_FlowRate],[CA143.MFC1437_FlowRate],[CA144.MFC1441_FlowRate],[CA144.MFC1442_FlowRate],[CA144.MFC1443_FlowRate],[CA144.MFC1444_FlowRate],[CA144.MFC1445_FlowRate],[CA144.MFC1446_FlowRate],[CA144.MFC1447_FlowRate],[COMMON_6.DG141_Pressure],[COMMON_6.PiG141_Pressure_Torr_Exponent],[CA143.AC143MF_Volt],[CA144.AC144MF_Volt],[EC01_SP6_FRGA.AMU_45],[EC01_SP6_FRGA.Argon_20],[EC01_SP6_FRGA.CarbonDioxide_44],[EC01_SP6_FRGA.MPOil_43],[EC01_SP6_FRGA.Nitro_28],[EC01_SP6_FRGA.Oxygen_32],[EC01_SP6_FRGA.Water_17],[EC01_SP6_FRGA.Water_18],[CA143.MR1431_ActualTorqueValue],[CA143.MR1432_ActualTorqueValue],[CA144.MR1441_ActualTorqueValue],[CA144.MR1442_ActualTorqueValue],[CA143.MR1431_ActualSpeedValue],[CA143.MR1432_ActualSpeedValue],[CA144.MR1441_ActualSpeedValue],[CA144.MR1442_ActualSpeedValue]
                ,StartDateTime
                ,DateTime
                FROM WideHistory
                WHERE wwRetrievalMode = 'Cyclic'
                AND wwResolution = 3000

                AND DateTime >= '{startDate}'
                AND DateTime <='{endDate}'")Hist
                """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA143.AC143MF_Power', 'CA144.AC144MF_Power', 'CA141.TMP1411_Rotation',
                     'CA141.TMP1412_Rotation', 'CA141.TMP1413_Rotation', 'CA142.TMP1421_Rotation',
                     'CA142.TMP1422_Rotation', 'CA142.TMP1423_Rotation', 'CA143.TMP1431_Rotation',
                     'CA143.TMP1432_Rotation', 'CA143.TMP1433_Rotation', 'CA144.TMP1441_Rotation',
                     'CA144.TMP1442_Rotation', 'CA144.TMP1443_Rotation', 'CA141.MFC1411_FlowRate',
                     'CA141.MFC1412_FlowRate', 'CA141.MFC1413_FlowRate', 'CA141.MFC1414_FlowRate',
                     'CA141.MFC1415_FlowRate', 'CA141.MFC1416_FlowRate', 'CA141.MFC1417_FlowRate',
                     'CA142.MFC1421_FlowRate', 'CA142.MFC1422_FlowRate', 'CA142.MFC1423_FlowRate',
                     'CA142.MFC1424_FlowRate', 'CA142.MFC1425_FlowRate', 'CA142.MFC1426_FlowRate',
                     'CA142.MFC1427_FlowRate', 'CA143.MFC1431_FlowRate', 'CA143.MFC1432_FlowRate',
                     'CA143.MFC1433_FlowRate', 'CA143.MFC1434_FlowRate', 'CA143.MFC1435_FlowRate',
                     'CA143.MFC1436_FlowRate', 'CA143.MFC1437_FlowRate', 'CA144.MFC1441_FlowRate',
                     'CA144.MFC1442_FlowRate', 'CA144.MFC1443_FlowRate', 'CA144.MFC1444_FlowRate',
                     'CA144.MFC1445_FlowRate', 'CA144.MFC1446_FlowRate', 'CA144.MFC1447_FlowRate',
                     'COMMON_6.DG141_Pressure', 'COMMON_6.PiG141_Pressure_Torr_Exponent', 'CA143.AC143MF_Volt',
                     'CA144.AC144MF_Volt','RCEN7.CA1VoltageFeedback','RCEN7.CA2VoltageFeedback', 'RCEN7.CA3VoltageFeedback', 'EC01_SP6_FRGA.AMU_45', 'EC01_SP6_FRGA.Argon_20',
                     'EC01_SP6_FRGA.CarbonDioxide_44', 'EC01_SP6_FRGA.MPOil_43', 'EC01_SP6_FRGA.Nitro_28',
                     'EC01_SP6_FRGA.Oxygen_32', 'EC01_SP6_FRGA.Water_17', 'EC01_SP6_FRGA.Water_18','CA143.MR1431_ActualTorqueValue','CA143.MR1432_ActualTorqueValue','CA144.MR1441_ActualTorqueValue','CA144.MR1442_ActualTorqueValue','CA143.MR1431_ActualSpeedValue','CA143.MR1432_ActualSpeedValue','CA144.MR1441_ActualSpeedValue','CA144.MR1442_ActualSpeedValue']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 1
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP6 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP6 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP6 Measurement Start Time', 'SP6 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP6 Measurement Start Time', 'SP6 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullSP7(startDate, endDate, Takt):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5
    Query = f"""SET QUOTED_IDENTIFIER OFF

                Select *
                FROM OPENQUERY(INSQL, "SELECT
               [CA161.DC1611_Power],[CA161.DC1612_Power],[CA162.DC1621_Power],[CA162.DC1622_Power],[CA163.DC1631_Power],[CA163.DC1632_Power],[CA164.DC1641_Power],[CA164.DC1642_Power],[RCEN7.CA11PowerFeedback],[RCEN7.CA12PowerFeedback],[RCEN7.CA21PowerFeedback],[RCEN7.CA22PowerFeedback],[RCEN7.CA31PowerFeedback],[RCEN7.CA32PowerFeedback],[CA161.TMP1611_Rotation],[CA161.TMP1612_Rotation],[CA161.TMP1613_Rotation],[CA162.TMP1621_Rotation],[CA162.TMP1622_Rotation],[CA162.TMP1623_Rotation],[CA163.TMP1631_Rotation],[CA163.TMP1632_Rotation],[CA163.TMP1633_Rotation],[CA164.TMP1641_Rotation],[CA164.TMP1642_Rotation],[CA164.TMP1643_Rotation],[RCEN7.TMP11Feedback],[RCEN7.TMP12Feedback],[RCEN7.TMP13Feedback],[RCEN7.TMP21Feedback],[RCEN7.TMP22Feedback],[RCEN7.TMP23Feedback],[RCEN7.TMP31Feedback],[RCEN7.TMP32Feedback],[RCEN7.TMP33Feedback],[CA161.MFC1611_FlowRate],[CA161.MFC1612_FlowRate],[CA161.MFC1613_FlowRate],[CA161.MFC1614_FlowRate],[CA161.MFC1615_FlowRate],[CA161.MFC1616_FlowRate],[CA161.MFC1617_FlowRate],[CA162.MFC1621_FlowRate],[CA162.MFC1622_FlowRate],[CA162.MFC1623_FlowRate],[CA162.MFC1624_FlowRate],[CA162.MFC1625_FlowRate],[CA162.MFC1626_FlowRate],[CA162.MFC1627_FlowRate],[CA163.MFC1631_FlowRate],[CA163.MFC1632_FlowRate],[CA163.MFC1633_FlowRate],[CA163.MFC1634_FlowRate],[CA163.MFC1635_FlowRate],[CA163.MFC1636_FlowRate],[CA163.MFC1637_FlowRate],[CA164.MFC1641_FlowRate],[CA164.MFC1642_FlowRate],[CA164.MFC1643_FlowRate],[CA164.MFC1644_FlowRate],[CA164.MFC1645_FlowRate],[CA164.MFC1646_FlowRate],[CA164.MFC1647_FlowRate],[RCEN7.MFC11Feedback],[RCEN7.MFC12Feedback],[RCEN7.MFC21Feedback],[RCEN7.MFC22Feedback],[RCEN7.MFC31Feedback],[RCEN7.MFC32Feedback],[COMMON_7.DG161_Pressure_Torr],[COMMON_7.PiG161_Pressure_Torr_Exponent],[CA161.DC1611_Volt],[CA162.DC1621_Volt],[CA163.DC1631_Volt],[CA164.DC1641_Volt],[RCEN7.CA11VoltageFeedback],[RCEN7.CA12VoltageFeedback],[RCEN7.CA21VoltageFeedback],[RCEN7.CA22VoltageFeedback],[RCEN7.CA31VoltageFeedback],[RCEN7.CA32VoltageFeedback],[EC01_SP7_FRGA.AMU_45],[EC01_SP7_FRGA.Argon_20],[EC01_SP7_FRGA.CarbonDioxide_44],[EC01_SP7_FRGA.MPOil_43],[EC01_SP7_FRGA.Nitro_28],[EC01_SP7_FRGA.Oxygen_32],[EC01_SP7_FRGA.Water_17],[EC01_SP7_FRGA.Water_18],[CA161.MR1611_ActualTorqueValue],[CA161.MR1612_ActualTorqueValue],[CA162.MR1621_ActualTorqueValue],[CA162.MR1622_ActualTorqueValue],[CA163.MR1631_ActualTorqueValue],[CA163.MR1632_ActualTorqueValue],[CA164.MR1641_ActualTorqueValue],[CA164.MR1642_ActualTorqueValue],[RCEN7.MR11TorqueFeedback],[RCEN7.MR12TorqueFeedback],[RCEN7.MR21TorqueFeedback],[RCEN7.MR22TorqueFeedback],[RCEN7.MR31TorqueFeedback],[RCEN7.MR32TorqueFeedback],[CA161.MR1611_ActualSpeedValue],[CA161.MR1612_ActualSpeedValue],[CA162.MR1621_ActualSpeedValue],[CA162.MR1622_ActualSpeedValue],[CA163.MR1631_ActualSpeedValue],[CA163.MR1632_ActualSpeedValue],[CA164.MR1641_ActualSpeedValue],[CA164.MR1642_ActualSpeedValue],[RCEN7.MR11SpeedFeedback],[RCEN7.MR12SpeedFeedback],[RCEN7.MR21SpeedFeedback],[RCEN7.MR22SpeedFeedback],[RCEN7.MR31SpeedFeedback],[RCEN7.MR32SpeedFeedback]
                ,StartDateTime
                ,DateTime
                FROM WideHistory
                WHERE wwRetrievalMode = 'Cyclic'
                AND wwResolution = 3000

                AND DateTime >= '{startDate}'
                AND DateTime <='{endDate}'")Hist
                """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'CA161.DC1611_Power', 'CA161.DC1612_Power', 'CA162.DC1621_Power', 'CA162.DC1622_Power',
                     'CA163.DC1631_Power', 'CA163.DC1632_Power', 'CA164.DC1641_Power', 'CA164.DC1642_Power',
                     'RCEN7.CA11PowerFeedback', 'RCEN7.CA12PowerFeedback', 'RCEN7.CA21PowerFeedback',
                     'RCEN7.CA22PowerFeedback', 'RCEN7.CA31PowerFeedback', 'RCEN7.CA32PowerFeedback',
                     'CA161.TMP1611_Rotation', 'CA161.TMP1612_Rotation', 'CA161.TMP1613_Rotation',
                     'CA162.TMP1621_Rotation', 'CA162.TMP1622_Rotation', 'CA162.TMP1623_Rotation',
                     'CA163.TMP1631_Rotation', 'CA163.TMP1632_Rotation', 'CA163.TMP1633_Rotation',
                     'CA164.TMP1641_Rotation', 'CA164.TMP1642_Rotation', 'CA164.TMP1643_Rotation',
                     'RCEN7.TMP11Feedback', 'RCEN7.TMP12Feedback', 'RCEN7.TMP13Feedback', 'RCEN7.TMP21Feedback',
                     'RCEN7.TMP22Feedback', 'RCEN7.TMP23Feedback', 'RCEN7.TMP31Feedback', 'RCEN7.TMP32Feedback',
                     'RCEN7.TMP33Feedback', 'CA161.MFC1611_FlowRate', 'CA161.MFC1612_FlowRate',
                     'CA161.MFC1613_FlowRate', 'CA161.MFC1614_FlowRate', 'CA161.MFC1615_FlowRate',
                     'CA161.MFC1616_FlowRate', 'CA161.MFC1617_FlowRate', 'CA162.MFC1621_FlowRate',
                     'CA162.MFC1622_FlowRate', 'CA162.MFC1623_FlowRate', 'CA162.MFC1624_FlowRate',
                     'CA162.MFC1625_FlowRate', 'CA162.MFC1626_FlowRate', 'CA162.MFC1627_FlowRate',
                     'CA163.MFC1631_FlowRate', 'CA163.MFC1632_FlowRate', 'CA163.MFC1633_FlowRate',
                     'CA163.MFC1634_FlowRate', 'CA163.MFC1635_FlowRate', 'CA163.MFC1636_FlowRate',
                     'CA163.MFC1637_FlowRate', 'CA164.MFC1641_FlowRate', 'CA164.MFC1642_FlowRate',
                     'CA164.MFC1643_FlowRate', 'CA164.MFC1644_FlowRate', 'CA164.MFC1645_FlowRate',
                     'CA164.MFC1646_FlowRate', 'CA164.MFC1647_FlowRate', 'RCEN7.MFC11Feedback', 'RCEN7.MFC12Feedback',
                     'RCEN7.MFC21Feedback', 'RCEN7.MFC22Feedback', 'RCEN7.MFC31Feedback', 'RCEN7.MFC32Feedback',
                     'COMMON_7.DG161_Pressure_Torr', 'COMMON_7.PiG161_Pressure_Torr_Exponent', 'CA161.DC1611_Volt',
                     'CA162.DC1621_Volt', 'CA163.DC1631_Volt', 'CA164.DC1641_Volt','RCEN7.CA11VoltageFeedback','RCEN7.CA12VoltageFeedback','RCEN7.CA21VoltageFeedback','RCEN7.CA22VoltageFeedback', 'RCEN7.CA31VoltageFeedback', 'RCEN7.CA32VoltageFeedback',  'EC01_SP7_FRGA.AMU_45',
                     'EC01_SP7_FRGA.Argon_20', 'EC01_SP7_FRGA.CarbonDioxide_44', 'EC01_SP7_FRGA.MPOil_43',
                     'EC01_SP7_FRGA.Nitro_28', 'EC01_SP7_FRGA.Oxygen_32', 'EC01_SP7_FRGA.Water_17',
                     'EC01_SP7_FRGA.Water_18','CA161.MR1611_ActualTorqueValue','CA161.MR1612_ActualTorqueValue','CA162.MR1621_ActualTorqueValue','CA162.MR1622_ActualTorqueValue','CA163.MR1631_ActualTorqueValue','CA163.MR1632_ActualTorqueValue','CA164.MR1641_ActualTorqueValue','CA164.MR1642_ActualTorqueValue','RCEN7.MR11TorqueFeedback','RCEN7.MR12TorqueFeedback','RCEN7.MR21TorqueFeedback','RCEN7.MR22TorqueFeedback','RCEN7.MR31TorqueFeedback','RCEN7.MR32TorqueFeedback','CA161.MR1611_ActualSpeedValue','CA161.MR1612_ActualSpeedValue','CA162.MR1621_ActualSpeedValue','CA162.MR1622_ActualSpeedValue','CA163.MR1631_ActualSpeedValue','CA163.MR1632_ActualSpeedValue','CA164.MR1641_ActualSpeedValue','CA164.MR1642_ActualSpeedValue','RCEN7.MR11SpeedFeedback','RCEN7.MR12SpeedFeedback','RCEN7.MR21SpeedFeedback','RCEN7.MR22SpeedFeedback','RCEN7.MR31SpeedFeedback','RCEN7.MR32SpeedFeedback']

    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    I = iter(columnsTitles)
    next(I)
    CarrierLength = 1.5
    RollingSize = int(CarrierLength * Takt * 20)
    Df['SP7 Measurement Start Time'] = np.roll(Df['DateTime'], RollingSize)
    Df['SP7 Measurement End Time'] = Df['DateTime']
    usefulCroColumns = ['SP7 Measurement Start Time', 'SP7 Measurement End Time']
    Df = Df[usefulCroColumns + [col for col in Df.columns if col not in usefulCroColumns]]
    ColList = ['SP7 Measurement Start Time', 'SP7 Measurement End Time', 'DateTime']
    for j in I:
        stdev = j + " stdev"
        Df[j] = Df[j].astype(float)
        Df[j] = Df[j].rolling(RollingSize).mean()
        Df[stdev] = Df[j].rolling(RollingSize).std()
        ColList.append(j)
        ColList.append(stdev)
    Df = Df.reindex(columns=ColList)
    # print(Df.to_string())
    Df = Df.iloc[RollingSize:]
    return Df


def BigPullPyro(startDate, endDate):
    # N = Minutes
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))

    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [PY.SP1_ENTRY_Y1], [PY.SP1_ENTRY_Y5], [PY.SP1_ENTRY_Y6], [PY.SP1_ENTRY_Y7], [PY.SP1_EXIT_Y1], [PY.SP1_EXIT_Y2], [PY.SP1_EXIT_Y3], [PY.SP1_EXIT_Y5], [PY.SP1_EXIT_Y6], [PY.SP1_EXIT_Y7], [PY.SP2_ENTRY_Y1], [PY.SP2_ENTRY_Y2], [PY.SP2_ENTRY_Y3], [PY.SP2_ENTRY_Y5], [PY.SP2_ENTRY_Y6], [PY.SP2_ENTRY_Y7], [PY.SP3_ENTRY_Y2], [PY.SP3_ENTRY_Y3], [PY.SP3_ENTRY_Y4], [PY.SP3_ENTRY_Y5], [PY.SP3_ENTRY_Y6], [PY.SP3_ENTRY_Y7], [PY.SP4_ENTRY_Y2], [PY.SP4_ENTRY_Y5], [PY.SP4_ENTRY_Y6], [PY.SP4_ENTRY_Y7], [PY.SP5_ENTRY_Y1], [PY.SP5_ENTRY_Y2], [PY.SP5_ENTRY_Y3], [PY.SP5_ENTRY_Y4], [PY.SP5_ENTRY_Y5], [PY.SP5_ENTRY_Y6], [PY.SP5_ENTRY_Y7],[PY.SP6_ENTRY_Y1], [PY.SP6_ENTRY_Y2], [PY.SP6_ENTRY_Y3], [PY.SP6_ENTRY_Y4], [PY.SP6_ENTRY_Y5], [PY.SP6_ENTRY_Y6], [PY.SP6_ENTRY_Y7], [PY.SP7_MID_Y1], [PY.SP7_MID_Y2], [PY.SP7_MID_Y6], [PY.SP7_MID_Y7], [PY.SP7_EXIT_Y4], [PY.SP7_EXIT_Y5], [PY.SP7_EXIT_Y7]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 3000

    AND DateTime >= '{startDate}'
    AND DateTime <='{endDate}'")Hist
    """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    columnsTitles = ['DateTime', 'PY.SP1_ENTRY_Y1', 'PY.SP1_ENTRY_Y5', 'PY.SP1_ENTRY_Y6', 'PY.SP1_ENTRY_Y7',
                     'PY.SP1_EXIT_Y1', 'PY.SP1_EXIT_Y2', 'PY.SP1_EXIT_Y3', 'PY.SP1_EXIT_Y4', 'PY.SP1_EXIT_Y5',
                     'PY.SP1_EXIT_Y6', 'PY.SP1_EXIT_Y7', 'PY.SP2_ENTRY_Y1', 'PY.SP2_ENTRY_Y2', 'PY.SP2_ENTRY_Y3',
                     'PY.SP2_ENTRY_Y4', 'PY.SP2_ENTRY_Y5', 'PY.SP2_ENTRY_Y6', 'PY.SP2_ENTRY_Y7',
                     'PY.SP3_ENTRY_Y2', 'PY.SP3_ENTRY_Y3', 'PY.SP3_ENTRY_Y4', 'PY.SP3_ENTRY_Y5', 'PY.SP3_ENTRY_Y6',
                     'PY.SP3_ENTRY_Y7', 'PY.SP4_ENTRY_Y2',
                     'PY.SP4_ENTRY_Y5', 'PY.SP4_ENTRY_Y6', 'PY.SP4_ENTRY_Y7',  'PY.SP5_ENTRY_Y1', 'PY.SP5_ENTRY_Y2', 'PY.SP5_ENTRY_Y3', 'PY.SP5_ENTRY_Y4', 'PY.SP5_ENTRY_Y5', 'PY.SP5_ENTRY_Y6', 'PY.SP5_ENTRY_Y7', 'PY.SP6_ENTRY_Y1', 'PY.SP6_ENTRY_Y2',
                     'PY.SP6_ENTRY_Y3', 'PY.SP6_ENTRY_Y4', 'PY.SP6_ENTRY_Y5', 'PY.SP6_ENTRY_Y6', 'PY.SP6_ENTRY_Y7',
                     'PY.SP7_MID_Y1', 'PY.SP7_MID_Y2', 'PY.SP7_MID_Y6', 'PY.SP7_MID_Y7', 'PY.SP7_EXIT_Y4',
                     'PY.SP7_EXIT_Y5', 'PY.SP7_EXIT_Y7']
    Df = Df.reindex(columns=columnsTitles)
    Df.fillna(0, inplace=True)
    DF=Df
    # print(Df.to_string())
    I = iter(columnsTitles)
    next(I)
    for j in I:
        SPtext = j + 'stdev'
        SPtext2 = j + 'stdev2'
        SPtext3 = j + 'stdev3'
        # print(SPtext)
        # print(SPtext2)
        Df[SPtext] = Df[j].rolling(8).std()
        Df[SPtext2] = Df[j].rolling(6).std()
        Df[SPtext3] = Df[j].rolling(5).std()

    #export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdasasasss2.csv', header=True)
    return DF


def BigHTOPull(startDate, endDate, Takt):
    # startDate = datetime.now() - timedelta(minutes=N)
    print(str(startDate))
    print(str(endDate))
    if (Takt == 300):
        Takt = 5
    elif Takt == 210:
        Takt = 3.5
    elif Takt == 240:
        Takt = 4

    Query = f"""SET QUOTED_IDENTIFIER OFF

             Select *
             FROM OPENQUERY(INSQL, "SELECT
             [HTO01.Zone1ActualTemperature],[HTO01.Zone2ActualTemperature],[HTO01.Zone3ActualTemperature],[HTO02.Zone1N_Actual_Temp],[HTO02.Zone1S_Actual_Temp],[HTO02.Zone2N_Actual_Temp],[HTO02.Zone2S_Actual_Temp],[HTO02.Zone3N_Actual_Temp],[HTO02.Zone3S_Actual_Temp],[HTO02.Zone4N_Actual_Temp],[HTO02.Zone4S_Actual_Temp]         
             ,StartDateTime
             ,DateTime
             FROM WideHistory
             WHERE wwRetrievalMode = 'Cyclic'
             AND wwResolution = 3000

             AND DateTime >= '{startDate}'
             AND DateTime <='{endDate}'")Hist
             """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    Df.drop_duplicates(subset=['DateTime'])
    #export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\HTOresist2.csv', header=True)
    columnsTitles = ['DateTime', 'HTO01.Zone3ActualTemperature', 'HTO01.Zone2ActualTemperature','HTO01.Zone1ActualTemperature','HTO02.Zone4S_Actual_Temp','HTO02.Zone4N_Actual_Temp','HTO02.Zone3S_Actual_Temp','HTO02.Zone3N_Actual_Temp','HTO02.Zone2S_Actual_Temp','HTO02.Zone2N_Actual_Temp','HTO02.Zone1S_Actual_Temp','HTO02.Zone1N_Actual_Temp']
    Df = Df.reindex(columns=columnsTitles)
    Df['HTO1 Zone 3 Measurement End Time'] = Df['DateTime']
    Df['HTO1 Zone 3 Measurement Start Time'] = np.roll(Df['DateTime'], 300)
    Df['HTO01.Zone2ActualTemperature'] = np.roll(Df['HTO01.Zone2ActualTemperature'], 300)
    Df['HTO1 Zone 2 Measurement End Time'] = np.roll(Df['DateTime'], 300)
    Df['HTO1 Zone 2 Measurement Start Time'] = np.roll(Df['DateTime'], 600)
    Df['HTO01.Zone1ActualTemperature'] = np.roll(Df['HTO01.Zone1ActualTemperature'], 600)
    Df['HTO1 Zone 1 Measurement End Time'] = np.roll(Df['DateTime'], 600)
    Df['HTO1 Zone 1 Measurement Start Time'] = np.roll(Df['DateTime'], 900)
    Df['HTO02.Zone4S_Actual_Temp'] = np.roll(Df['HTO02.Zone4S_Actual_Temp'], 900)
    Df['HTO02.Zone4N_Actual_Temp'] = np.roll(Df['HTO02.Zone4N_Actual_Temp'], 900)
    Df['HTO2 Zone 4 Measurement End Time'] = np.roll(Df['DateTime'], 900)
    Df['HTO2 Zone 4 Measurement Start Time'] = np.roll(Df['DateTime'], 1125)
    Df['HTO02.Zone3S_Actual_Temp'] = np.roll(Df['HTO02.Zone3S_Actual_Temp'], 1125)
    Df['HTO02.Zone3N_Actual_Temp'] = np.roll(Df['HTO02.Zone3N_Actual_Temp'], 1125)
    Df['HTO2 Zone 3 Measurement End Time'] = np.roll(Df['DateTime'], 1125)
    Df['HTO2 Zone 3 Measurement Start Time'] = np.roll(Df['DateTime'], 1350)
    Df['HTO02.Zone2S_Actual_Temp'] = np.roll(Df['HTO02.Zone2S_Actual_Temp'], 1350)
    Df['HTO02.Zone2N_Actual_Temp'] = np.roll(Df['HTO02.Zone2N_Actual_Temp'], 1350)
    Df['HTO2 Zone 2 Measurement End Time'] = np.roll(Df['DateTime'], 1350)
    Df['HTO2 Zone 2 Measurement Start Time'] = np.roll(Df['DateTime'], 1575)
    Df['HTO02.Zone1S_Actual_Temp'] = np.roll(Df['HTO02.Zone1S_Actual_Temp'], 1575)
    Df['HTO02.Zone1N_Actual_Temp'] = np.roll(Df['HTO02.Zone1N_Actual_Temp'], 1575)
    Df['HTO2 Zone 1 Measurement End Time'] = np.roll(Df['DateTime'], 1575)
    Df['HTO2 Zone 1 Measurement Start Time'] = np.roll(Df['DateTime'], 1800)
    averagingRollsizelist=[300,300,300,225,225,225,225,225,225,225,225]
    J=iter(columnsTitles)
    next(J)
    index = 0
    for i in J:
        colname = i+" stdev"
        Df[i]=Df[i].rolling(averagingRollsizelist[index]).mean()
        Df[colname] = Df[i].rolling(averagingRollsizelist[index]).std()
        index+=1
    colTitles = ['DateTime', 'HTO1 Zone 3 Measurement Start Time','HTO1 Zone 3 Measurement End Time','HTO01.Zone3ActualTemperature', 'HTO01.Zone3ActualTemperature stdev',  'HTO1 Zone 2 Measurement Start Time','HTO1 Zone 2 Measurement End Time','HTO01.Zone2ActualTemperature','HTO01.Zone2ActualTemperature stdev',
                 'HTO1 Zone 1 Measurement Start Time', 'HTO1 Zone 1 Measurement End Time',
                 'HTO01.Zone1ActualTemperature','HTO01.Zone1ActualTemperature stdev','HTO2 Zone 4 Measurement Start Time', 'HTO2 Zone 4 Measurement End Time', 'HTO02.Zone4S_Actual_Temp', 'HTO02.Zone4S_Actual_Temp stdev','HTO02.Zone4N_Actual_Temp','HTO02.Zone4N_Actual_Temp stdev',
                     'HTO2 Zone 3 Measurement Start Time', 'HTO2 Zone 3 Measurement End Time', 'HTO02.Zone3S_Actual_Temp','HTO02.Zone3S_Actual_Temp stdev', 'HTO02.Zone3N_Actual_Temp','HTO02.Zone3N_Actual_Temp stdev', 'HTO2 Zone 2 Measurement Start Time', 'HTO2 Zone 2 Measurement End Time','HTO02.Zone2S_Actual_Temp','HTO02.Zone2S_Actual_Temp stdev',
                     'HTO02.Zone2N_Actual_Temp','HTO02.Zone2N_Actual_Temp stdev', 'HTO2 Zone 1 Measurement Start Time', 'HTO2 Zone 1 Measurement End Time','HTO02.Zone1S_Actual_Temp','HTO02.Zone1S_Actual_Temp stdev', 'HTO02.Zone1N_Actual_Temp', 'HTO02.Zone1N_Actual_Temp stdev']
    Df = Df.reindex(columns=colTitles)
    # print(Df.to_string())
    #export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\HTOresist3.csv', header=True)
    Df = Df.iloc[1875:]
    return Df

def HowdthecarriersdoHTO(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['HTOtime'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeH'].values[0]
        ,minutes=Config.loc[Config['Takt'] == Takt, 'HTOExitTimeM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=5)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigHTOPull(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    HTO= df2
    index = 0
    ZoneList = [HTO]
    TimeZoneList = ['HTOtime']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " HTO.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'HTOtime', 'index', 'DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 1, "HTO")
    return df


def HowdthecarriersdoPYRO(DF, bATCH, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    df['SP1timeentry'] = df['ActualStart'] + timedelta(
        minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeentryM'].values[0])
    df['SP1timeexit'] = df['ActualStart'] + timedelta(
        minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeexitM'].values[0])
    df['SP2time'] = df['ActualStart'] + timedelta(minutes=Config.loc[Config['Takt'] == Takt, 'SP2timeentryM'].values[0])
    df['SP3time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP3timeentryH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP3timeentryM'].values[0],
                                                  seconds=Config.loc[Config['Takt'] == Takt, 'SP3timeentryS'].values[0])
    df['SP4time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP4timeentryH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP4timeentryM'].values[0])
    df['SP5time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP5timeentryH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP5timeentryM'].values[0],
                                                  seconds=Config.loc[Config['Takt'] == Takt, 'SP5timeentryS'].values[0])
    df['SP6time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP6timeentryH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP6timeentryM'].values[0])
    df['SP7timemid'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP7timemidH'].values[0],
                                                     minutes=Config.loc[Config['Takt'] == Takt, 'SP7timemidM'].values[0])
    SP1LISTEntry = ['DateTime', 'PY.SP1_ENTRY_Y1', 'PY.SP1_ENTRY_Y5', 'PY.SP1_ENTRY_Y6', 'PY.SP1_ENTRY_Y7']
    SP1LISTExit = ['DateTime', 'PY.SP1_EXIT_Y1', 'PY.SP1_EXIT_Y2', 'PY.SP1_EXIT_Y3', 'PY.SP1_EXIT_Y5',
                   'PY.SP1_EXIT_Y6', 'PY.SP1_EXIT_Y7']
    SP2LIST = ['DateTime', 'PY.SP2_ENTRY_Y1', 'PY.SP2_ENTRY_Y2', 'PY.SP2_ENTRY_Y3', 'PY.SP2_ENTRY_Y5', 'PY.SP2_ENTRY_Y6', 'PY.SP2_ENTRY_Y7']
    SP3LIST = ['DateTime', 'PY.SP3_ENTRY_Y2', 'PY.SP3_ENTRY_Y3', 'PY.SP3_ENTRY_Y4',
               'PY.SP3_ENTRY_Y5', 'PY.SP3_ENTRY_Y6', 'PY.SP3_ENTRY_Y7']
    SP4LIST = ['DateTime', 'PY.SP4_ENTRY_Y2',
               'PY.SP4_ENTRY_Y5', 'PY.SP4_ENTRY_Y6', 'PY.SP4_ENTRY_Y7']
    SP5LIST =['DateTime', 'PY.SP5_ENTRY_Y1', 'PY.SP5_ENTRY_Y2', 'PY.SP5_ENTRY_Y3', 'PY.SP5_ENTRY_Y4', 'PY.SP5_ENTRY_Y5', 'PY.SP5_ENTRY_Y6', 'PY.SP5_ENTRY_Y7']
    SP6LIST = ['DateTime', 'PY.SP6_ENTRY_Y1', 'PY.SP6_ENTRY_Y2', 'PY.SP6_ENTRY_Y3', 'PY.SP6_ENTRY_Y4',
               'PY.SP6_ENTRY_Y5', 'PY.SP6_ENTRY_Y6', 'PY.SP6_ENTRY_Y7']

    SP7LISTMID = ['DateTime', 'PY.SP7_MID_Y1', 'PY.SP7_MID_Y2', 'PY.SP7_MID_Y6', 'PY.SP7_MID_Y7']
    SP1LISTEntry2 = [Config.loc[Config['Takt'] == Takt, 'SP1EntryLow'].values[0], Config.loc[Config['Takt'] == Takt, 'SP1EntryHigh'].values[0]]
    SP1LISTExit2 = [Config.loc[Config['Takt'] == Takt, 'SP1ExitLow'].values[0], Config.loc[Config['Takt'] == Takt, 'SP1ExitHigh'].values[0]]
    SP2LIST2 = [Config.loc[Config['Takt'] == Takt, 'SP2Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP2High'].values[0]]
    SP3LIST2 = [Config.loc[Config['Takt'] == Takt, 'SP3Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP3High'].values[0]]
    SP4LIST2 = [Config.loc[Config['Takt'] == Takt, 'SP4Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP4High'].values[0]]
    SP5LIST2 = [Config.loc[Config['Takt'] == Takt, 'SP5Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP5High'].values[0]]
    SP6LIST2 = [Config.loc[Config['Takt'] == Takt, 'SP6Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP6High'].values[0]]
    SP7LISTMID2 = [Config.loc[Config['Takt'] == Takt, 'SP7Low'].values[0], Config.loc[Config['Takt'] == Takt, 'SP7High'].values[0]]

    Tlist = [SP1LISTEntry, SP1LISTExit, SP2LIST, SP3LIST, SP4LIST, SP5LIST,SP6LIST, SP7LISTMID]
    Tlist2 = [SP1LISTEntry2, SP1LISTExit2, SP2LIST2, SP3LIST2, SP4LIST2, SP5LIST2,SP6LIST2, SP7LISTMID2]
    stdevlist = [Config.loc[Config['Takt'] == Takt, 'SP1ENTRYPyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP1EXITPyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP2PyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP3PyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP4PyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP5PyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP6PyroSTDEV'].values[0],
                 Config.loc[Config['Takt'] == Takt, 'SP7PyroSTDEV'].values[0]]
    index = 0
    ZoneList = ['SP1entry', 'SP1exit', 'SP2', 'SP3', 'SP4', 'SP5', 'SP6', 'SP7mid']
    ZoneList2 = [1,1,2,3,4,5,6,7]
    LOClist = [1,3,1,1,1,1,1,2]
    TimeZoneList = ['SP1timeentry', 'SP1timeexit', 'SP2time', 'SP3time', 'SP4time', 'SP5time', 'SP6time', 'SP7timemid']
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullPyro(StartDate, EndDate)
    # df2=pd.read_csv(r'C:\Users\arobinson\Desktop\ODdasasasss.csv')
    df2 = df2.reset_index()

    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    fileList = []
    for i in Tlist:
        met=ZoneList[index]+" Pyro "+'Measurement End Time'
        mst = ZoneList[index] + " Pyro " + 'Measurement Start Time'
        ColumnList = ['DateTime', met]
        index2 = []
        DF = df2.filter(Tlist[index])
        I = iter(i)
        next(I)
        for j in I:
            SPtext = j + 'stdev'
            # print(SPtext)
            SPtext2 = j + 'good'
            # print(SPtext2)
            DF[SPtext] = DF[j].rolling(int(Config.loc[Config['Takt'] == Takt, 'PyroRollSize'].values[0])).std()
            DF.loc[(DF[SPtext] <= stdevlist[index]), SPtext2] = 1
            DF.loc[(DF[SPtext] >= stdevlist[index]), SPtext2] = 0
            index2.append(SPtext2)

        DF['Good record'] = DF.loc[:, index2].prod(axis=1)
        DF['Good record'] = DF['Good record'].astype(int)
        DF['IsChanged2'] = (DF['Good record'].diff() != 0).astype(int)
        DF['lite Identifier2'] = DF['IsChanged2'].cumsum()
        DF = DF[(DF['Good record'] == 1)]


        x = iter(i)
        next(x)
        for y in x:
            #print("trying")
            DF = DF[(DF[y] < Tlist2[index][1]) & (DF[y] > Tlist2[index][0])]



        z = iter(i)
        next(z)
        for K in z:
            #print("trying")
            DF = DF[(DF[K] < DF[K].quantile(.95)) & (DF[K] > DF[K].quantile(.05))]


        l = iter(i)
        next(l)
        for j in l:
            tag = j + ' mean'
            ColumnList.append(tag)
            tag2 = j + ' stdev'
            ColumnList.append(tag2)
            DF[tag] = DF.groupby('lite Identifier2')[j].transform('mean')
            DF[tag2] = DF.groupby('lite Identifier2')[j].transform('std')

        # fil = ZoneList[index] + " Carrier Conditions.csv"
        # filename = os.path.join(r'C:\Users\arobinson\Desktop', fil)
        # export_csv = DF.to_csv(filename, header=True)
        DF[met] = DF.groupby('lite Identifier2')['DateTime'].transform('max')
        DF = DF.drop_duplicates(subset='lite Identifier2', keep='first')
        # fil =  ZoneList[index] + " 2 Carrier Conditions.csv"
        # filename = os.path.join(r'C:\Users\arobinson\Desktop', fil)
        # export_csv2 = DF.to_csv(filename, header=True)
        DF = DF.sort_values('DateTime')
        DF = DF.filter(ColumnList)
        df.sort_values(TimeZoneList[index], inplace=True)
        df3 = df.dropna(subset=[TimeZoneList[index]])
        df4 = pd.merge_asof(DF, df3, left_on='DateTime', right_on=TimeZoneList[index], tolerance=pd.Timedelta('4m'))
        df4['IsChanged3'] = (df4['BOSequence'].diff() != 0).astype(int)
        df4['NotChanged'] = (df4['BOSequence'].diff() == 0).astype(int)
        df4['lite Identifier5000'] = df4['IsChanged3'].cumsum()
        df4['ColumnCount'] = df4.groupby('lite Identifier5000')['NotChanged'].cumsum()
        df4['CarrierColumn'] = df4['ColumnCount'] + 1
        df4 = df4[(df4['ColumnCount'] <= 3)]
        df4 = df4.dropna(subset=['TestPlan'])
        df4 = df4.drop(
            ['IsChanged3', 'NotChanged', 'lite Identifier5000', 'ActualFinish', 'Approximate Intermediate',
             'SP1timeentry', 'SP1timeexit', 'SP2time', 'SP3time', 'SP4time', 'SP5time', 'SP6time', 'SP7timemid', 'ColumnCount'],
            axis=1)
        usefulCroColumns=['Batch', 'BOSequence', 'BONumber','Source','CarrierReleaseID', 'TestPlan', 'CarrierLayOut',  'Status', 'GateTimer', 'ActualStart', 'CarrierColumn']
        df4 = df4[usefulCroColumns + [col for col in df4.columns if col not in usefulCroColumns]]
        df4.rename(columns={'DateTime':mst}, inplace=True)
        fil = ZoneList[index] + " " + str(bATCH) + " Pyro Conditions.csv"

        filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
        export_csv2 = df4.to_csv(filename, header=True, index=False)
        UpdatePYROs(df4, ZoneList2[index], LOClist[index])
        if index >0 :
            df4=df4.drop(['Batch', 'BOSequence', 'Source','CarrierReleaseID', 'TestPlan', 'CarrierLayOut',  'Status', 'GateTimer', 'ActualStart'], axis=1)
        fileList.append(df4)
        index += 1
        # export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\PleaseWork.csv', header=True)
    # df=df.drop_duplicates('TestPlan')
    # export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\PleaseWork.csv', header=True)
    Base=fileList[0]
    j=iter(fileList)
    next(j)
    MergeList = ['BONumber', 'CarrierColumn']
    for i in j:
        Base = Base.merge(i, how='outer', on=MergeList, suffixes=('', '_y'))
    drop_y(Base)
    Base['WonderTrace Rev']= "1.0"
    fil = str(bATCH) + " Pyro Conditions.csv"
    filename = os.path.join(r'C:\Users\arobinson\Desktop', fil)
    #export_csv2 = Base.to_csv(filename, header=True, index=False)

    return fileList

def HowdthecarriersdoSP1(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP1time'] = df['ActualStart'] + timedelta(minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeexitM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP1(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP1 = df2
    index = 0
    ZoneList = [SP1]
    TimeZoneList = ['SP1time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP1.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP1time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 1, "SP")
    return df


def HowdthecarriersdoSP2(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP2time'] = df['ActualStart'] + timedelta(minutes=Config.loc[Config['Takt'] == Takt, 'SP2timeexitM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP2(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP2 = df2
    index = 0
    ZoneList = [SP2]
    TimeZoneList = ['SP2time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP2.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP2time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 2, "SP")
    return df


def HowdthecarriersdoSP3(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP3time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP3timeexitH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP3timeexitM'].values[0],
                                                  seconds=Config.loc[Config['Takt'] == Takt, 'SP3timeexitS'].values[0])

    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP3(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP3 = df2
    index = 0
    ZoneList = [SP3]
    TimeZoneList = ['SP3time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP3.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP3time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 3, "SP")
    return df


def HowdthecarriersdoSP4(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP4time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP4timeexitH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP4timeexitM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP4(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP4 = df2
    index = 0
    ZoneList = [SP4]
    TimeZoneList = ['SP4time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP4.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP4time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 4, "SP")
    return df


def HowdthecarriersdoSP5(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP5time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP5timeexitH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP5timeexitM'].values[0],
                                                  seconds=Config.loc[Config['Takt'] == Takt, 'SP5timeexitS'].values[0])

    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP5(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP5 = df2
    index = 0
    ZoneList = [SP5]
    TimeZoneList = ['SP5time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP5.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP5time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 5, "SP")
    return df


def HowdthecarriersdoSP6(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP6time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP6timeexitH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP6timeexitM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP6(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP6 = df2
    index = 0
    ZoneList = [SP6]
    TimeZoneList = ['SP6time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP6.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP6time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 6, "SP")
    return df


def HowdthecarriersdoSP7(DF, Date, Config):
    df = DF
    df = df.dropna(subset=['GateTimer'])
    items_counts = df['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    print(str(Takt))
    df['ActualStart'] = pd.to_datetime(df['ActualStart'])
    df['ActualFinish'] = pd.to_datetime(df['ActualFinish'])
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    df['SP7time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt'] == Takt, 'SP7timeexitH'].values[0],
                                                  minutes=Config.loc[Config['Takt'] == Takt, 'SP7timeexitM'].values[0])
    # df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = BigPullSP7(StartDate, EndDate, Takt)
    df2 = df2.reset_index()
    # print(df2.columns.values)
    # df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP7 = df2
    index = 0
    ZoneList = [SP7]
    TimeZoneList = ['SP7time']
    for i in ZoneList:
        i.dropna(subset=['DateTime'], inplace=True)
        i.sort_values('DateTime', inplace=True)
        df.sort_values(TimeZoneList[index])
        df.dropna(subset=[TimeZoneList[index]], inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df = pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index += 1
    df = df.drop_duplicates(subset=['ActualStart'])
    FIL = Date + " SP7.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', FIL)
    df = df.drop(['ActualFinish', 'Approximate Intermediate', 'SP7time', 'index','DateTime'], axis=1)
    export_csv = df.to_csv(filename, header=True, index=False)
    UpdateFeedback(df, 7, "SP")
    return df


def Howdthecarrierdolith(DF,Config):
    df=DF
    Takt = df['GateTimer'].mean()
    df['Approximate Intermediate'] = pd.to_datetime(df['ActualStart'])
    #df['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(minutes=34)
    df['SP5time'] = df['ActualStart'] + timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP5timeentryH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP5timeentryM'].values[0], seconds=Config.loc[Config['Takt']==Takt, 'SP5timeentryS'].values[0])
    SP1LIST = ['DateTime', 'CA041.AC041_MF_Power', 'CA042.AC042_MF_Power', 'CA043.AC043_MF_Power', 'CA044.AC044_MF_Power', 'CA045.AC045_MF_Power', 'CA046.AC046_MF_Power', 'CA047.AC047_MF_Power', 'CA048.AC048_MF_Power', 'RCEX1.CA1PowerSetpoint', 'RCEX1.CA2PowerSetpoint', 'RCEX1.CA3PowerSetpoint','CA041.TMP0411_Rotation', 'CA041.TMP0412_Rotation', 'CA041.TMP0413_Rotation', 'CA042.TMP0421_Rotation', 'CA042.TMP0422_Rotation', 'CA042.TMP0423_Rotation', 'CA043.TMP0431_Rotation', 'CA043.TMP0432_Rotation', 'CA043.TMP0433_Rotation', 'CA044.TMP0441_Rotation', 'CA044.TMP0442_Rotation', 'CA044.TMP0443_Rotation', 'CA045.TMP0451_Rotation', 'CA045.TMP0452_Rotation', 'CA045.TMP0453_Rotation', 'CA046.TMP0461_Rotation', 'CA046.TMP0462_Rotation', 'CA046.TMP0463_Rotation', 'CA047.TMP0471_Rotation', 'CA047.TMP0472_Rotation', 'CA047.TMP0473_Rotation', 'CA048.TMP0481_Rotation', 'CA048.TMP0482_Rotation', 'CA048.TMP0483_Rotation', 'RCEX1.TMP11Setpoint', 'RCEX1.TMP12Setpoint', 'RCEX1.TMP13Setpoint', 'RCEX1.TMP21Setpoint', 'RCEX1.TMP22Setpoint', 'RCEX1.TMP23Setpoint', 'RCEX1.TMP31Setpoint', 'RCEX1.TMP32Setpoint', 'RCEX1.TMP33Setpoint', 'CA041.MFC0411_FlowRate','CA041.MFC0412_FlowRate','CA041.MFC0413_FlowRate','CA041.MFC0414_FlowRate','CA041.MFC0415_FlowRate','CA041.MFC0416_FlowRate','CA041.MFC0417_FlowRate','CA042.MFC0421_FlowRate','CA042.MFC0422_FlowRate','CA042.MFC0423_FlowRate','CA042.MFC0424_FlowRate','CA042.MFC0425_FlowRate','CA042.MFC0426_FlowRate','CA042.MFC0427_FlowRate','CA043.MFC0431_FlowRate','CA043.MFC0432_FlowRate','CA043.MFC0433_FlowRate','CA043.MFC0434_FlowRate','CA043.MFC0435_FlowRate','CA043.MFC0436_FlowRate','CA043.MFC0437_FlowRate','CA044.MFC0441_FlowRate','CA044.MFC0442_FlowRate','CA044.MFC0443_FlowRate','CA044.MFC0444_FlowRate','CA044.MFC0445_FlowRate','CA044.MFC0446_FlowRate','CA044.MFC0447_FlowRate','CA045.MFC0451_FlowRate','CA045.MFC0452_FlowRate','CA045.MFC0453_FlowRate','CA045.MFC0454_FlowRate','CA045.MFC0455_FlowRate','CA045.MFC0456_FlowRate','CA045.MFC0457_FlowRate','CA046.MFC0461_FlowRate','CA046.MFC0462_FlowRate','CA046.MFC0463_FlowRate','CA046.MFC0464_FlowRate','CA046.MFC0465_FlowRate','CA046.MFC0466_FlowRate','CA046.MFC0467_FlowRate','CA047.MFC0471_FlowRate','CA047.MFC0472_FlowRate','CA047.MFC0473_FlowRate','CA047.MFC0474_FlowRate','CA047.MFC0475_FlowRate','CA047.MFC0476_FlowRate','CA047.MFC0477_FlowRate','CA048.MFC0481_FlowRate','CA048.MFC0482_FlowRate','CA048.MFC0483_FlowRate','CA048.MFC0484_FlowRate','CA048.MFC0485_FlowRate','CA048.MFC0486_FlowRate','CA048.MFC0487_FlowRate',  'RCEX1.MFC11Setpoint', 'RCEX1.MFC12Setpoint', 'RCEX1.MFC21Setpoint', 'RCEX1.MFC22Setpoint', 'RCEX1.MFC31Setpoint', 'RCEX1.MFC32Setpoint','COMMON_1.DG041_Pressure','COMMON_1.PiG041_Pressure', 'CA041.AC041_MF_Volt', 'CA042.AC042_MF_Volt', 'CA043.AC043_MF_Volt', 'CA044.AC044_MF_Volt', 'CA045.AC045_MF_Volt', 'CA046.AC046_MF_Volt', 'CA046.AC046_MF_Volt', 'CA047.AC047_MF_Volt', 'CA048.AC048_MF_Volt']
    SP2LIST = ['DateTime', 'CA061.AC061_MF_Power', 'CA062.AC062_MF_Power', 'CA063.AC063_MF_Power', 'CA064.AC064_MF_Power','CA061.TMP0611_Rotation', 'CA061.TMP0612_Rotation', 'CA061.TMP0613_Rotation', 'CA062.TMP0621_Rotation', 'CA062.TMP0622_Rotation', 'CA062.TMP0623_Rotation', 'CA063.TMP0631_Rotation', 'CA063.TMP0632_Rotation', 'CA063.TMP0633_Rotation', 'CA064.TMP0641_Rotation', 'CA064.TMP0642_Rotation', 'CA064.TMP0643_Rotation','CA061.MFC0611_FlowRate','CA061.MFC0612_FlowRate','CA061.MFC0613_FlowRate','CA061.MFC0614_FlowRate','CA061.MFC0615_FlowRate','CA061.MFC0616_FlowRate','CA061.MFC0617_FlowRate','CA062.MFC0621_FlowRate','CA062.MFC0622_FlowRate','CA062.MFC0623_FlowRate','CA062.MFC0624_FlowRate','CA062.MFC0625_FlowRate','CA062.MFC0626_FlowRate','CA062.MFC0627_FlowRate','CA063.MFC0631_FlowRate','CA063.MFC0632_FlowRate','CA063.MFC0633_FlowRate','CA063.MFC0634_FlowRate','CA063.MFC0635_FlowRate','CA063.MFC0636_FlowRate','CA063.MFC0637_FlowRate','CA064.MFC0641_FlowRate','CA064.MFC0642_FlowRate','CA064.MFC0643_FlowRate','CA064.MFC0644_FlowRate','CA064.MFC0645_FlowRate','CA064.MFC0646_FlowRate','CA064.MFC0647_FlowRate','COMMON_2.DG061_Pressure','COMMON_2.PiG061_Pressure', 'CA061.AC061_MF_Volt', 'CA062.AC062_MF_Volt' 'CA063.AC063_MF_Volt', 'CA064.AC064_MF_Volt']
    SP3LIST = ['DateTime', 'CA081.MF_Power', 'CA082.MF_Power', 'CA083.MF_Power', 'CA084.MF_Power','CA081.TMP1', 'CA081.TMP2', 'CA081.TMP3', 'CA082.TMP1', 'CA082.TMP2', 'CA082.TMP3', 'CA083.TMP1', 'CA083.TMP2', 'CA083.TMP3', 'CA084.TMP1', 'CA084.TMP2', 'CA084.TMP3','CA081.MFC1', 'CA081.MFC2', 'CA081.MFC3', 'CA081.MFC4', 'CA081.MFC5', 'CA081.MFC6', 'CA081.MFC7', 'CA082.MFC1', 'CA082.MFC2', 'CA082.MFC3', 'CA082.MFC4', 'CA082.MFC5', 'CA082.MFC6', 'CA082.MFC7', 'CA083.MFC1', 'CA083.MFC2', 'CA083.MFC3', 'CA083.MFC4', 'CA083.MFC5', 'CA083.MFC6', 'CA083.MFC7', 'CA084.MFC1', 'CA084.MFC2', 'CA084.MFC3', 'CA084.MFC4', 'CA084.MFC5', 'CA084.MFC6', 'CA084.MFC7','COMMON_3.DG081_Pressure','COMMON_3.PiG081_Pressure', 'CA081.MF_Volt', 'CA082.MF_Volt', 'CA083.MF_Volt', 'CA084.MF_Volt']
    SP4LIST = ['DateTime', 'CA101.AC101_MF_Power', 'CA102.AC102_MF_Power', 'CA103.AC103_MF_Power', 'CA104.AC104_MF_Power', 'CA105.AC105_MF_Power', 'CA106.AC106_MF_Power', 'CA107.AC107_MF_Power', 'CA108.AC108_MF_Power', 'RCEN4.CA1PowerSetpoint', 'RCEN4.CA2PowerSetpoint', 'RCEN4.CA3PowerSetpoint','CA101.TMP1011_Rotation', 'CA101.TMP1012_Rotation', 'CA101.TMP1013_Rotation', 'CA102.TMP1021_Rotation', 'CA102.TMP1022_Rotation', 'CA102.TMP1023_Rotation', 'CA103.TMP1031_Rotation', 'CA103.TMP1032_Rotation', 'CA103.TMP1033_Rotation', 'CA104.TMP1041_Rotation', 'CA104.TMP1042_Rotation', 'CA104.TMP1043_Rotation', 'CA105.TMP1051_Rotation', 'CA105.TMP1052_Rotation', 'CA105.TMP1053_Rotation', 'CA106.TMP1061_Rotation', 'CA106.TMP1062_Rotation', 'CA106.TMP1063_Rotation', 'CA107.TMP1071_Rotation', 'CA107.TMP1072_Rotation', 'CA107.TMP1073_Rotation', 'CA108.TMP1081_Rotation', 'CA108.TMP1082_Rotation', 'CA108.TMP1083_Rotation', 'RCEN4.TMP11Setpoint', 'RCEN4.TMP12Setpoint', 'RCEN4.TMP13Setpoint', 'RCEN4.TMP21Setpoint', 'RCEN4.TMP22Setpoint', 'RCEN4.TMP23Setpoint', 'RCEN4.TMP31Setpoint', 'RCEN4.TMP32Setpoint', 'RCEN4.TMP33Setpoint','CA101.MFC1011_FlowRate','CA101.MFC1012_FlowRate','CA101.MFC1013_FlowRate','CA101.MFC1014_FlowRate','CA101.MFC1015_FlowRate','CA101.MFC1016_FlowRate','CA101.MFC1017_FlowRate','CA102.MFC1021_FlowRate','CA102.MFC1022_FlowRate','CA102.MFC1023_FlowRate','CA102.MFC1024_FlowRate','CA102.MFC1025_FlowRate','CA102.MFC1026_FlowRate','CA102.MFC1027_FlowRate','CA103.MFC1031_FlowRate','CA103.MFC1032_FlowRate','CA103.MFC1033_FlowRate','CA103.MFC1034_FlowRate','CA103.MFC1035_FlowRate','CA103.MFC1036_FlowRate','CA103.MFC1037_FlowRate','CA104.MFC1041_FlowRate','CA104.MFC1042_FlowRate','CA104.MFC1043_FlowRate','CA104.MFC1044_FlowRate','CA104.MFC1045_FlowRate','CA104.MFC1046_FlowRate','CA104.MFC1047_FlowRate','CA105.MFC1051_FlowRate','CA105.MFC1052_FlowRate','CA105.MFC1053_FlowRate','CA105.MFC1054_FlowRate','CA105.MFC1055_FlowRate','CA105.MFC1056_FlowRate','CA105.MFC1057_FlowRate','CA106.MFC1061_FlowRate','CA106.MFC1062_FlowRate','CA106.MFC1063_FlowRate','CA106.MFC1064_FlowRate','CA106.MFC1065_FlowRate','CA106.MFC1066_FlowRate','CA106.MFC1067_FlowRate','CA107.MFC1071_FlowRate','CA107.MFC1072_FlowRate','CA107.MFC1073_FlowRate','CA107.MFC1074_FlowRate','CA107.MFC1075_FlowRate','CA107.MFC1076_FlowRate','CA107.MFC1077_FlowRate','CA108.MFC1081_FlowRate','CA108.MFC1082_FlowRate','CA108.MFC1083_FlowRate','CA108.MFC1084_FlowRate','CA108.MFC1085_FlowRate','CA108.MFC1086_FlowRate','CA108.MFC1087_FlowRate','COMMON_4.DG101_Pressure','COMMON_4.PiG101_Pressure','CA101.AC101_MF_Volt', 'CA102.AC102_MF_Volt',  'CA103.AC103_MF_Volt','CA104.AC104_MF_Volt', 'CA105.AC105_MF_Volt', 'CA106.AC106_MF_Volt', 'CA107.AC107_MF_Volt', 'CA108.AC108_MF_Volt']
    SP5LIST = ['DateTime', 'COMMON_5.TMP1211_Rotation', 'COMMON_5.TMP1212_Rotation', 'COMMON_5.TMP1213_Rotation', 'COMMON_5.TMP1214_Rotation', 'COMMON_5.TMP1221_Rotation', 'COMMON_5.TMP1222_Rotation', 'COMMON_5.TMP1223_Rotation', 'COMMON_5.TMP1224_Rotation','PVD01_MFC1211.MFC01','PVD01_MFC1211.MFC02','PVD01_MFC1211.MFC03','PVD01_MFC1211.MFC04','PVD01_MFC1211.MFC05','PVD01_MFC1211.MFC06','PVD01_MFC1211.MFC07','PVD01_MFC1211.MFC08','PVD01_MFC1211.MFC09','PVD01_MFC1211.MFC10','PVD01_MFC1212.MFC01','PVD01_MFC1212.MFC02','PVD01_MFC1212.MFC03','PVD01_MFC1212.MFC04','PVD01_MFC1212.MFC05','PVD01_MFC1212.MFC06','PVD01_MFC1212.MFC07','PVD01_MFC1212.MFC08','PVD01_MFC1212.MFC09','PVD01_MFC1212.MFC10','PVD01_MFC1213.MFC01','PVD01_MFC1213.MFC02','PVD01_MFC1213.MFC03','PVD01_MFC1213.MFC04','PVD01_MFC1213.MFC05','PVD01_MFC1213.MFC06','PVD01_MFC1213.MFC07','PVD01_MFC1213.MFC08','PVD01_MFC1213.MFC09','PVD01_MFC1213.MFC10','PVD01_MFC1214.MFC01','PVD01_MFC1214.MFC02','PVD01_MFC1214.MFC03','PVD01_MFC1214.MFC04','PVD01_MFC1214.MFC05','PVD01_MFC1214.MFC06','PVD01_MFC1214.MFC07','PVD01_MFC1214.MFC08','PVD01_MFC1214.MFC09','PVD01_MFC1214.MFC10', 'COMMON_5.MFC1213_FlowRate', 'COMMON_5.MFC1214_FlowRate','COMMON_5.DG121_Pressure_Torr','COMMON_5.PiG121_Pressure', 'CA121.DC121_Volt', 'CA122.DC122_Volt', 'CA123.DC123_Volt', 'CA124.DC124_Volt', 'CA125.DC125_Volt', 'CA126.DC126_Volt', 'CA127.DC127_Volt', 'CA128.DC128_Volt']
    SP6LIST = ['DateTime', 'CA143.AC143MF_Power','CA144.AC144MF_Power','CA141.TMP1411_Rotation', 'CA141.TMP1412_Rotation', 'CA141.TMP1413_Rotation', 'CA142.TMP1421_Rotation', 'CA142.TMP1422_Rotation', 'CA142.TMP1423_Rotation', 'CA143.TMP1431_Rotation', 'CA143.TMP1432_Rotation', 'CA143.TMP1433_Rotation', 'CA144.TMP1441_Rotation', 'CA144.TMP1442_Rotation', 'CA144.TMP1443_Rotation','CA141.MFC1411_FlowRate','CA141.MFC1412_FlowRate','CA141.MFC1413_FlowRate','CA141.MFC1414_FlowRate','CA141.MFC1415_FlowRate','CA141.MFC1416_FlowRate','CA141.MFC1417_FlowRate','CA142.MFC1421_FlowRate','CA142.MFC1422_FlowRate','CA142.MFC1423_FlowRate','CA142.MFC1424_FlowRate','CA142.MFC1425_FlowRate','CA142.MFC1426_FlowRate','CA142.MFC1427_FlowRate','CA143.MFC1431_FlowRate','CA143.MFC1432_FlowRate','CA143.MFC1433_FlowRate','CA143.MFC1434_FlowRate','CA143.MFC1435_FlowRate','CA143.MFC1436_FlowRate','CA143.MFC1437_FlowRate','CA144.MFC1441_FlowRate','CA144.MFC1442_FlowRate','CA144.MFC1443_FlowRate','CA144.MFC1444_FlowRate','CA144.MFC1445_FlowRate','CA144.MFC1446_FlowRate','CA144.MFC1447_FlowRate','COMMON_6.DG141_Pressure','COMMON_6.PiG141_Pressure_Torr_Exponent', 'CA143.AC143MF_Volt', 'CA144.AC144MF_Volt']
    SP7LIST = ['DateTime', 'CA161.DC1611_Power', 'CA161.DC1612_Power', 'CA162.DC1621_Power', 'CA162.DC1622_Power', 'CA163.DC1631_Power', 'CA163.DC1632_Power', 'CA164.DC1641_Power', 'CA164.DC1642_Power', 'RCEN7.CA11PowerSetpoint', 'RCEN7.CA12PowerSetpoint', 'RCEN7.CA21PowerSetpoint', 'RCEN7.CA22PowerSetpoint', 'RCEN7.CA31PowerSetpoint', 'RCEN7.CA32PowerSetpoint','CA161.TMP1611_Rotation', 'CA161.TMP1612_Rotation', 'CA161.TMP1613_Rotation', 'CA162.TMP1621_Rotation', 'CA162.TMP1622_Rotation', 'CA162.TMP1623_Rotation', 'CA163.TMP1631_Rotation', 'CA163.TMP1632_Rotation', 'CA163.TMP1633_Rotation', 'CA164.TMP1641_Rotation', 'CA164.TMP1642_Rotation', 'CA164.TMP1643_Rotation', 'RCEN7.TMP11Setpoint', 'RCEN7.TMP12Setpoint', 'RCEN7.TMP13Setpoint', 'RCEN7.TMP21Setpoint', 'RCEN7.TMP22Setpoint', 'RCEN7.TMP23Setpoint', 'RCEN7.TMP31Setpoint', 'RCEN7.TMP32Setpoint', 'RCEN7.TMP33Setpoint', 'CA161.MFC1611_FlowRate','CA161.MFC1612_FlowRate','CA161.MFC1613_FlowRate','CA161.MFC1614_FlowRate','CA161.MFC1615_FlowRate','CA161.MFC1616_FlowRate','CA161.MFC1617_FlowRate','CA162.MFC1621_FlowRate','CA162.MFC1622_FlowRate','CA162.MFC1623_FlowRate','CA162.MFC1624_FlowRate','CA162.MFC1625_FlowRate','CA162.MFC1626_FlowRate','CA162.MFC1627_FlowRate','CA163.MFC1631_FlowRate','CA163.MFC1632_FlowRate','CA163.MFC1633_FlowRate','CA163.MFC1634_FlowRate','CA163.MFC1635_FlowRate','CA163.MFC1636_FlowRate','CA163.MFC1637_FlowRate','CA164.MFC1641_FlowRate','CA164.MFC1642_FlowRate','CA164.MFC1643_FlowRate','CA164.MFC1644_FlowRate','CA164.MFC1645_FlowRate','CA164.MFC1646_FlowRate','CA164.MFC1647_FlowRate', 'RCEN7.MFC11Setpoint', 'RCEN7.MFC12Setpoint', 'RCEN7.MFC21Setpoint', 'RCEN7.MFC22Setpoint', 'RCEN7.MFC31Setpoint', 'RCEN7.MFC32Setpoint','COMMON_7.DG161_Pressure_Torr','COMMON_7.PiG161_Pressure_Torr_Exponent','CA161.DC1611_Volt', 'CA162.DC1621_Volt', 'CA163.DC1631_Volt', 'CA164.DC1641_Volt']
    StartDate = (df['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (df['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    df2 = LITHIATIONHistory(StartDate,EndDate,Config, Takt)
    df2=df2.reset_index()
    #print(df2.columns.values)
    #df2.head()
    df2['DateTime'] = pd.to_datetime(df2['DateTime'])
    SP1 = df2.filter(SP1LIST)
    SP2 = df2.filter(SP2LIST)
    SP3 = df2.filter(SP3LIST)
    SP4 = df2.filter(SP4LIST)
    SP5 = df2.filter(SP5LIST)
    SP6 = df2.filter(SP6LIST)
    SP7 = df2.filter(SP7LIST)
    index = 0
    ZoneList= [SP5]
    TimeZoneList=['SP5time']
    for i in ZoneList:
        i.sort_values('DateTime',inplace=True)
        i.dropna(subset='DateTime', inplace=True)
        df.dropna(subset=[TimeZoneList[index]],inplace=True)
        df.sort_values(TimeZoneList[index], inplace=True)
        df=pd.merge_asof(df, i, left_on=TimeZoneList[index], right_on='DateTime', direction='nearest')
        index+=1
    #export_csv = df.to_csv(r'C:\Users\arobinson\Desktop\PleaseWork20.csv', header=True)
    return df

def LITHIATIONHistory(startDate, EndDate, Config, Takt):
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [CA121.DC121_Power],[ CA124.DC124_Power],[ CA125.DC125_Power],[ CA126.DC126_Power],[ CA127.DC127_Power],[ CA128.DC128_Power],[ CA129.CA_kW_FB],[ CA130.CA_kW_FB],[ CA131.CA_kW_FB],[CA1312.CA_kW_FB],[TRACKING_5.SP5_CarrierTracking_2]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 3000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'])
    Df.fillna(0, inplace=True)
    Df = Df.reset_index()
    Df['CA121.DC121_Power'] = Df['CA121.DC121_Power'].astype(float)
    Df['CA124.DC124_Power'] = Df['CA124.DC124_Power'].astype(float)
    Df['CA125.DC125_Power'] = Df['CA125.DC125_Power'].astype(float)
    Df['CA126.DC126_Power'] = Df['CA126.DC126_Power'].astype(float)
    Df['CA127.DC127_Power'] = Df['CA127.DC127_Power'].astype(float)
    Df['CA128.DC128_Power'] = Df['CA128.DC128_Power'].astype(float)
    Df['CA129.CA_kW_FB'] = Df['CA129.CA_kW_FB'].astype(float)
    Df['CA130.CA_kW_FB'] = Df['CA130.CA_kW_FB'].astype(float)
    Df['CA131.CA_kW_FB'] = Df['CA131.CA_kW_FB'].astype(float)
    Df['CA132.CA_kW_FB'] = Df['CA132.CA_kW_FB'].astype(float)
    Df['Mean121_Power'] = Df['CA121.DC121_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean124_Power'] = Df['CA124.DC124_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean125_Power'] = Df['CA125.DC125_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean126_Power'] = Df['CA126.DC126_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean127_Power'] = Df['CA127.DC127_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean128_Power'] = Df['CA128.DC128_Power'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean129.CA_kW_FB'] = Df['CA129.CA_kW_FB'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean130.CA_kW_FB'] = Df['CA130.CA_kW_FB'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean131.CA_kW_FB'] = Df['CA131.CA_kW_FB'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Mean132.CA_kW_FB'] = Df['CA132.CA_kW_FB'].rolling(Config.loc[Config['Takt']==Takt, 'LithiationRollSize'].values[0]).mean()
    Df['Total Power'] =Df['Mean125_Power']+Df['Mean126_Power']+ Df['Mean127_Power']+ Df['Mean128_Power']+Df['Mean129.CA_kW_FB']+Df['Mean130.CA_kW_FB']+ Df['Mean131.CA_kW_FB']+ Df['Mean121_Power']+ Df['Mean124_Power']+ Df['Mean132.CA_kW_FB']
    Df['ActualCarrier'] = np.roll(Df['TRACKING_5.SP5_CarrierTracking_2'], Config.loc[Config['Takt']==Takt, 'SP5TrackingRoll'].values[0])
    Df['ActualCarrier'] = Df['ActualCarrier'].astype(float)
    Df2 = Df
    Df2['IsChanged2'] = (Df2['ActualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(hours=Config.loc[Config['Takt']==Takt, 'SP5timeexitH'].values[0], minutes=Config.loc[Config['Takt']==Takt, 'SP5timeexitM'].values[0], seconds=Config.loc[Config['Takt']==Takt, 'SP5timeexitS'].values[0])
    Df2 = Df2.sort_values('ActualStart')
    Df2 = Df2.drop_duplicates(subset='Metric', keep='first')
    Df3 = Df2.filter(['Mean121_Power', 'Mean124_Power', 'Mean125_Power', 'Mean126_Power', 'Mean127_Power',
                      'Mean128_Power', 'Mean129.CA_kW_FB', 'Mean130.CA_kW_FB', 'Mean131.CA_kW_FB','Mean132.CA_kW_FB', 'Total Power', 'ActualStart','ActualCarrier'])
    Df3 = Df3.sort_values('ActualStart')
    #export_csv = Df3.to_csv(r'C:\Users\arobinson\Desktop\SP524hr.csv', header=True)
    print(Df3.head())
    return Df3

def SP1Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP1550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP1550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP1_EXIT_Y1_550nm], [Tracking_1.SP1_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df.fillna(0, inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_1.SP1_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP1TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD1.SP1_EXIT_Y1_550nm'] = Df['OD1.SP1_EXIT_Y1_550nm'].astype(float)

    Df.loc[(Df['OD1.SP1_EXIT_Y1_550nm'] >= Lsl) & (Df['OD1.SP1_EXIT_Y1_550nm'] <= Usl) , 'Good record'] = 1
    Df.loc[(Df['OD1.SP1_EXIT_Y1_550nm'] <= Lsl) | (Df['OD1.SP1_EXIT_Y1_550nm'] >= Usl) , 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP1_550 meanY1'] = Df.groupby('lite Identifier')['OD1.SP1_EXIT_Y1_550nm'].transform('mean')
    Df['SP1_550 stdevY1'] = Df.groupby('lite Identifier')['OD1.SP1_EXIT_Y1_550nm'].transform('std')
    Df['SP1_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP1_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP1timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP1timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP1_550 Measurement Start Time', 'SP1_550 Measurement End Time', 'actualCarrier', 'SP1_550 meanY1', 'SP1_550 stdevY1', 'LiteCount', 'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df3.to_csv(r'C:\Users\arobinson\Desktop\SP1Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def SP1Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP1800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP1800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP1_EXIT_Y1_800nm], [Tracking_1.SP1_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_1.SP1_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP1TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD1.SP1_EXIT_Y1_550nm'] = Df['OD1.SP1_EXIT_Y1_800nm'].astype(float)
    Df.loc[(Df['OD1.SP1_EXIT_Y1_550nm'] >= Lsl) & (Df['OD1.SP1_EXIT_Y1_550nm'] <= Usl) , 'Good record'] = 1
    Df.loc[(Df['OD1.SP1_EXIT_Y1_550nm'] <= Lsl) | (Df['OD1.SP1_EXIT_Y1_550nm'] >= Usl) , 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP1_800 meanY1'] = Df.groupby('lite Identifier')['OD1.SP1_EXIT_Y1_550nm'].transform('mean')
    Df['SP1_800 stdevY1'] = Df.groupby('lite Identifier')['OD1.SP1_EXIT_Y1_550nm'].transform('std')
    Df['SP1_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP1_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP1timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP1timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP1timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP1_800 Measurement Start Time', 'SP1_800 Measurement End Time', 'actualCarrier','SP1_800 meanY1', 'SP1_800 stdevY1', 'LiteCount', 'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP7Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP1(row):
    ColumnList = ['SP1_550 meanY1']
    BadChannels = []
    ChannelHeightList = [3.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP12(row):
    ColumnList = ['SP1_800 meanY1']
    BadChannels = []
    ChannelHeightList = [3.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP1OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP1Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP1550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial = Trial.drop(['DateTime'], axis=1)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP1_550 OD Mean'] = FinalTable.apply(ODMeanRowSP1, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP1550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 1, 550)
    return FinalTable


def SP1OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP1Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP1800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial = Trial.drop(['DateTime'], axis=1)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP1_800 OD Mean'] = FinalTable.apply(ODMeanRowSP12, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP1800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 1, 800)
    return FinalTable


def SP2Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP2550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP2550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP2_EXIT_Y1_550nm], [OD1.SP2_EXIT_Y2_550nm], [OD1.SP2_EXIT_Y3_550nm], [OD1.SP2_EXIT_Y4_550nm], [OD1.SP2_EXIT_Y5_550nm], [OD1.SP2_EXIT_Y6_550nm], [OD1.SP2_EXIT_Y7_550nm], [Tracking_2.SP2_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_2.SP2_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP2TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP2_EXIT_Y1_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP2_EXIT_Y2_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD1.SP2_EXIT_Y3_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD1.SP2_EXIT_Y4_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD1.SP2_EXIT_Y5_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP2_EXIT_Y6_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP2_EXIT_Y7_550nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP2_550 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP2_550 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP2_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP2_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP2_550 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP2_550 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP2_550 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP2_550 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP2_550 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP2_550 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP2_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP2_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP2_550 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP2_550 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP2_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP2_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP2timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP2timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP2timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP2_550 Measurement Start Time', 'SP2_550 Measurement End Time', 'actualCarrier', 'SP2_550 meanY1', 'SP2_550 stdevY1', 'SP2_550 meanY2', 'SP2_550 stdevY2', 'SP2_550 meanY3', 'SP2_550 stdevY3', 'SP2_550 meanY4','SP2_550 stdevY4',  'SP2_550 meanY5', 'SP2_550 stdevY5', 'SP2_550 meanY6', 'SP2_550 stdevY6', 'SP2_550 meanY7', 'SP2_550 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    #export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv',header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP7Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def SP2Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP2800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP2800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
     [OD1.SP2_EXIT_Y1_800nm],[OD1.SP2_EXIT_Y2_800nm],[OD1.SP2_EXIT_Y3_800nm],[OD1.SP2_EXIT_Y4_800nm],[OD1.SP2_EXIT_Y5_800nm],[OD1.SP2_EXIT_Y6_800nm],[OD1.SP2_EXIT_Y7_800nm],[Tracking_2.SP2_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_2.SP2_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP2TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP2_EXIT_Y1_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP2_EXIT_Y2_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD1.SP2_EXIT_Y3_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD1.SP2_EXIT_Y4_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD1.SP2_EXIT_Y5_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP2_EXIT_Y6_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP2_EXIT_Y7_800nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP2_800 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP2_800 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP2_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP2_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP2_800 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP2_800 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP2_800 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP2_800 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP2_800 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP2_800 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP2_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP2_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP2_800 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP2_800 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP2_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP2_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP2timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP2timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP2timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP2_800 Measurement Start Time', 'SP2_800 Measurement End Time', 'actualCarrier', 'SP2_800 meanY1', 'SP2_800 stdevY1', 'SP2_800 meanY2', 'SP2_800 stdevY2', 'SP2_800 meanY3', 'SP2_800 stdevY3', 'SP2_800 meanY4','SP2_800 stdevY4',  'SP2_800 meanY5', 'SP2_800 stdevY5', 'SP2_800 meanY6', 'SP2_800 stdevY6', 'SP2_800 meanY7', 'SP2_800 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP7Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP2(row):
    ColumnList = ['SP2_550 meanY1', 'SP2_550 meanY2', 'SP2_550 meanY3', 'SP2_550 meanY4', 'SP2_550 meanY5', 'SP2_550 meanY6', 'SP2_550 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP22(row):
    ColumnList = ['SP2_800 meanY1', 'SP2_800 meanY2', 'SP2_800 meanY3', 'SP2_800 meanY4', 'SP2_800 meanY5', 'SP2_800 meanY6', 'SP2_800 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def SP2OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP2Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP2550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    #export_csv = Trial.to_csv(r'C:\Users\arobinson\Desktop\ODdasasasas.csv', header=True)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP2_550 OD Mean'] = FinalTable.apply(ODMeanRowSP2, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP2550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 2, 550)
    return FinalTable


def SP2OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP2Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP2800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP2_800 OD Mean'] = FinalTable.apply(ODMeanRowSP22, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP2800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 2, 800)
    return FinalTable


def SP3Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP3550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP3550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
     [OD1.SP3_EXIT_Y1_550nm],[OD1.SP3_EXIT_Y2_550nm],[OD1.SP3_EXIT_Y6_550nm],[OD1.SP3_EXIT_Y7_550nm],[Tracking_3.SP3_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_3.SP3_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP3TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP3_EXIT_Y1_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP3_EXIT_Y2_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP3_EXIT_Y6_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP3_EXIT_Y7_550nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP3_550 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP3_550 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP3_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP3_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP3_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP3_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP3_550 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP3_550 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP3_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP3_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP3timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP3timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP3timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(['DateTime', 'SP3_550 Measurement Start Time', 'SP3_550 Measurement End Time', 'actualCarrier', 'SP3_550 meanY1', 'SP3_550 stdevY1', 'SP3_550 meanY2', 'SP3_550 stdevY2', 'SP3_550 meanY6', 'SP3_550 stdevY6', 'SP3_550 meanY7', 'SP3_550 stdevY7', 'CarrierColumn', 'LiteCount',
                      'ActualStart'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP4Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def SP3Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP3800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP3800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP3_EXIT_Y1_800nm],[OD1.SP3_EXIT_Y2_800nm],[OD1.SP3_EXIT_Y6_800nm],[OD1.SP3_EXIT_Y7_800nm],[Tracking_3.SP3_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['Tracking_3.SP3_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP3TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP3_EXIT_Y1_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP3_EXIT_Y2_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP3_EXIT_Y6_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP3_EXIT_Y7_800nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP3_800 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP3_800 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP3_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP3_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP3_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP3_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP3_800 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP3_800 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP3_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP3_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP3timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP3timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP3timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(['DateTime', 'SP3_800 Measurement Start Time', 'SP3_800 Measurement End Time', 'actualCarrier', 'SP3_800 meanY1', 'SP3_800 stdevY1', 'SP3_800 meanY2', 'SP3_800 stdevY2', 'SP3_800 meanY6', 'SP3_800 stdevY6', 'SP3_800 meanY7', 'SP3_800 stdevY7', 'CarrierColumn', 'LiteCount',
                      'ActualStart'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP4Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP3(row):
    ColumnList = ['SP3_550 meanY1', 'SP3_550 meanY2', 'SP3_550 meanY6', 'SP3_550 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP32(row):
    ColumnList = ['SP3_800 meanY1', 'SP3_800 meanY2', 'SP3_800 meanY6', 'SP3_800 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP3OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP3Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP3550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP3_550 OD Mean'] = FinalTable.apply(ODMeanRowSP3, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP3550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 3, 550)
    return FinalTable


def SP3OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualFinish'].max()).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP3Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP3800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP3_800 OD Mean'] = FinalTable.apply(ODMeanRowSP32, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP3800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 3, 800)
    return FinalTable


def SP4Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP4550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP4550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP4_EXIT_Y1_550nm], [OD1.SP4_EXIT_Y2_550nm],[OD1.SP4_EXIT_Y3_550nm],[OD1.SP4_EXIT_Y5_550nm], [OD1.SP4_EXIT_Y6_550nm], [OD1.SP4_EXIT_Y7_550nm], [TRACKING_4.SP4_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_4.SP4_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP4TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP4_EXIT_Y1_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP4_EXIT_Y2_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD1.SP4_EXIT_Y3_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD1.SP4_EXIT_Y5_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP4_EXIT_Y6_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP4_EXIT_Y7_550nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP4_550 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP4_550 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP4_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP4_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP4_550 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP4_550 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP4_550 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP4_550 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP4_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP4_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP4_550 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP4_550 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP4_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP4_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP4timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP4timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP4timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP4_550 Measurement Start Time', 'SP4_550 Measurement End Time', 'actualCarrier', 'SP4_550 meanY1', 'SP4_550 stdevY1', 'SP4_550 meanY2', 'SP4_550 stdevY2', 'SP4_550 meanY3', 'SP4_550 stdevY3', 'SP4_550 meanY5', 'SP4_550 stdevY5', 'SP4_550 meanY6', 'SP4_550 stdevY6', 'SP4_550 meanY7', 'SP4_550 stdevY7', 'CarrierColumn',
         'LiteCount', 'ActualStart'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP4Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def SP4Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP4800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP4800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD1.SP4_EXIT_Y1_800nm],[OD1.SP4_EXIT_Y2_800nm],[OD1.SP4_EXIT_Y3_800nm],[OD1.SP4_EXIT_Y5_800nm],[OD1.SP4_EXIT_Y6_800nm],[OD1.SP4_EXIT_Y7_800nm],[TRACKING_4.SP4_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_4.SP4_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP4TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD1.SP4_EXIT_Y1_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD1.SP4_EXIT_Y2_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD1.SP4_EXIT_Y3_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD1.SP4_EXIT_Y5_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD1.SP4_EXIT_Y6_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD1.SP4_EXIT_Y7_800nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP4_800 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP4_800 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP4_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP4_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP4_800 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP4_800 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP4_800 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP4_800 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP4_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP4_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP4_800 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP4_800 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP4_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP4_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP4timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP4timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP4timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP4_800 Measurement Start Time', 'SP4_800 Measurement End Time', 'actualCarrier', 'SP4_800 meanY1', 'SP4_800 stdevY1', 'SP4_800 meanY2', 'SP4_800 stdevY2', 'SP4_800 meanY3', 'SP4_800 stdevY3', 'SP4_800 meanY5', 'SP4_800 stdevY5', 'SP4_800 meanY6', 'SP4_800 stdevY6', 'SP4_800 meanY7', 'SP4_800 stdevY7', 'CarrierColumn',
         'LiteCount', 'ActualStart'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP4Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP4(row):
    ColumnList = ['SP4_550 meanY1', 'SP4_550 meanY2', 'SP4_550 meanY3', 'SP4_550 meanY5', 'SP4_550 meanY6', 'SP4_550 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP42(row):
    ColumnList = ['SP4_800 meanY1', 'SP4_800 meanY2', 'SP4_800 meanY3', 'SP4_800 meanY5', 'SP4_800 meanY6', 'SP4_800 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP4OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP4Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart',  direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP4550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP4_550 OD Mean'] = FinalTable.apply(ODMeanRowSP4, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP4550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 4, 550)
    return FinalTable


def SP4OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP4Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart',  direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP4800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP4_800 OD Mean'] = FinalTable.apply(ODMeanRowSP42, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP4800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 4, 800)
    return FinalTable


def SP5Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP5550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP5550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD3_5.P01_CT1_SP5_Exit2_Y1_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y3_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y4_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y5_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y6_MarkerA],[OD3_5.P01_CT1_SP5_Exit2_Y7_MarkerA],[TRACKING_5.SP5_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_5.SP5_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP5TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y1_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerA'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP5_550 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP5_550 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP5_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP5_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP5_550 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP5_550 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP5_550 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP5_550 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP5_550 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP5_550 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP5_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP5_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP5_550 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP5_550 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP5_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP5_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP5timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP5timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP5timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP5_550 Measurement Start Time', 'SP5_550 Measurement End Time', 'actualCarrier', 'SP5_550 meanY1', 'SP5_550 stdevY1', 'SP5_550 meanY2', 'SP5_550 stdevY2', 'SP5_550 meanY3', 'SP5_550 stdevY3', 'SP5_550 meanY4','SP5_550 stdevY4',  'SP5_550 meanY5', 'SP5_550 stdevY5', 'SP5_550 meanY6', 'SP5_550 stdevY6', 'SP5_550 meanY7', 'SP5_550 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP5550nm.csv',header=True)
    # print(Df2.head())
    return Df3


def SP5Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP5800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP5800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF
    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD3_5.P01_CT1_SP5_Exit2_Y1_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y3_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y4_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y5_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y6_MarkerB],[OD3_5.P01_CT1_SP5_Exit2_Y7_MarkerB],[TRACKING_5.SP5_CarrierTracking_3]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_5.SP5_CarrierTracking_3'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP5TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y1_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD3_5.P01_CT1_SP5_Exit2_Y2_MarkerB'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP5_800 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP5_800 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP5_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP5_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP5_800 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP5_800 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP5_800 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP5_800 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP5_800 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP5_800 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP5_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP5_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP5_800 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP5_800 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP5_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP5_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP5timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP5timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP5timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP5_800 Measurement Start Time', 'SP5_800 Measurement End Time', 'actualCarrier', 'SP5_800 meanY1', 'SP5_800 stdevY1', 'SP5_800 meanY2', 'SP5_800 stdevY2', 'SP5_800 meanY3', 'SP5_800 stdevY3', 'SP5_800 meanY4','SP5_800 stdevY4',  'SP5_800 meanY5', 'SP5_800 stdevY5', 'SP5_800 meanY6', 'SP5_800 stdevY6', 'SP5_800 meanY7', 'SP5_800 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP624hr550nm.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP5(row):
    ColumnList = ['SP5_550 meanY1', 'SP5_550 meanY2', 'SP5_550 meanY3', 'SP5_550 meanY4', 'SP5_550 meanY5', 'SP5_550 meanY6', 'SP5_550 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def ODMeanRowSP52(row):
    ColumnList = ['SP5_800 meanY1', 'SP5_800 meanY2', 'SP5_800 meanY3', 'SP5_800 meanY4', 'SP5_800 meanY5', 'SP5_800 meanY6', 'SP5_800 meanY7']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP5OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP5Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP5550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP5_550 OD Mean'] = FinalTable.apply(ODMeanRowSP5, axis=1)
    drop_y(FinalTable)

    fil = str(Date) + " SP5550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 5, 550)
    return FinalTable


def SP5OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP5Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP5800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP5_800 OD Mean'] = FinalTable.apply(ODMeanRowSP52, axis=1)
    drop_y(FinalTable)

    fil = str(Date) + " SP5800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 5, 800)
    return FinalTable


def SP6Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP6550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP6550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF
    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD2.SP6_EXIT_Y2_550nm],[OD2.SP6_EXIT_Y3_550nm],[OD2.SP6_EXIT_Y4_550nm],[OD2.SP6_EXIT_Y5_550nm],[OD2.SP6_EXIT_Y6_550nm],[OD2.SP6_EXIT_Y7_550nm],[TRACKING_6.SP6_CarrierTracking_2]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_6.SP6_CarrierTracking_2'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP6TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD2.SP6_EXIT_Y2_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD2.SP6_EXIT_Y3_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD2.SP6_EXIT_Y4_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD2.SP6_EXIT_Y5_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD2.SP6_EXIT_Y6_550nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD2.SP6_EXIT_Y7_550nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP6_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP6_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP6_550 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP6_550 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP6_550 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP6_550 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP6_550 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP6_550 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP6_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP6_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP6_550 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP6_550 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP6_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP6_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    # DFE= Df.to_csv(r'C:\Users\arobinson\Desktop\SP6550nmSAVE.csv',header=True)
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP6timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP6timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP6timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP6_550 Measurement Start Time', 'SP6_550 Measurement End Time', 'actualCarrier', 'SP6_550 meanY2', 'SP6_550 stdevY2', 'SP6_550 meanY3', 'SP6_550 stdevY3', 'SP6_550 meanY4','SP6_550 stdevY4',  'SP6_550 meanY5', 'SP6_550 stdevY5', 'SP6_550 meanY6', 'SP6_550 stdevY6', 'SP6_550 meanY7', 'SP6_550 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP6550nm.csv',header=True)
    # print(Df2.head())
    return Df3


def SP6Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP6800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP6800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [OD2.SP6_EXIT_Y2_800nm],[OD2.SP6_EXIT_Y3_800nm],[OD2.SP6_EXIT_Y4_800nm],[OD2.SP6_EXIT_Y5_800nm],[OD2.SP6_EXIT_Y6_800nm],[OD2.SP6_EXIT_Y7_800nm],[TRACKING_6.SP6_CarrierTracking_2]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_6.SP6_CarrierTracking_2'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP6TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['OD2.SP6_EXIT_Y2_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['OD2.SP6_EXIT_Y3_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y4_800nm'] = Df['OD2.SP6_EXIT_Y4_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y5_800nm'] = Df['OD2.SP6_EXIT_Y5_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['OD2.SP6_EXIT_Y6_800nm'].astype(float)
    Df['OD2.SP6_EXIT_Y7_800nm'] = Df['OD2.SP6_EXIT_Y7_800nm'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y4_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y4_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y5_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y5_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y7_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y7_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y4_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y4_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y5_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y5_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y7_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y7_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP6_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP6_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP6_800 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP6_800 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP6_800 meanY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('mean')
    Df['SP6_800 stdevY4'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y4_800nm'].transform('std')
    Df['SP6_800 meanY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('mean')
    Df['SP6_800 stdevY5'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y5_800nm'].transform('std')
    Df['SP6_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP6_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP6_800 meanY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('mean')
    Df['SP6_800 stdevY7'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y7_800nm'].transform('std')
    Df['SP6_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP6_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP6timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP6timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP6timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = Df2['LiteCount'] + 1
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP6_800 Measurement Start Time', 'SP6_800 Measurement End Time', 'actualCarrier', 'SP6_800 meanY2', 'SP6_800 stdevY2', 'SP6_800 meanY3', 'SP6_800 stdevY3', 'SP6_800 meanY4','SP6_800 stdevY4',  'SP6_800 meanY5', 'SP6800 stdevY5', 'SP6_800 meanY6', 'SP6_800 stdevY6', 'SP6_800 meanY7', 'SP6_800 stdevY7', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP624hr550nm.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP6(row):
    ColumnList = ['SP6_550 meanY2', 'SP6_550 meanY3', 'SP6_550 meanY4', 'SP6_550 meanY5', 'SP6_550 meanY6', 'SP6_550 meanY7']
    BadChannels = []
    ChannelHeightList = [14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP62(row):
    ColumnList = ['SP6_800 meanY2', 'SP6_800 meanY3', 'SP6_800 meanY4', 'SP6_800 meanY5', 'SP6_800 meanY6', 'SP6_800 meanY7']
    BadChannels = []
    ChannelHeightList = [14.875, 25.875, 36.875, 47.875, 58.875, 69.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP6OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP6Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP6550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP6_550 OD Mean'] = FinalTable.apply(ODMeanRowSP6, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP6550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 6, 550)
    return FinalTable


def SP6OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP6Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP6800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP6_800 OD Mean'] = FinalTable.apply(ODMeanRowSP62, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP6800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 6, 800)
    return FinalTable


def SP7Pull550(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP7550HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP7550LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF

    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [PVD01_OD2_SP7.EXIT_Y1_MarkerA],[PVD01_OD2_SP7.EXIT_Y2_MarkerA],[PVD01_OD2_SP7.EXIT_Y3_MarkerA],[PVD01_OD2_SP7.EXIT_Y6_MarkerA],[TRACKING_7.SP7_CarrierTracking_2]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_7.SP7_CarrierTracking_2'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP7TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y1_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y2_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y3_MarkerA'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y6_MarkerA'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP7_550 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP7_550 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP7_550 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP7_550 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP7_550 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP7_550 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP7_550 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP7_550 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP7_550 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP7_550 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP7timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP7timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP7timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP7_550 Measurement Start Time', 'SP7_550 Measurement End Time', 'actualCarrier', 'SP7_550 meanY1', 'SP7_550 stdevY1', 'SP7_550 meanY2', 'SP7_550 stdevY2', 'SP7_550 meanY3', 'SP7_550 stdevY3', 'SP7_550 meanY6', 'SP7_550 stdevY6', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP7Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def SP7Pull800(startDate, EndDate, Config, Takt):
    Usl = Config.loc[Config['Takt'] == Takt, 'SP7800HighThreshold'].values[0]
    Lsl = Config.loc[Config['Takt'] == Takt, 'SP7800LowThreshold'].values[0]
    print(str(startDate))
    print(str(EndDate))
    Query = f"""SET QUOTED_IDENTIFIER OFF
    Select *
    FROM OPENQUERY(INSQL, "SELECT
    [PVD01_OD2_SP7.EXIT_Y1_MarkerB],[PVD01_OD2_SP7.EXIT_Y2_MarkerB],[PVD01_OD2_SP7.EXIT_Y3_MarkerB],[PVD01_OD2_SP7.EXIT_Y6_MarkerB],[TRACKING_7.SP7_CarrierTracking_2]
    ,StartDateTime
    ,DateTime
    FROM WideHistory
    WHERE wwRetrievalMode = 'Cyclic'
    AND wwResolution = 1000

    AND DateTime >= '{startDate}'
    AND DateTime <='{EndDate}'")Hist
    """
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-WW-VM-HIST;"
                          "Trusted_Connection=yes;")
    Df = pd.read_sql_query(Query, cnxn)
    cnxn.close()
    Df.drop_duplicates(subset=['DateTime'], inplace=True)
    Df['actualCarrier'] = np.roll(Df['TRACKING_7.SP7_CarrierTracking_2'],
                                  int(Config.loc[Config['Takt'] == Takt, 'SP7TrackingRoll1sec'].values[0]))
    Df = Df.reset_index()
    Df['actualCarrier'] = Df['actualCarrier'].astype(float)
    Df['OD2.SP6_EXIT_Y1_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y1_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y2_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y2_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y3_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y3_MarkerB'].astype(float)
    Df['OD2.SP6_EXIT_Y6_800nm'] = Df['PVD01_OD2_SP7.EXIT_Y6_MarkerB'].astype(float)
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y1_800nm'] <= Usl) & (
                Df['OD2.SP6_EXIT_Y2_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y2_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y3_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y3_800nm'] <= Usl) & (
                       Df['OD2.SP6_EXIT_Y6_800nm'] >= Lsl) & (Df['OD2.SP6_EXIT_Y6_800nm'] <= Usl), 'Good record'] = 1
    Df.loc[(Df['OD2.SP6_EXIT_Y1_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y1_800nm'] >= Usl) | (
                Df['OD2.SP6_EXIT_Y2_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y2_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y3_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y3_800nm'] >= Usl) | (
                       Df['OD2.SP6_EXIT_Y6_800nm'] <= Lsl) | (Df['OD2.SP6_EXIT_Y6_800nm'] >= Usl), 'Good record'] = 0
    Df['IsChanged'] = (Df['Good record'].diff() != 0).astype(int)
    Df['lite Identifier'] = Df['IsChanged'].cumsum()
    Df['SP7_800 meanY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('mean')
    Df['SP7_800 stdevY1'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y1_800nm'].transform('std')
    Df['SP7_800 meanY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('mean')
    Df['SP7_800 stdevY2'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y2_800nm'].transform('std')
    Df['SP7_800 meanY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('mean')
    Df['SP7_800 stdevY3'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y3_800nm'].transform('std')
    Df['SP7_800 meanY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('mean')
    Df['SP7_800 stdevY6'] = Df.groupby('lite Identifier')['OD2.SP6_EXIT_Y6_800nm'].transform('std')
    Df['SP7_800 Measurement Start Time'] = Df.groupby('lite Identifier')['DateTime'].transform('min')
    Df['SP7_800 Measurement End Time'] = Df.groupby('lite Identifier')['DateTime'].transform('max')
    DF = Df.drop_duplicates(subset='lite Identifier', keep='first')
    Df2 = DF[(DF['Good record'] == 1)]
    Df2['IsChanged2'] = (Df2['actualCarrier'].diff() != 0).astype(int)
    Df2['Metric'] = Df2['IsChanged2'].cumsum()
    Df2['Approximate Intermediate'] = Df2.groupby('Metric')['DateTime'].transform('min')
    Df2['Approximate Intermediate'] = pd.to_datetime(Df2['Approximate Intermediate'])
    Df2['ActualStart'] = Df2['Approximate Intermediate'] - timedelta(
        hours=Config.loc[Config['Takt'] == Takt, 'SP7timeexitH'].values[0],
        minutes=Config.loc[Config['Takt'] == Takt, 'SP7timeexitM'].values[0],
        seconds=Config.loc[Config['Takt'] == Takt, 'SP7timeexitS'].values[0])
    Df2['IsChanged3'] = (Df2['actualCarrier'].diff() == 0).astype(int)
    Df2['LiteCount'] = Df2.groupby('ActualStart')['IsChanged3'].cumsum()
    Df2['CarrierColumn'] = (Df2['LiteCount'] + 1).astype(int)
    Df2 = Df2.sort_values('ActualStart')
    Df3 = Df2.filter(
        ['DateTime', 'SP7_800 Measurement Start Time', 'SP7_800 Measurement End Time', 'actualCarrier', 'SP7_800 meanY1', 'SP7_800 stdevY1', 'SP7_800 meanY2', 'SP7_800 stdevY2', 'SP7_800 meanY3', 'SP7_800 stdevY3', 'SP7_800 meanY6', 'SP7_800 stdevY6', 'LiteCount',
         'ActualStart', 'CarrierColumn'])
    Df3 = Df3.sort_values('ActualStart')
    # export_csv = Df.to_csv(r'C:\Users\arobinson\Desktop\ODdatasasasas.csv', header=True)  # Don't forget to add '.csv' at the end of the path
    # export_csv = Df2.to_csv(r'C:\Users\arobinson\Desktop\SP7Gen35.csv',header=True)
    # print(Df2.head())
    return Df3


def ODMeanRowSP7(row):
    ColumnList = ['SP7_550 meanY1', 'SP7_550 meanY2', 'SP7_550 meanY3', 'SP7_550 meanY6']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 58.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()

def ODMeanRowSP72(row):
    ColumnList = ['SP7_800 meanY1', 'SP7_800 meanY2', 'SP7_800 meanY3', 'SP7_800 meanY6']
    BadChannels = []
    ChannelHeightList = [3.875, 14.875, 25.875, 58.875]
    index = 0
    Beginningstage = True
    for height in ChannelHeightList:
        if Beginningstage:
            if row['CorrC1Y'] > height:
                BadChannels.append(ColumnList[index])
            else:
                Beginningstage = False
        else:
            if row['CorrC2Y'] < height:
                BadChannels.append(ColumnList[index])

        index += 1
    ColumnList = [c for c in ColumnList if c not in BadChannels]
    return row[ColumnList].mean()


def SP7OD550Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP7Pull550(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP7550CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP7_550 OD Mean'] = FinalTable.apply(ODMeanRowSP7, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP7550ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        [ 'ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable=FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 7, 550)
    return FinalTable


def SP7OD800Clean(Date, Config, Cro, LITES, LitePositions):
    CRO = Cro
    CRO['ActualStart'] = pd.to_datetime(CRO['ActualStart'])
    CRO['ActualFinish'] = pd.to_datetime(CRO['ActualFinish'])
    CRO = CRO.dropna(subset=['GateTimer'])
    items_counts = CRO['GateTimer'].value_counts()
    Takt = items_counts.idxmax()
    StartDate = (CRO['ActualStart'].min()).strftime("%m/%d/%Y %H:%M:%S")
    EndDate = (CRO['ActualStart'].max() + timedelta(hours=3)).strftime("%m/%d/%Y %H:%M:%S")
    LiteODS = SP7Pull800(StartDate, EndDate, Config, Takt)
    Trial = pd.merge_asof(LiteODS, CRO, on='ActualStart', direction='nearest', tolerance=pd.Timedelta('4m'))
    fil =str(Date) + " SP7800CARRIERODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    Trial.drop(['DateTime'], axis=1, inplace=True)
    #export_csv2 = Trial.to_csv(filename, header=True, index=False)
    Lites = LITES
    EndBo = Lites['BONumber'].max()
    StartBo = Lites['BONumber'].min()
    CORRpos = LitePositions
    LiteTable = Lites
    MergeList = ['Batch', 'BOSequence', 'CarrierColumn']
    FinalTable = LiteTable.merge(Trial, how='left', on=MergeList, suffixes=('', '_y'))
    FinalTable['CorrC1Y'] = FinalTable['CorrC1Y'].astype(float)
    FinalTable['CorrC2Y'] = FinalTable['CorrC2Y'].astype(float)
    FinalTable['SP7_800 OD Mean'] = FinalTable.apply(ODMeanRowSP72, axis=1)
    drop_y(FinalTable)
    fil = str(Date) + " SP7800ODTABLE.csv"
    filename = os.path.join(r'\\echromics\ob\General\PVD01\WonderTrace', fil)
    FinalTable = FinalTable.drop(
        ['ActualFinish', 'LiteCount'],
        axis=1)
    FinalTable = FinalTable.drop_duplicates(subset=['LiteID'])
    export_csv2 = FinalTable.to_csv(filename, header=True, index=False)
    UpdateODs(FinalTable, 7, 800)
    return FinalTable


def getMeAllTheNewRecords():
    start_time = time.time()
    Date=(datetime.now()).strftime("%Y-%m-%d %H_%M_%S")
    Config=pd.read_csv(r'\\echromics\ob\General\PVD01\Overwatch\CoaterToLite.csv')
    Config= Config.astype(float)
    Config['LithiationRollSize']=Config['LithiationRollSize'].astype(int)
    pull=supull(Config)
    if len(pull.index)>3:
        ShouldIGo=0
        items_counts = pull['GateTimer'].value_counts()
        Takt = items_counts.idxmax()
        if Takt==210:
            ShouldIGo=1
        elif Takt == 300:
            ShouldIGo = 1
        elif Takt == 3.5:
            ShouldIGo = 1
        elif Takt == 5:
            ShouldIGo = 1
        elif Takt == 240:
            ShouldIGo = 1
        elif Takt == 4:
            ShouldIGo = 1
        if ShouldIGo == 1:
            superpull=Croull(Config,pull)
            pulltime=(time.time() - start_time)
            Cro = superpull[0][0]
            Cro2 = Cro
            UpdateCro(Cro2, 2)
            UpdateTimes(Cro2)
            SP1 = superpull[0][1]
            SP2 = superpull[0][2]
            SP3 = superpull[0][3]
            SP4 = superpull[0][4]
            SP5 = superpull[0][5]
            SP6 = superpull[0][6]
            SP7 = superpull[0][7]
            ODSET = superpull[0][8]
            HTO = superpull[0][9]
            print("Cro Length is "+str(len(Cro.index)))
            if len(Cro.index) > 9:
                print("Enough Carriers")
                print(str(ShouldIGo))

                StartDate = (Cro['ActualStart'].min()).strftime("%m-%d-%Y %H:%M:%S")
                EndDate = (Cro['ActualStart'].max() + timedelta(hours=3)).strftime("%m-%d-%Y %H:%M:%S")
                LITES=superpull[1]
                LITES2=superpull[2]
                FromHTOtoITORs(LITES2)
                # if len(SP1.index) > 2:
                #     HowdthecarriersdoSP1(SP1, Date, Config)
                # if len(SP2.index) > 2:
                #     HowdthecarriersdoSP2(SP2, Date, Config)
                # if len(SP3.index) > 2:
                #     HowdthecarriersdoSP3(SP3, Date, Config)
                # if len(SP4.index) > 2:
                #     HowdthecarriersdoSP4(SP4, Date, Config)
                # if len(SP5.index) > 2:
                #     HowdthecarriersdoSP5(SP5, Date, Config)
                # if len(SP6.index) > 2:
                #     HowdthecarriersdoSP6(SP6, Date, Config)
                # if len(SP7.index) > 2:
                #     HowdthecarriersdoSP7(SP7, Date, Config)
                # if len(HTO.index) > 2:
                #     Result = HowdthecarriersdoHTO(HTO, Date, Config)

                if len(LITES2.index) > 5 and len(ODSET.index>5):
                     print("Enough lites")
                #      UpdateLITES(LITES, 2)
                #      Result = HowdthecarriersdoPYRO(ODSET, Date, Config)
                #      EndBo = LITES['BONumber'].max()
                #      StartBo = LITES['BONumber'].min()
                #      LitePositions = LitePos(StartBo, EndBo)
                # #    list1=['LiteID', 'Batch', 'BOSequence', 'LiteIdentity', 'CarrierColumn', 'LiteLoc', 'Testplan', 'Carrier LayOut', 'SP1 550 OD Mean']
                #      SP1550 = SP1OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP1550 = SP1550.filter(list1)
                # #     #list2 = ['LiteID', 'SP1 800 OD Mean']
                #      SP1800 = SP1OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP1800 = SP1800.filter(list2)
                # #     #list3 = ['LiteID', 'SP2 550 OD Mean']
                #      SP2550 = SP2OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP2550 = SP2550.filter(list3)
                # #     #list4 = ['LiteID', 'SP2 800 OD Mean']
                #      SP2800 = SP2OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP2800 = SP2800.filter(list4)
                # #     #list5 = ['LiteID', 'SP3 550 OD Mean']
                #      SP3550 = SP3OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP3550 = SP3550.filter(list5)
                # #     #list6 = ['LiteID', 'SP3 800 OD Mean']
                #      SP3800 = SP3OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP3800 = SP3800.filter(list6)
                # #     #list7 = ['LiteID', 'SP4 550 OD Mean']
                #      SP4550 = SP4OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP4550 = SP4550.filter(list7)
                # #     #list8 = ['LiteID', 'SP4 800 OD Mean']
                #      SP4800 = SP4OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP4800 = SP4800.filter(list8)
                # #     #list9 = ['LiteID', 'SP5 550 OD Mean']
                #      SP5550 = SP5OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP5550 = SP5550.filter(list9)
                # #     #list10 = ['LiteID', 'SP5 800 OD Mean']
                #      SP5800 = SP5OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP5800 = SP5800.filter(list10)
                # #     #list11 = ['LiteID', 'SP6 550 OD Mean']
                #      SP6550 = SP6OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP6550 = SP6550.filter(list11)
                # #     #list12 = ['LiteID', 'SP6 800 OD Mean']
                #      SP6800 = SP6OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP6800 = SP6800.filter(list12)
                # #     #list13 = ['LiteID', 'SP7 550 OD Mean']
                #      SP7550 = SP7OD550Clean(Date, Config, ODSET, LITES, LitePositions)
                # #     #SP7550 = SP7550.filter(list13)
                # #     #list14 = ['LiteID', 'SP7 800 OD Mean']
                #      SP7800 = SP7OD800Clean(Date, Config, ODSET, LITES, LitePositions)
                #     #SP7800 = SP7800.filter(list14)
                #     #DfList = [SP1800,SP2550,SP2800,SP3550,SP3800,SP4550,SP4800,SP5550,SP5800,SP6550,SP6800,SP7550,SP7800]
                #     #ODTable = SP1550
                #     #for x in DfList:
                #      #   ODTable = pd.merge(ODTable, x, on='LiteID')
                #     #fil = str(Date) + " ODTABLE.csv"
                #     #filename = os.path.join(r'C:\Users\arobinson\Desktop', fil)
                #     #export_csv2 = ODTable.to_csv(filename, header=True)
                #     #expectedtrain = list['BatchNumber'].max()

                # if len(LITES2.index) > 5:
                #     FromHTOtoITORs(LITES2)


    EndTime=(time.time() - start_time)
    print("--- %s seconds ---" % (time.time() - start_time))
    #return [EndTime,Batch,StartDate,EndDate,pulltime]
    WonderTraceupdate(0,0)
    return EndTime
#CroPull(-54,-54)
#LitePull(-54,-54)
def keepmeintheloop():
    while True:
        processTime=getMeAllTheNewRecords()
        timedelt=1800-processTime
        if timedelt>0:
            time.sleep(1800-processTime)

def senderror(x):
    emaillist = ["Aaron.Robinson@view.com"]
    subject = "Error from WonderTrace"
    contents = x
    print("I'm Trying to send it")
    o365_auth = ('PVD01@view.com', 'J&Ss8ITc')
    mailserver = smtplib.SMTP('view-com.mail.protection.outlook.com', 25)
    mailserver.set_debuglevel(1)
    print("smtp succesesful")
    # mailserver.ehlo()
    #mailserver.login('PVD01@view.com', 'J&Ss8ITc')
    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = 'PVD01@view.com'
    msg['To'] = emaillist
    msg.set_content(contents)
    mailserver.send_message(msg)
    mailserver.quit()
    print("I sent it")

# def carriercheck(DF):
#     Results = list(zip(Names, Values))
#     Statuses = pd.DataFrame(Results, columns=['Tag', 'Actual Value'])
#     updateRecipe(FileScraper(r'\\echromics\ob\General\Coater-1 Testplans\Current Recipe.csv'))
#     Recipe = pd.read_csv(r'\\echromics\ob\General\PVD01\Overwatch\CheckRecipeAutoUpdate.csv')
#     Recipe.loc[(Recipe['Tag'] == 'CA043.MFC0434_FlowRate') & (Recipe['Recipe Value'] == 0), 'Recipe Value'] = 6
#     Recipe.loc[(Recipe['Tag'] == 'CA045.MFC0457_FlowRate') & (Recipe['Recipe Value'] == 0), 'Recipe Value'] = 8
#     Recipe.loc[(Recipe['Tag'] == 'CA046.MFC0466_FlowRate') & (Recipe['Recipe Value'] == 0), 'Recipe Value'] = 10
#     Recipe.loc[(Recipe['Tag'] == 'CA083.MF_Power') & (Recipe['Recipe Value'] == 0), 'Recipe Value'] = .3
#     # Statuses.to_csv(r'C:\Users\arobinson\Desktop\Tags4.csv', index=None, header=True)0
#     Report_mid = pd.merge(Statuses, Recipe, on=['Tag'], how='inner')
#     Report_mid['Delta'] = Report_mid['Actual Value'] - Report_mid['Recipe Value']
#     Report = Report_mid.loc[abs(Report_mid['Delta']) > .3]

#SendGapEmail2()
while True:
 try : keepmeintheloop()
 except Exception as e :
     senderror(str(e))
     WonderTraceupdate(0,1)
     time.sleep(900)
     pass
 else : break
#keepmeintheloop()
#getMeAllTheNewRecords()
