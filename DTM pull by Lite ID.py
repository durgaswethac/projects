import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
from re import search
import timeit

start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Transmission"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT [ID], [Server_ID], [ReadTime], [SerialNumber], [PeakIntensity],
       [PeakIntensityWavelength], [a*], [b*], [L*], [Position_X], [Position_Y],
       [StoreSpectra], [a*_Corrected], [b*_Corrected], [L*_Corrected],
       [SensorTemp], [LocationNum], [LiteID], [ProbeType], [SpectraID],
       [RackTemp], [TECAmps], [OpticTemp], [IntegrationTime],
       [Wavelengths], [Transmission] FROM [fct].[Data] WHERE Server_ID = 4 order by readtime desc """
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query), con=conn)
string = """Grey 2-1"""

y= df.loc[df['LiteID']==string]
print(len(y))

y.to_csv("DTM "+string+".csv")


stop = timeit.default_timer()
print(stop - start)
