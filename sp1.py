import time
import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import numpy as np
from datetime import datetime, timedelta
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-WW-VM-HIST"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
[TRACKING_1.SP1_CarrierTracking_5] as SP1_Carr_Tr_5, [EC01_SP1_1200.BONumber] as SP1_BONum_1200, 
[CA041.AC041_MF_Power] as CA41_Power, [CA042.AC042_MF_Power] as CA42_Power, [CA043.AC043_MF_Power] as CA43_Power, 
[CA044.AC044_MF_Power] as CA44_Power, [CA045.AC045_MF_Power] as CA45_Power, [CA046.AC046_MF_Power] as CA46_Power, 
[CA047.AC047_MF_Power] as CA47_Power, [CA048.AC048_MF_Power] as CA48_Power, [RCEX1.CA1PowerFeedback] as CAEX1_Power, 
[RCEX1.CA2PowerFeedback] as CAEX2_Power, [RCEX1.CA3PowerFeedback] as CAEX3_Power, [COMMON_1.SP1_TransferSpeedAct] as 
SP1_LS, [PVD01_SP2_EN_Y4.Carrier_ID] as InSitu_CarrID, [PVD01_SP2_EN_Y4.C_RXpos] as InSitu_XPos, 
[PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value] as InSitu_Avg, [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] as 
InSitu_ChAPkWave FROM OPENQUERY(INSQL,"SELECT DateTime = convert(nvarchar, DateTime, 21), 
[TRACKING_1.SP1_CarrierTracking_5], [EC01_SP1_1200.BONumber], [CA041.AC041_MF_Power], [CA042.AC042_MF_Power], 
[CA043.AC043_MF_Power], [CA044.AC044_MF_Power], [CA045.AC045_MF_Power], [CA046.AC046_MF_Power], 
[CA047.AC047_MF_Power], [CA048.AC048_MF_Power], [RCEX1.CA1PowerFeedback], [RCEX1.CA2PowerFeedback], 
[RCEX1.CA3PowerFeedback], [COMMON_1.SP1_TransferSpeedAct], [PVD01_SP2_EN_Y4.Carrier_ID], [PVD01_SP2_EN_Y4.C_RXpos], 
[PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value], [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] FROM WideHistory WHERE 
wwRetrievalMode = 'cyclic' AND wwResolution = 1000 AND DateTime > CONVERT(nvarchar, Dateadd(dd, -1, GetDate())) order 
by DateTime");"""

with engine.begin() as conn:
    df_1_datapull = pd.read_sql_query(sql=text(strHistQuery), con=conn)
BOshift_power = df_1_datapull['SP1_BONum_1200'].shift(147)
df_1_datapull['SP1_Power BO Shift'] = BOshift_power

df_1_datapull = df_1_datapull.loc[:, ['InSitu_ChAPkWave', 'SP1_Power BO Shift']]
Botracking_gb = df_1_datapull.groupby(['SP1_Power BO Shift'], sort=False).mean()
Botracking_gb.to_csv('df_2_datapull2.csv')

stop = timeit.default_timer()
print(stop-start)