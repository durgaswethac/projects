import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit

start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Transmission"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT t1.ID, t1.[Factory_ID], t1.[FactoryLine_ID], t1.[Server_ID], 
	t1.VGST_DID, t1.VGST_RID, t1.VGST_SID, t1.[ReadTime], 
	t1.[SerialNumber], t1.[PeakIntensity], t1.[PeakIntensityWavelength], t1.[a*], 
	t1.[b*], t1.[L*], t1.[Position_X], t1.[Position_Y], 
	t1.[StoreSpectra], t1.[a*_Corrected], t1.[b*_Corrected], t1.[L*_Corrected], 
	t1.[SensorTemp], t1.[LocationNum], t1.[BONumber], t1.[ProbeType], 
	t1.[SpectraID], t1.[RackTemp], t1.[TECAmps], t1.[OpticTemp], 
	t1.[IntegrationTime], t1.[CollectedTimeUtc], t1.[Wavelengths], t1.[Transmission], 
	t1.[PLC_xpos], t1.[PLC_ypos], t1.[Timestamp] 
	FROM [fct].[Data]  t1;"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query), con=conn)

df.to_csv('Static Transmission Head Data Pulls.csv')
stop = timeit.default_timer()
print(stop - start)
