import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-LSTN-PRDSQL;DATABASE=WIPL1"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT [ID],[LiteID],[MeasurementNumber],[MeasurementPosition],[FilmResistivity],[FilmTemperature]
      ,[DateTime] ,[RSProbe],[ScanID],[Z] FROM [WIPL1].[dbo].[RSProbeData] WHERE DateTime > CONVERT(nvarchar, Dateadd(dd, -60, GetDate())) order by 
DateTime;"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query),con=conn)

df.to_csv('ITO_Rs.csv')

stop = timeit.default_timer()
print(stop-start)