import pandas as pd
import pyodbc
import numpy as np


def pullConfig():
    Query = f"""SELECT TOP(165) *    FROM [tst].[Coater_SPC_Info] t1    WHERE  (  (  ( [ControlZone] IN  ( 'Config' ) 
    )  )  )     ORDER BY [ID] DESC;"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "UID=reportinguser;""PWD=SQLR3ports;"
                          "DATABASE=Coatertrace;")
    df = pd.read_sql_query(Query, cnxn)
    df.to_csv('df.csv')


pullConfig()


def pullhistquery():
    strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
    [TRACKING_1.SP1_CarrierTracking_2] as SP1_Carr_Tr_2, [TRACKING_1.SP1_CarrierTracking_5] as SP1_Carr_Tr_5, 
    [EC01_SP1_1170.BONumber] as SP1_BONum_1170, [EC01_SP1_1200.BONumber] as SP1_BONum_1200, [CA041.AC041_MF_Power] as 
    CA41_Power, [CA042.AC042_MF_Power] as CA42_Power, [CA043.AC043_MF_Power] as CA43_Power, [CA044.AC044_MF_Power] as 
    CA44_Power, [CA045.AC045_MF_Power] as CA45_Power, [CA046.AC046_MF_Power] as CA46_Power, [CA047.AC047_MF_Power] as 
    CA47_Power, [CA048.AC048_MF_Power] as CA48_Power, [RCEX1.CA1PowerFeedback] as CAEX1_Power, 
    [RCEX1.CA2PowerFeedback] as CAEX2_Power, [RCEX1.CA3PowerFeedback] as CAEX3_Power, [PY.SP1_ENTRY_Y1] as 
    Y1_SP1EntPyro, [PY.SP1_ENTRY_Y2] as Y2_SP1EntPyro, [PY.SP1_ENTRY_Y3] as Y3_SP1EntPyro, [PY.SP1_ENTRY_Y4] as 
    Y4_SP1EntPyro, [PY.SP1_ENTRY_Y5] as Y5_SP1EntPyro, [PY.SP1_ENTRY_Y6] as Y6_SP1EntPyro, [PY.SP1_ENTRY_Y7] as 
    Y7_SP1EntPyro, [PY.SP1_EXIT_Y1] as Y1_SP1ExitPyro, [PY.SP1_EXIT_Y2] as Y2_SP1ExitPyro, [PY.SP1_EXIT_Y3] as 
    Y3_SP1ExitPyro, [PY.SP1_EXIT_Y4] as Y4_SP1ExitPyro, [PY.SP1_EXIT_Y5] as Y5_SP1ExitPyro, [PY.SP1_EXIT_Y6] as 
    Y6_SP1ExitPyro, [PY.SP1_EXIT_Y7] as Y7_SP1ExitPyro, [PVD01_OD1_SP1.EXIT_Y1_MarkerA] as Y1_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y2_MarkerA] as Y2_SP1OD, [PVD01_OD1_SP1.EXIT_Y3_MarkerA] as Y3_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y4_MarkerA] as Y4_SP1OD, [PVD01_OD1_SP1.EXIT_Y5_MarkerA] as Y5_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y6_MarkerA] as Y6_SP1OD, [PVD01_OD1_SP1.EXIT_Y7_MarkerA] as Y7_SP1OD, 
    [COMMON_1.SP1_TransferSpeedAct] as SP1_LS, [PVD01_SP2_EN_Y4.Carrier_ID] as InSitu_CarrID, 
    [PVD01_SP2_EN_Y4.BO_Number] as InSitu_BONum, [PVD01_SP2_EN_Y4.C_RXpos] as InSitu_XPos, 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value] as InSitu_Avg, [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] as 
    InSitu_ChAPkWave FROM OPENQUERY(INSQL,"SELECT DateTime = convert(nvarchar, DateTime, 21), 
    [TRACKING_1.SP1_CarrierTracking_2], [TRACKING_1.SP1_CarrierTracking_5], [EC01_SP1_1170.BONumber], 
    [EC01_SP1_1200.BONumber], [CA041.AC041_MF_Power], [CA042.AC042_MF_Power], [CA043.AC043_MF_Power], 
    [CA044.AC044_MF_Power], [CA045.AC045_MF_Power], [CA046.AC046_MF_Power], [CA047.AC047_MF_Power], 
    [CA048.AC048_MF_Power], [RCEX1.CA1PowerFeedback], [RCEX1.CA2PowerFeedback], [RCEX1.CA3PowerFeedback], 
    [PY.SP1_ENTRY_Y1], [PY.SP1_ENTRY_Y2], [PY.SP1_ENTRY_Y3], [PY.SP1_ENTRY_Y4], [PY.SP1_ENTRY_Y5], [PY.SP1_ENTRY_Y6], 
    [PY.SP1_ENTRY_Y7], [PY.SP1_EXIT_Y1], [PY.SP1_EXIT_Y2], [PY.SP1_EXIT_Y3], [PY.SP1_EXIT_Y4], [PY.SP1_EXIT_Y5], 
    [PY.SP1_EXIT_Y6], [PY.SP1_EXIT_Y7], [PVD01_OD1_SP1.EXIT_Y1_MarkerA], [PVD01_OD1_SP1.EXIT_Y2_MarkerA], 
    [PVD01_OD1_SP1.EXIT_Y3_MarkerA], [PVD01_OD1_SP1.EXIT_Y4_MarkerA], [PVD01_OD1_SP1.EXIT_Y5_MarkerA], 
    [PVD01_OD1_SP1.EXIT_Y6_MarkerA], [PVD01_OD1_SP1.EXIT_Y7_MarkerA], [COMMON_1.SP1_TransferSpeedAct], 
    [PVD01_SP2_EN_Y4.Carrier_ID], [PVD01_SP2_EN_Y4.BO_Number], [PVD01_SP2_EN_Y4.C_RXpos], 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value], [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] FROM WideHistory 
    WHERE wwRetrievalMode = 'Cyclic' AND wwResolution = 1000 AND DateTime > CONVERT(nvarchar, Dateadd(mi, -18, 
    GetDate())) order by DateTime");"""
    dt_raw = pyodbc.connect("DSN=OB-WW-VM-HIST;SERVER=OB-WW-VM-HIST;UID=wwuser123;PWD=Password123;")
    df_1 = pd.read_sql_query(strHistQuery, dt_raw)

    entry_pyro = df_1.loc[:,
                 ['Y1_SP1EntPyro', 'Y2_SP1EntPyro', 'Y3_SP1EntPyro', 'Y4_SP1EntPyro', 'Y5_SP1EntPyro',
                  'Y6_SP1EntPyro', 'Y7_SP1EntPyro']]
    pyro_entry = ['Y1_SP1EntPyro', 'Y2_SP1EntPyro', 'Y3_SP1EntPyro', 'Y4_SP1EntPyro', 'Y5_SP1EntPyro',
                  'Y6_SP1EntPyro', 'Y7_SP1EntPyro']
    meanpyro = entry_pyro.mean()
    pyroentrysum = 0
    pyroentryactive = []
    pyroentry_values = []
    for i in pyro_entry:
        if 150 < entry_pyro.iloc[1][i] < 290:
            pyroentryactive.append(i)
            pyroentrysum += entry_pyro.iloc[1][i]
            pyroentry_values.append(entry_pyro.iloc[-2][i])
    pyroentry_std = np.std(pyroentry_values, ddof=1)
    pyrodata = pd.DataFrame()
    pyrodata = pd.concat([pyrodata, entry_pyro], axis=1)
    pyrodata.loc['active sensors'] = pyroentryactive
    pyrodata['Mean Entry Pyro'] = meanpyro
    pyrodata.to_csv('Pyroentry data.csv')
    print(pyrodata.columns)


pullhistquery()
