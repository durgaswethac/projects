import time
import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()

def RTMPull():
    connection_string = "DRIVER={SQL Server};SERVER=OB-WW-VM-HIST"
    connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

    engine = db.create_engine(connection_url)
    strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
    [PVD01_SP1_EN_Y4.RTM_Alarm_Wrd] as SP1_RTM_Alarm, [PVD01_SP2_EN_Y4.RTM_Alarm_Wrd] as SP2_RTM_Alarm, 
    [PVD01_SP3_MD_Y4.RTM_Alarm_Wrd] as SP3_RTM_Alarm, [PVD01_SP4_EN_Y4.RTM_Alarm_Wrd] as SP4_EN_RTM_Alarm, 
    [PVD01_SP4_EX_Y4.RTM_Alarm_Wrd] as SP4_EX_RTM_Alarm, [PVD01_SP6_EN_Y4.RTM_Alarm_Wrd] as SP6_EN_RTM_Alarm , 
    [PVD01_SP6_EX_Y4.RTM_Alarm_Wrd] as SP6_EX_RTM_Alarm , [PVD01_SP7_EX_Y4.RTM_Alarm_Wrd] as SP7_EX_RTM_Alarm, 
    [PVD01_SP1_EN_Y4.RTM_Status_Wrd] as SP1_Status,[PVD01_SP2_EN_Y4.RTM_Status_Wrd] as SP2_Status, 
    [PVD01_SP3_MD_Y4.RTM_Status_Wrd] as SP3_MD_RTM_Status, [PVD01_SP4_EN_Y4.RTM_Status_Wrd] as SP4_EN_RTM_Status, 
    [PVD01_SP4_EX_Y4.RTM_Status_Wrd] as SP4_EX_RTM_Status, [PVD01_SP6_EN_Y4.RTM_Status_Wrd] as SP6_EN_RTM_Status, 
    [PVD01_SP6_EX_Y4.RTM_Status_Wrd] as SP6_EX_RTM_Status, [PVD01_SP7_EX_Y4.RTM_Status_Wrd] as SP7_EX_RTM_Status FROM 
    OPENQUERY(INSQL,"SELECT DateTime = convert(nvarchar, DateTime, 21), [PVD01_SP1_EN_Y4.RTM_Alarm_Wrd], 
    [PVD01_SP2_EN_Y4.RTM_Alarm_Wrd], [PVD01_SP3_MD_Y4.RTM_Alarm_Wrd], [PVD01_SP4_EN_Y4.RTM_Alarm_Wrd],
    [PVD01_SP4_EX_Y4.RTM_Alarm_Wrd], [PVD01_SP6_EN_Y4.RTM_Alarm_Wrd],[PVD01_SP6_EX_Y4.RTM_Alarm_Wrd], 
    [PVD01_SP7_EX_Y4.RTM_Alarm_Wrd], [PVD01_SP1_EN_Y4.RTM_Status_Wrd],[PVD01_SP2_EN_Y4.RTM_Status_Wrd], 
    [PVD01_SP3_MD_Y4.RTM_Status_Wrd], [PVD01_SP4_EN_Y4.RTM_Status_Wrd], [PVD01_SP4_EX_Y4.RTM_Status_Wrd], 
    [PVD01_SP6_EN_Y4.RTM_Status_Wrd], [PVD01_SP6_EX_Y4.RTM_Status_Wrd], [PVD01_SP7_EX_Y4.RTM_Status_Wrd] FROM 
    WideHistory WHERE wwRetrievalMode = 'cyclic' AND wwResolution = 1000 AND DateTime > CONVERT( nvarchar, 
    Dateadd(mm, -3, GetDate())) order by DateTime");"""
    with engine.begin() as conn:
        df = pd.read_sql_query(sql=text(strHistQuery),con=conn)
    f=df[-1:]
    print(f)
# def senderror(x):
#     emaillist = ["durga.chivukula@view.com"]
#     subject = "TCM disabled"
#     contents = x
#     print("I'm Trying to send it")
#     o365_auth = ('PVD01@view.com', 'J&Ss8ITc')
#     mailserver = smtplib.SMTP('view-com.mail.protection.outlook.com', 25)
#     mailserver.set_debuglevel(1)
#     print("smtp succesesful")
#     msg = EmailMessage()
#     msg['Subject'] = subject
#     msg['From'] = 'PVD01@view.com'
#     msg['To'] = emaillist
#     msg.set_content(contents)
#     mailserver.send_message(msg)
#     mailserver.quit()
#     print("I sent it")
#
# value = RTMPull()
# if value == 'Alarm':
#     senderror('TCM has been disabled')
RTMPull()
stop = timeit.default_timer()
print(stop - start)