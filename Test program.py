import pandas as pd

SP = pd.read_excel('Test Tables.xlsx')
A5 = 0
A2 = 0
SP['A2'] = ''
SP['A5'] = ''
SP['Alarm'] = ''
SP['New Power'] = ''
for i in range(2, len(SP),3):
    if abs(SP.iloc[i]['Del_T2']) > 8.67:
        alarm = 1
        if SP.iloc[i]['Del_T2'] < 0:
            if ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']) < -0.5:
                if (SP.iloc[i]['Carrier Power'] + 0.5) > 88:
                    New_power = 88
                else:
                    New_power = SP.iloc[i]['Carrier Power'] + 0.5
            elif -0.5 < ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']):
                if (SP.iloc[i]['Carrier Power'] - (
                        (SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff'])) > 88:
                    New_power = 88
                else:
                    New_power = SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff'])
        elif SP.iloc[i]['Del_T2'] > 0:
            if ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']) > 0.5:
                if (SP.iloc[i]['Carrier Power'] - 0.5) > 50:
                    New_power = 50
                else:
                    New_power = SP.iloc[i]['Carrier Power'] - 0.5
            elif ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']) < 0.5:
                if (SP.iloc[i]['Carrier Power'] - (
                        (SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff'])) < 50:
                    New_power = 50
                else:
                    New_power = SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff'])
    elif abs(SP.iloc[i]['Del_T2']) > 5:
        if SP.iloc[i]['Del_T2'] < 0:
            A5 += -1
        elif SP.iloc[i]['Del_T2'] > 0:
            A5 += 1
        if abs(A5) > 2:
            alarm = 5
            if SP.iloc[i]['Del_T2'] < 0:
                if ((SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff']) < -0.5:
                    if (SP.iloc[i]['Carrier Power'] + 0.5) > 88:
                        New_power = 88
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] + 0.5
                elif -0.5 < ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']):
                    if (SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff'])) > 88:
                        New_power = 88
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - (
                                (SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff'])
            elif SP.iloc[i]['Del_T2'] > 0:
                if ((SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff']) > 0.5:
                    if (SP.iloc[i]['Carrier Power'] - 0.5) < 50:
                        New_power = 50
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - 0.5
                elif -0.5 < ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']):
                    if (SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff'])) < 50:
                        New_power = 50
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - (
                                (SP.iloc[i]['Del_T2'] * 0.75) / SP.iloc[i]['T2/Power Coeff'])
        else:
            alarm = 0
            New_power = 0
    elif SP.iloc[i]['Del_T2'] != 0:
        if SP.iloc[i]['Del_T2'] > 0:
            A2 += 1
        else:
            A2 += -1
        if abs(A2) > 7:
            alarm = 2
            if SP.iloc[i]['Del_T2'] < 0:
                if ((SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff']) < -0.5:
                    if (SP.iloc[i]['Carrier Power'] + 0.5) > 88:
                        New_power = 88
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] + 0.5
                elif -0.5 < ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']):
                    if (SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff'])) > 88:
                        New_power = 88
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - (
                                (SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff'])
            elif SP.iloc[i]['Del_T2'] > 0:
                if ((SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff']) > 0.5:
                    if (SP.iloc[i]['Carrier Power'] - 0.5) < 50:
                        New_power = 50
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - 0.5
                elif ((SP.iloc[i]['Del_T2'] * 0.5) / SP.iloc[i]['T2/Power Coeff']) < 0.5:
                    if (SP.iloc[i]['Carrier Power'] - (
                            (SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff'])) < 50:
                        New_power = 50
                    else:
                        New_power = SP.iloc[i]['Carrier Power'] - (
                                (SP.iloc[i]['Del_T2']) / SP.iloc[i]['T2/Power Coeff'])
        else:
            alarm = 0
            New_power = 0
    else:
        alarm = 0
        New_power = 0
    if SP.iloc[i]['Carrier Power'] == New_power:
        A5 = 0
        A2 = 0
    print("A2 is equal to "+str(A2))
    print("A5 is equal to " + str(A5))
    SP.iloc[i, SP.columns.get_loc('A2')] = A2
    SP.iloc[i, SP.columns.get_loc('A5')] = A5
    SP.iloc[i, SP.columns.get_loc('Alarm')] = alarm
    SP.iloc[i, SP.columns.get_loc('New Power')] = New_power
SP.to_csv('Truth Table.csv')