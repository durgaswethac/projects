import pandas as pd
import numpy as np
from datetime import datetime
from tabulate import tabulate
import time
import smtplib
from email.message import EmailMessage
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit

start = timeit.default_timer()


def Heartbeat():
    connection_string = "DRIVER={SQL Server};SERVER=OB-WW-VM-HIST"
    connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

    engine = db.create_engine(connection_url)
    strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
    [IG01_Pos1067.POS1067_TCMSystemEnable] as TCMEnable FROM OPENQUERY(INSQL,"SELECT DateTime = 
    convert(nvarchar, DateTime, 21),[IG01_Pos1067.POS1067_TCMSystemEnable]  FROM WideHistory WHERE wwRetrievalMode = 'cyclic' AND wwResolution 
    = 60000 AND DateTime > CONVERT(nvarchar, Dateadd(mi, -1, GetDate())) order by DateTime");"""
# 60000 represents 1 min gap
    with engine.begin() as conn:
        df = pd.read_sql_query(sql=text(strHistQuery), con=conn)
    f = df.iloc[-1,1]
    a='Alarm'
    if f ==0:
        return a

def senderror(x):
    emaillist = ["durga.chivukula@view.com"]
    subject = "TCM disabled"
    contents = x
    print("I'm Trying to send it")
    o365_auth = ('PVD01@view.com', 'J&Ss8ITc')
    mailserver = smtplib.SMTP('view-com.mail.protection.outlook.com', 25)
    mailserver.set_debuglevel(1)
    print("smtp succesesful")
    msg = EmailMessage()
    msg['Subject'] = subject
    msg['From'] = 'PVD01@view.com'
    msg['To'] = emaillist
    msg.set_content(contents)
    mailserver.send_message(msg)
    mailserver.quit()
    print("I sent it")

value = Heartbeat()
if value == 'Alarm':
    senderror('TCM has been disabled')

stop = timeit.default_timer()
print(stop - start)
