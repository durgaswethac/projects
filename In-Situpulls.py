import pyodbc
import pandas as pd


def pullthickness():
    Query = f"""SELECT *    FROM RTM.[VGIS_Data]
	WHERE ReadTime > CONVERT(nvarchar, Dateadd(mi, -1, GetDate())) order by ReadTime;"""

    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW03;"
                          "UID=reportinguser;""PWD=SQLR3ports;"
                          "DATABASE=Reflective;")
    df = pd.read_sql_query(Query, cnxn)
    df.to_csv('thickness-pull.csv')

pullthickness()