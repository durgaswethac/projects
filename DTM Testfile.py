import timeit

import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL

start = timeit.default_timer()


def CheckIG():
    connection_string = "DRIVER={SQL Server}; SERVER=OB-WS-DTM1\SQLEXPRESS"
    connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})
    engine = db.create_engine(connection_url)
    Query = """SELECT * FROM [fct].[Data] WHERE Server_ID = 4 AND ReadTime"""
    with engine.begin() as conn:
        df = pd.read_sql_query(sql=text(Query), con=conn)

    df.to_csv('DTM Test.csv')


CheckIG()
stop = timeit.default_timer()
print(stop - start)
