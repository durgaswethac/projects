import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Reflective;UID=reportinguser;PWD=SQLR3ports"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})

engine = db.create_engine(connection_url)
Query = """SELECT BONumber, ReadTime, L5_Thickness_Avg, L7_Thickness_Avg,L8_Thickness_Avg FROM RTM.[VGIS_ThicknessSummary] WHERE ReadTime > CONVERT(nvarchar, Dateadd(mm, -3, GetDate())) 
order by ReadTime"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query),con=conn)

df.to_csv('Thickness Parameter.csv')

stop = timeit.default_timer()
print(stop-start)