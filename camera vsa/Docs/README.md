# VSACamera_VideoCapture_Python

## Description
Python 3.10 scripts used to monitor stripping band camera at VSA1.

## Release Notes
* Version 1.0
	* supports avi and mp4 video exports.
	* supports python 3.10
	* Uses Opencv-python for camera and video control.
	* Untested for different fps and resolutions other than 360p 260fps
	* config to control video retention time along with video length and even video file type.
	* can support USB or IP camera
	* setting blank resolution in config results in 360p at 260fps
	* supports file logging with dated folder structure.
	* logs can be set to adjust minimum log type via config. Supported types are "DEBUG", "INFO", "WARN", "ERROR"

