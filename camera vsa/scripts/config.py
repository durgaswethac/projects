import json
import os

class DictToObjConverter(object):
    def __init__(self, _dict):
        for key in _dict:
            setattr(self, key, _dict)

class JSONParser:
    
    def Read(self, file_path):
        try:    
            with open(file_path) as file:
                _json = json.load(file)
                print(_json)
                return _json
        except Exception as ex:
            print(ex)
            return None

    def Write(self, file_path, _dict):
        try:
            if not os.path.exists(file_path):
                with open(file_path, "x") as file:
                    print(f"created file: {file_path}")
            
            # write to file
            with open(file_path, "w") as file:
                file.write(json.dumps(_dict, indent=4))

            # validate file
            return os.path.exists(file_path)
        except Exception as ex:
            print(ex)
            return False

    def ConvertFromDictToObj(self, _dict):
        try:
            return DictToObjConverter(_dict)
        except Exception as ex:
            return None
            

        

    
