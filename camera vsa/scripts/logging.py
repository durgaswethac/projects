import os
import datetime

MIN_LOG_TYPE = {
    "DEBUG": 0,
    "INFO": 1,
    "WARN": 2,
    "ERROR": 3
}

class Logger:
    def __init__(self, _MinLogType, file_name, dir_path = "../logs"):
        self.MinLogType = MIN_LOG_TYPE[_MinLogType]
        self.DirPath = dir_path
        self.Filename = file_name
        self.MinLogTypeStr = _MinLogType

    def Debug(self, message):
        self.Write(message, 0, "DEBUG")

    def Info(self, message):
        self.Write(message, 1, "INFO")

    def Warn(self, message):
        self.Write(message, 2, "WARN")

    def Error(self, message):
        self.Write(message, 3, "ERROR")

    def Write(self, message, log_type_int, log_type):
        if self.MinLogType <= log_type_int:
            #create date string and path
            now = datetime.datetime.now()
            now_str = now.strftime("%Y-%m-%d %H:%M:%S")
            dated_dir_str = now.strftime("%Y-%m-%d")
            dated_dir = f"{self.DirPath}/{dated_dir_str}"

            #Validate directory is created if not create it.
            if not os.path.exists(dated_dir):
                os.mkdir(dated_dir)
                
            if os.path.exists(dated_dir):
                full_path = f"{dated_dir}/{self.Filename}"
                new_message = f"{now_str} ~ {log_type} ~ {message} \n"

                print(new_message)
                with open(full_path, "a") as file:
                    file.write(new_message)
