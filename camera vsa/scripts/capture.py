import numpy as np
import os
import cv2

class VideoCapture:
    def __init__(self, filename, FPS, Camera, Resolution = ""):
        self.filename = filename
        self.FPS = FPS
        self.Resolution = Resolution
        self.Camera = Camera

        self.STD_DIMENSIONS = {
            "480p": (640, 480),
            "720p": (1280, 720),
            "1080p": (1920, 1080),
            "4k": (3840, 2160)
        }

        self.ENCODING = {
            "mp4": "XVID",
            "avi": "XVID" #or MJPG
        }

        self.Video = cv2.VideoCapture(self.Camera)
        self.Video.set(cv2.CAP_PROP_POS_FRAMES, self.FPS)

        #Set size
        self.setSize()

        #Set file writer
        self.createFileWriter()

        #Verify camera is not being accessed        
        if self.Video.isOpened() == False:
            print("Error reading video file")


    def Capture(self):
        ret, frame = self.Video.read()
        return frame

    def WriteToFile(self, frame):
        if not self.FileWriter.isOpened():
            self.createFileWriter()
        self.FileWriter.write(frame)

    def setSize(self, res = ""):
        if res in self.STD_DIMENSIONS:
            self.size = self.STD_DIMENSIONS[res]
        else:
            self.size = (int(self.Video.get(3)), int(self.Video.get(4)))
        print(f"Size set to frame: {self.size}")

    def createFileWriter(self):
        #*"MJPG" works for avi but not mp4 XVID worked for mp4 and avi
        _encode = self.setEncoding(self.filename)
        self.FileWriter = cv2.VideoWriter(self.filename, cv2.VideoWriter_fourcc(*f"{_encode}"), 10, self.size)
        print(f"created file writer: set filename: {self.filename} and size: {self.size}")

    def setEncoding(self, filename):
        file_ext = filename.split(".")[-1]
        if file_ext in self.ENCODING:
            return self.ENCODING[file_ext]
        else:
            return "XVID"
        
