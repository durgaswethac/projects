import numpy as np
import os
import shutil
import cv2
from capture import VideoCapture
import datetime
from logging import Logger
from config import JSONParser

"""
local_storage_dir = "../Camera1"
#network_storage_dir = "//OB-VM-FileMover/VSA Stripping Band Video/Camera_1"
network_storage_dir = "//echromics/ob/Engineering/Personnel_Folders/Jason Hodges/CameraTesting"
filename = '../tmp.avi'
fps = 260.0
camera = 1
res = ""
minutes_per_video = 1
camera_name = "Stripping_Band_Camera1"
retainment_hours = 1
min_log_type = "DEBUG"
"""

# definitions

def setup_config(file_path):
    try:
        parser = JSONParser()
        # check if config file exists
        if not os.path.exists(file_path):
            
            # if not, create default values and export to file
            default_values = set_default_config_values()

            if default_values != None:
                parser.Write(file_path, default_values)
            
        # import config
        _dict = parser.Read(file_path)
        config = parser.ConvertFromDictToObj(_dict)

        return _dict
        
    except:
        return None


def set_default_config_values():
    try:
        _dict = {
            "local_storage_dir": "../Camera1",
            "network_storage_dir": "//echromics/ob/Engineering/Personnel_Folders/Jason Hodges/CameraTesting",
            "filename": "../tmp.avi",
            "fps": 260.0,
            "camera": 1,
            "res": "",
            "minutes_per_video": 1,
            "camera_name": "Stripping_Band_Camera1",
            "retainment_hours": 1,
            "min_log_type": "DEBUG"
        }
        return _dict
    except:
        return None

def get_time_lapse_minutes(now, last_write):
    try:
        time_lapsed = now - last_write
        return time_lapsed.seconds / 60
    except Exception as ex:
        logger.Error(f"Unable to get time lapse from: {now} to {last_write} with exception: {ex}")
        return None

def get_datetimestamp_hours(now, _hours):
    try:
        return now - datetime.timedelta(hours=_hours)
    except Exception as ex:
        logger.Error(f"Unable to get datetimestamp minus {_hours} hours with Exception: {ex}")
        return None

def create_new_filename(camera_name, now, filename):
    try:
        date_time_str = now.strftime("%Y-%m-%d_%H-%M")
        file_ext = filename.split(".")[-1]
        return f"{camera_name}_{date_time_str}.{file_ext}"
    except Exception as ex:
        logger.Error(f"Unable to create new filename for camera: {camera_name} with Exception: {ex}")
        return None

def move_file(source_file_path, dest_file_path):
    try:
        logger.Info(f"source file path: {source_file_path}")
        logger.Info(f"destination file path: {dest_file_path}")
        
        shutil.move(source_file_path, dest_file_path)
        return os.path.exists(dest_file_path)
    except Exception as ex:
        logger.Error(f"Unable to move file from: {source_file_path} to {dest_file_path} with Exception: {ex}")
        return False

def copy_file(source_file_path, dest_file_path):
    try:
        logger.Info(f"source file path: {source_file_path}")
        logger.Info(f"destination file path: {dest_file_path}")
        
        shutil.copy(source_file_path, dest_file_path)
        return os.path.exists(dest_file_path)
    except Exception as ex:
        logger.Error(f"Unable to copy file from: {source_file_path} to: {dest_file_path} with Exception: {ex}")
        return False

def find_files_in_folder(directory, file_ext):
    try:
        logger.Debug(f"Looking for files with ext: {file_ext} in directory: {directory}")
        files = []
        for file in os.listdir(directory):
            if file.endswith(file_ext):
                files.append(f"{directory}/{file}")
        return files
    except Exception as ex:
        logger.Error(f"Unable to find files in directory: {directory} with file extension: {file_ext} with Exception: {ex}")
        return None

def check_for_older_files(files, hours_to_keep, now):
    try:
        old_files = []
        oldest_retainment_datetime = get_datetimestamp_hours(now, hours_to_keep)
        for file in files:
            file_datetime= datetime.datetime.fromtimestamp(os.path.getmtime(file))
            if file_datetime < oldest_retainment_datetime:
                logger.Info(f"found old file: {file} with datetime: {file_datetime}")
                old_files.append(file)
        return old_files
    except Exception as ex:
        logger.Error(f"Unable to check for older files with Exception: {ex}")
        return None

def delete_files(files):
    try:
        for file in files:    
            os.remove(file)
            logger.Info(f"deleted file: {file}")
    except Exception as ex:
        [logger.Error(f"Unable to delete files: {file} with Exception: {ex}") for file in files]
        return False



#import config
config_file = "../config/config.json"
config = setup_config(config_file)
if config == None:
    raise SystemExit

# Setup logging
logger = Logger(config["min_log_type"], "event.log")
logger.Debug("Program Started")

# Setup Video capture
vc = VideoCapture(config["filename"], config["fps"], config["camera"], config["res"])


last_write_time = datetime.datetime.now()
# loop
while(True):
    try:
        now = datetime.datetime.now()

        #Check for old files and delete outside of retainment hours
        local_storage_files = find_files_in_folder(config["local_storage_dir"], config["filename"].split(".")[-1])
        old_files = check_for_older_files(local_storage_files, config["retainment_hours"], now)
        delete_files(old_files)

        network_storage_files = find_files_in_folder(config["network_storage_dir"], config["filename"].split(".")[-1])
        old_files = check_for_older_files(network_storage_files, config["retainment_hours"], now)
        delete_files(old_files)
        
        # Check if timestamp has hit new video mark.
        minutes_lapsed = get_time_lapse_minutes(now, last_write_time)

        if minutes_lapsed != None:
            
            if minutes_lapsed > config["minutes_per_video"]:
                logger.Info(f"video time has lapsed: {minutes_lapsed} / {config['minutes_per_video']} minutes")
                # create new name for file
                new_file_name = create_new_filename(config["camera_name"], now, config["filename"])
                logger.Info(f"new file name: {new_file_name}")
                # move temp file to local storage and network storage
                if new_file_name != None:
                    vc.FileWriter.release()
                    logger.Info("The video was successfully saved")
                    local_file_path = f"{config['local_storage_dir']}/{new_file_name}"
                    validated_file_move = move_file(config["filename"], local_file_path)
                    logger.Info(f"Validated file move: {validated_file_move}")

                    if validated_file_move == True:
                        network_file_path = f"{config['network_storage_dir']}/{new_file_name}"
                        validated_file_copy = copy_file(local_file_path, network_file_path)
                        logger.Info(f"Validated file copy: {validated_file_copy}")

                    # set last_write_time to now
                    last_write_time = now
        
        frame = vc.Capture()
        vc.WriteToFile(frame)
        
        cv2.imshow("VSA Stripping Band Camera Feed", frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    except Exception as ex:
        logger.Error(f"general error occured during process loop with exeception: {ex}")
vc.Video.release()
vc.FileWriter.release()
cv2.destroyAllWindows()
logger.Warn(f"Program stopped")



