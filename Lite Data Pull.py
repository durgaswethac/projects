import pandas as pd
import sqlalchemy as db
from sqlalchemy import text
from sqlalchemy.engine import URL
import timeit
start = timeit.default_timer()
connection_string = "DRIVER={SQL Server};SERVER=OB-VM-SQLDW03;DATABASE=Reflective"
connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})
liteid = "32571`736`71`17`1"
engine = db.create_engine(connection_url)
Query = f"""SELECT [ID],[Server_ID],[Factory_ID],[FactoryLine_ID],[VGSUMID],[ReadTime],[LiteID],[SerialNumber],
[ProbeAngle],[PeakIntensity_Min],[PeakIntensity_Max],[PeakIntensity_Avg],[PeakIntensity_StDev],
[PeakIntensityWavelength_Min],[PeakIntensityWavelength_Max],[PeakIntensityWavelength_Avg],
[PeakIntensityWavelength_StDev],[a*_Min],[a*_Max],[a*_Avg],[a*_StDev],[b*_Min],[b*_Max],[b*_Avg],[b*_StDev],[L*_Min],
[L*_Max],[L*_Avg],[L*_StDev],[RefPeak],[RefPeakWL],[SpectraSize],[RefIntensity],[RefWavelengths],
[IntensityProcessed],[BO_Number],[Width],[Length],[Thickness],[L Min],[a Min],[b Min],[L Max],[a Max],[b Max],[L Min 10 deg],
[a Min 10 deg],[b Min 10 deg],[L Max 10 deg],[a Max 10 deg],[b Max 10 deg],[a*_Corrected_Min],[a*_Corrected_Max],[a*_Corrected_Avg],
[a*_Corrected_StDev],[a*_Corrected_Samples],[b*_Corrected_Min],[b*_Corrected_Max],[b*_Corrected_Avg],
[b*_Corrected_StDev],[b*_Corrected_Samples],[L*_Corrected_Min],[L*_Corrected_Max],[L*_Corrected_Avg],
[L*_Corrected_StDev],[L*_Corrected_Samples],[MaxDE],[DBTemp45],[DBTemp10],[VGRID],[SensorTemp],[TintState],[Shape],
[H1],[W1],[ProductGen],[ToolState] FROM [Reflective].[fct].[ReflectiveViewGlass_Summary] WHERE LiteID='{liteid}' 
order by ReadTime desc;"""
with engine.begin() as conn:
    df = pd.read_sql_query(sql=text(Query),con=conn)

df.to_csv('Lite Data pull.csv')

stop = timeit.default_timer()
print(stop-start)