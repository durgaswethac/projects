import pandas as pd
import pyodbc
import numpy as np


def pullConfig():
    Query = f"""SELECT TOP(165) *    FROM [tst].[Coater_SPC_Info] t1    WHERE  (  (  ( [ControlZone] IN  ( 'Config' ) 
    )  )  )     ORDER BY [ID] DESC;"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "UID=reportinguser;""PWD=SQLR3ports;"
                          "DATABASE=Coatertrace;")
    df = pd.read_sql_query(Query, cnxn)
    df.to_csv('df.csv')
    p = df.loc[164, 'Value']
    p=int(p)
    return p


g = pullConfig()


def SP1Controls():
    strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
    [TRACKING_1.SP1_CarrierTracking_2] as SP1_Carr_Tr_2, [TRACKING_1.SP1_CarrierTracking_5] as SP1_Carr_Tr_5, 
    [EC01_SP1_1170.BONumber] as SP1_BONum_1170, [EC01_SP1_1200.BONumber] as SP1_BONum_1200, [CA041.AC041_MF_Power] as 
    CA41_Power, [CA042.AC042_MF_Power] as CA42_Power, [CA043.AC043_MF_Power] as CA43_Power, [CA044.AC044_MF_Power] as 
    CA44_Power, [CA045.AC045_MF_Power] as CA45_Power, [CA046.AC046_MF_Power] as CA46_Power, [CA047.AC047_MF_Power] as 
    CA47_Power, [CA048.AC048_MF_Power] as CA48_Power, [RCEX1.CA1PowerFeedback] as CAEX1_Power, 
    [RCEX1.CA2PowerFeedback] as CAEX2_Power, [RCEX1.CA3PowerFeedback] as CAEX3_Power, [PY.SP1_ENTRY_Y1] as 
    Y1_SP1EntPyro, [PY.SP1_ENTRY_Y2] as Y2_SP1EntPyro, [PY.SP1_ENTRY_Y3] as Y3_SP1EntPyro, [PY.SP1_ENTRY_Y4] as 
    Y4_SP1EntPyro, [PY.SP1_ENTRY_Y5] as Y5_SP1EntPyro, [PY.SP1_ENTRY_Y6] as Y6_SP1EntPyro, [PY.SP1_ENTRY_Y7] as 
    Y7_SP1EntPyro, [PY.SP1_EXIT_Y1] as Y1_SP1ExitPyro, [PY.SP1_EXIT_Y2] as Y2_SP1ExitPyro, [PY.SP1_EXIT_Y3] as 
    Y3_SP1ExitPyro, [PY.SP1_EXIT_Y4] as Y4_SP1ExitPyro, [PY.SP1_EXIT_Y5] as Y5_SP1ExitPyro, [PY.SP1_EXIT_Y6] as 
    Y6_SP1ExitPyro, [PY.SP1_EXIT_Y7] as Y7_SP1ExitPyro, [PVD01_OD1_SP1.EXIT_Y1_MarkerA] as Y1_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y2_MarkerA] as Y2_SP1OD, [PVD01_OD1_SP1.EXIT_Y3_MarkerA] as Y3_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y4_MarkerA] as Y4_SP1OD, [PVD01_OD1_SP1.EXIT_Y5_MarkerA] as Y5_SP1OD, 
    [PVD01_OD1_SP1.EXIT_Y6_MarkerA] as Y6_SP1OD, [PVD01_OD1_SP1.EXIT_Y7_MarkerA] as Y7_SP1OD, 
    [COMMON_1.SP1_TransferSpeedAct] as SP1_LS, [PVD01_SP2_EN_Y4.Carrier_ID] as InSitu_CarrID, 
    [PVD01_SP2_EN_Y4.BO_Number] as InSitu_BONum, [PVD01_SP2_EN_Y4.C_RXpos] as InSitu_XPos, 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value] as InSitu_Avg, [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] as 
    InSitu_ChAPkWave FROM OPENQUERY(INSQL,"SELECT DateTime = convert(nvarchar, DateTime, 21), 
    [TRACKING_1.SP1_CarrierTracking_2], [TRACKING_1.SP1_CarrierTracking_5], [EC01_SP1_1170.BONumber], 
    [EC01_SP1_1200.BONumber], [CA041.AC041_MF_Power], [CA042.AC042_MF_Power], [CA043.AC043_MF_Power], 
    [CA044.AC044_MF_Power], [CA045.AC045_MF_Power], [CA046.AC046_MF_Power], [CA047.AC047_MF_Power], 
    [CA048.AC048_MF_Power], [RCEX1.CA1PowerFeedback], [RCEX1.CA2PowerFeedback], [RCEX1.CA3PowerFeedback], 
    [PY.SP1_ENTRY_Y1], [PY.SP1_ENTRY_Y2], [PY.SP1_ENTRY_Y3], [PY.SP1_ENTRY_Y4], [PY.SP1_ENTRY_Y5], [PY.SP1_ENTRY_Y6], 
    [PY.SP1_ENTRY_Y7], [PY.SP1_EXIT_Y1], [PY.SP1_EXIT_Y2], [PY.SP1_EXIT_Y3], [PY.SP1_EXIT_Y4], [PY.SP1_EXIT_Y5], 
    [PY.SP1_EXIT_Y6], [PY.SP1_EXIT_Y7], [PVD01_OD1_SP1.EXIT_Y1_MarkerA], [PVD01_OD1_SP1.EXIT_Y2_MarkerA], 
    [PVD01_OD1_SP1.EXIT_Y3_MarkerA], [PVD01_OD1_SP1.EXIT_Y4_MarkerA], [PVD01_OD1_SP1.EXIT_Y5_MarkerA], 
    [PVD01_OD1_SP1.EXIT_Y6_MarkerA], [PVD01_OD1_SP1.EXIT_Y7_MarkerA], [COMMON_1.SP1_TransferSpeedAct], 
    [PVD01_SP2_EN_Y4.Carrier_ID], [PVD01_SP2_EN_Y4.BO_Number], [PVD01_SP2_EN_Y4.C_RXpos], 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value], [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] FROM WideHistory 
    WHERE wwRetrievalMode = 'Full' AND wwResolution = 10 AND DateTime > CONVERT(nvarchar, Dateadd(mi, -6, 
    GetDate())) order by DateTime");"""
    dt_raw = pyodbc.connect("DSN=OB-WW-VM-HIST;SERVER=OB-WW-VM-HIST;UID=wwuser123;PWD=Password123;")
    df_1 = pd.read_sql_query(strHistQuery, dt_raw)
    df_1.to_csv('df_1.csv')
    df_1_gb = df_1.groupby(['InSitu_CarrID', 'InSitu_BONum']).mean()
    df_1_gb['Total Power'] = df_1_gb[['CA41_Power', 'CA42_Power', 'CA43_Power',
                                      'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                      'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']].sum(axis=1)
    df_1_gb['SP1 Entry Pyro'] = df_1_gb[
        ['Y1_SP1EntPyro', 'Y2_SP1EntPyro', 'Y3_SP1EntPyro', 'Y4_SP1EntPyro', 'Y5_SP1EntPyro',
         'Y6_SP1EntPyro', 'Y7_SP1EntPyro']].sum(axis=1)
    df_1_gb['SP1 Exit Pyro'] = df_1_gb[
        ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro', 'Y6_SP1ExitPyro',
         'Y7_SP1ExitPyro']].sum(axis=1)
    df_1_gb['SP1 OD'] = df_1_gb[
        ['Y1_SP1OD', 'Y2_SP1OD', 'Y3_SP1OD', 'Y4_SP1OD', 'Y5_SP1OD', 'Y6_SP1OD', 'Y7_SP1OD']].sum(axis=1)
    df_1_gb.rename(columns={'InSitu_ChAPkWave': 'T2'}, inplace=True)
    df_1_gb.to_csv('df_1_gb.csv')

    if len(df_1_gb) == 3:
        LS = df_1_gb.loc[:, ['SP1_LS']].tail(2)
        Trough2 = df_1_gb.loc[:, ['T2']].tail(2)
        cathodes = df_1_gb.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                                   'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                   'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']].tail(2)
        listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                         "CA47_Power",
                         "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]
        exit_pyro = df_1_gb.loc[:,
                    ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                     'Y6_SP1ExitPyro',
                     'Y7_SP1ExitPyro']].tail(2)

        pyro_exit = ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                     'Y6_SP1ExitPyro',
                     'Y7_SP1ExitPyro']

        activecathodes = 0
        total_power = 0
        for i in listofcolumns:
            if cathodes.iloc[1][i] > 20:
                activecathodes += 1
                total_power += cathodes.iloc[1][i]
        if activecathodes == 0:
            t2_power_coeff = 6.99
            carrier_power = 0
        else:
            Power_average = total_power / activecathodes
            t2_power_coeff = (activecathodes * 9.55) / 11
            carrier_power = 11 * Power_average / activecathodes
        pyroexitactive = []
        pyroexit_values = []
        pyroexitsum = 0
        for i in pyro_exit:
            if 180 < exit_pyro.iloc[1][i] < 320:
                pyroexitactive.append(i)
                pyroexitsum += exit_pyro.iloc[1][i]
                pyroexit_values.append(exit_pyro.iloc[-2][i])
        pyroexit_std = np.std(pyroexit_values, ddof=1)
        Pyroexit_active = ' '.join(pyroexitactive)
        p = ['SP1_LS']
        for i in p:
            if 600 < LS.iloc[1][i] < 750:
                Takttime = 300
            elif 900 < LS.iloc[1][i] < 1050:
                Takttime = 210
        z = ['T2']
        for i in z:
            t2 = Trough2.iloc[1][i]
        #     del_t2 = t2 - Target_T2
        df_1_gb.to_csv('df_1_gb.csv')
        SP1_data = pd.DataFrame()
        SP1_data = pd.concat([SP1_data, cathodes.head(1)], axis=1)
        SP1_data['Active Cathodes'] = activecathodes
        SP1_data['T2/Power Coeff'] = t2_power_coeff
        SP1_data['Carrier Power'] = carrier_power
        SP1_data = pd.concat([SP1_data, exit_pyro.head(1)], axis=1)
        SP1_data['Pyro Active Sensors'] = Pyroexit_active
        SP1_data['T2'] = t2
        SP1_data['Gen'] = g
        SP1_data['Takttime'] = Takttime
        SP1_data['Coater Status'] = 'Coater is running'
        # print(SP1_data.columns)
        SP1_data.to_csv('SP1_data.csv')
        return SP1_data
    elif len(df_1_gb) == 1:
        df_1_gb[df_1_gb.loc[:, ['SP1_BONum_1170']] == 0]
        Trough2 = df_1_gb.loc[:, ['T2']].tail(2)
        df_1_gb['Coater Gap'] = 'Coater is gaped'
        cathodes = df_1_gb.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                                   'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                   'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']]
        listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                         "CA47_Power",
                         "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]
        entry_pyro = df_1_gb.loc[:,
                     ['Y1_SP1EntPyro', 'Y2_SP1EntPyro', 'Y3_SP1EntPyro', 'Y4_SP1EntPyro', 'Y5_SP1EntPyro',
                      'Y6_SP1EntPyro', 'Y7_SP1EntPyro']]
        exit_pyro = df_1_gb.loc[:,
                    ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                     'Y6_SP1ExitPyro',
                     'Y7_SP1ExitPyro']]
        pyro_entry = ['Y1_SP1EntPyro', 'Y2_SP1EntPyro', 'Y3_SP1EntPyro', 'Y4_SP1EntPyro', 'Y5_SP1EntPyro',
                      'Y6_SP1EntPyro', 'Y7_SP1EntPyro']
        pyro_exit = ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                     'Y6_SP1ExitPyro',
                     'Y7_SP1ExitPyro']
        activecathodes = 0
        total_power = 0
        if activecathodes == 0:
            t2_power_coeff = 6.99
            carrier_power = 0
        Takttime = 330
        z = ['T2']
        for i in z:
            t2 = Trough2.iloc[0][i]
        Pyroexit_active = ' '.join(pyro_exit)
        SP1_data = pd.DataFrame()

        SP1_data = pd.concat([SP1_data, cathodes.head(1)], axis=1)
        SP1_data['Active Cathodes'] = activecathodes
        SP1_data['T2/Power Coeff'] = t2_power_coeff
        SP1_data['Carrier Power'] = carrier_power
        SP1_data = pd.concat([SP1_data, exit_pyro.head(1)], axis=1)
        SP1_data['Pyro Exit Sensors'] = Pyroexit_active
        SP1_data['T2'] = t2
        SP1_data['Gen'] = g
        SP1_data['Takttime'] = Takttime
        SP1_data['Caoter Status'] = df_1_gb['Coater Gap']
        df_1_gb.to_csv('df_1_gb.csv')
        SP1_data.to_csv('SP1_data.csv')
        return SP1_data


    elif len(df_1_gb) == 2:
        BONum = df_1_gb.loc[:, ['SP1_BONum_1200']].tail(2)
        x = ['SP1_BONum_1200']
        for i in x:
            if BONum.iloc[1][i] != 0:
                LS = df_1_gb.loc[:, ['SP1_LS']].tail(2)
                Trough2 = df_1_gb.loc[:, ['T2']].tail(2)
                cathodes = df_1_gb.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                                           'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                           'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']].tail(2)
                listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                                 "CA47_Power",
                                 "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]
                exit_pyro = df_1_gb.loc[:,
                            ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                             'Y6_SP1ExitPyro',
                             'Y7_SP1ExitPyro']].tail(2)

                pyro_exit = ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                             'Y6_SP1ExitPyro',
                             'Y7_SP1ExitPyro']

                activecathodes = 0
                total_power = 0
                for i in listofcolumns:
                    if cathodes.iloc[1][i] > 20:
                        activecathodes += 1
                        total_power += cathodes.iloc[1][i]
                    Power_average = total_power / activecathodes
                    t2_power_coeff = (activecathodes * 9.55) / 11
                    carrier_power = 11 * Power_average / activecathodes
                if activecathodes == 0:
                    t2_power_coeff = 6.99
                    carrier_power = 0

                pyroexitactive = []
                pyroexit_values = []
                pyroexitsum = 0
                for i in pyro_exit:
                    if 180 < exit_pyro.iloc[1][i] < 320:
                        pyroexitactive.append(i)
                        pyroexitsum += exit_pyro.iloc[1][i]
                        pyroexit_values.append(exit_pyro.iloc[-2][i])
                pyroexit_std = np.std(pyroexit_values, ddof=1)
                Pyroexit_active = ' '.join(pyroexitactive)
                p = ['SP1_LS']
                for i in p:
                    if 600 < LS.iloc[1][i] < 750:
                        Takttime = 300
                    elif 900 < LS.iloc[1][i] < 1050:
                        Takttime = 210
                z = ['T2']
                for i in z:
                    t2 = Trough2.iloc[1][i]
                # del_t2 = t2 - Target_T2
                df_1_gb.to_csv('df_1_gb.csv')
                SP1_data = pd.DataFrame()
                SP1_data = pd.concat([SP1_data, cathodes.head(1)], axis=1)
                SP1_data['Active Cathodes'] = activecathodes
                SP1_data['T2/Power Coeff'] = t2_power_coeff
                SP1_data['Carrier Power'] = carrier_power
                SP1_data = pd.concat([SP1_data, exit_pyro.head(1)], axis=1)
                SP1_data['Pyro Active Sensors'] = Pyroexit_active
                SP1_data['T2'] = t2
                SP1_data['Gen'] = g
                SP1_data['Takttime'] = Takttime
                SP1_data['Coater Status'] = 'Coater is running'
                # print(SP1_data.columns)
                SP1_data.to_csv('SP1_data.csv')
                return SP1_data
            elif BONum.iloc[2][i] != 0:
                LS = df_1_gb.loc[:, ['SP1_LS']].tail(2)
                Trough2 = df_1_gb.loc[:, ['T2']].tail(2)
                cathodes = df_1_gb.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                                           'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                           'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']].tail(2)
                listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                                 "CA47_Power",
                                 "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]
                exit_pyro = df_1_gb.loc[:,
                            ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                             'Y6_SP1ExitPyro',
                             'Y7_SP1ExitPyro']].tail(2)

                pyro_exit = ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                             'Y6_SP1ExitPyro',
                             'Y7_SP1ExitPyro']

                activecathodes = 0
                total_power = 0
                for i in listofcolumns:
                    if cathodes.iloc[1][i] > 20:
                        activecathodes += 1
                        total_power += cathodes.iloc[1][i]
                    Power_average = total_power / activecathodes
                    t2_power_coeff = (activecathodes * 9.55) / 11
                    carrier_power = 11 * Power_average / activecathodes
                if activecathodes == 0:
                    t2_power_coeff = 6.99
                    carrier_power = 0

                pyroexitactive = []
                pyroexit_values = []
                pyroexitsum = 0
                for i in pyro_exit:
                    if 180 < exit_pyro.iloc[1][i] < 320:
                        pyroexitactive.append(i)
                        pyroexitsum += exit_pyro.iloc[1][i]
                        pyroexit_values.append(exit_pyro.iloc[-2][i])
                pyroexit_std = np.std(pyroexit_values, ddof=1)
                Pyroexit_active = ' '.join(pyroexitactive)
                p = ['SP1_LS']
                for i in p:
                    if 600 < LS.iloc[1][i] < 750:
                        Takttime = 300
                    elif 900 < LS.iloc[1][i] < 1050:
                        Takttime = 210
                z = ['T2']
                for i in z:
                    t2 = Trough2.iloc[1][i]
                # del_t2 = t2 - Target_T2
                df_1_gb.to_csv('df_1_gb.csv')
                SP1_data = pd.DataFrame()
                SP1_data = pd.concat([SP1_data, cathodes.head(1)], axis=1)
                SP1_data['Active Cathodes'] = activecathodes
                SP1_data['T2/Power Coeff'] = t2_power_coeff
                SP1_data['Carrier Power'] = carrier_power
                SP1_data = pd.concat([SP1_data, exit_pyro.head(1)], axis=1)
                SP1_data['Pyro Active Sensors'] = Pyroexit_active
                SP1_data['T2'] = t2
                SP1_data['Gen'] = g
                SP1_data['Takttime'] = Takttime
                SP1_data['Coater Status'] = 'Coater is running'
                # print(SP1_data.columns)
                SP1_data.to_csv('SP1_data.csv')
                return SP1_data
        else:
            LS = df_1_gb.loc[:, ['SP1_LS']].tail(2)
            df_1_gb['Coater Gap'] = 'Coater is Starting Up'
            cathodes = df_1_gb.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                                       'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                                       'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']].tail(1)
            listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                             "CA47_Power", "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]
            exit_pyro = df_1_gb.loc[:,
                        ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                         'Y6_SP1ExitPyro', 'Y7_SP1ExitPyro']]
            pyro_exit = ['Y1_SP1ExitPyro', 'Y2_SP1ExitPyro', 'Y3_SP1ExitPyro', 'Y4_SP1ExitPyro', 'Y5_SP1ExitPyro',
                         'Y6_SP1ExitPyro', 'Y7_SP1ExitPyro']
            activecathodes = 0
            total_power = 0
            if activecathodes == 0:
                t2_power_coeff = 6.99
                carrier_power = 0
            z = ['T2']
            for i in z:
                t2 = Trough2.iloc[1][i]
            Takttime = 'NaN'
            Pyroexit_active = ' '.join(pyro_exit)
            SP1_data = pd.DataFrame()
            SP1_data = pd.concat([SP1_data, cathodes.head(1)], axis=1)
            SP1_data['Active Cathodes'] = activecathodes
            SP1_data['T2/Power Coeff'] = t2_power_coeff
            SP1_data['Carrier Power'] = carrier_power
            SP1_data = pd.concat([SP1_data, exit_pyro.tail(1)], axis=1)
            SP1_data['Pyro Exit Sensors'] = Pyroexit_active
            SP1_data['T2'] = t2
            SP1_data['Gen'] = g
            SP1_data['Takttime'] = Takttime
            SP1_data['Caoter Status'] = df_1_gb['Coater Gap']
            df_1_gb.to_csv('df_1_gb.csv')
            SP1_data.to_csv('SP1_data.csv')
            return SP1_data
