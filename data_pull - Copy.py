import pyodbc
import pandas as pd
import numpy as np
import timeit
start = timeit.default_timer()
def pullConfig():
    Query = f"""SELECT TOP(165) *    FROM [tst].[Coater_SPC_Info] t1    WHERE  (  (  ( [ControlZone] IN  ( 'Config' ) 
    )  )  )     ORDER BY [ID] DESC;"""
    cnxn = pyodbc.connect("Driver={SQL Server};"
                          "Server=OB-VM-SQLDW02;"
                          "UID=reportinguser;""PWD=SQLR3ports;"
                          "DATABASE=Coatertrace;")
    df = pd.read_sql_query(Query, cnxn)
    df.to_csv('df_datapull.csv')
    p = df.loc[164, 'Value']
    p = int(p)
    return p


g = pullConfig()

def SP1Controls():
    strHistQuery = f"""SET QUOTED_IDENTIFIER OFF SELECT CONVERT(VARCHAR(23),DateTime,121) as DateTimeToMatch, 
    [TRACKING_1.SP1_CarrierTracking_2] as SP1_Carr_Tr_2, [TRACKING_1.SP1_CarrierTracking_5] as SP1_Carr_Tr_5, 
    [EC01_SP1_1170.BONumber] as SP1_BONum_1170, [EC01_SP1_1200.BONumber] as SP1_BONum_1200, [CA041.AC041_MF_Power] as 
    CA41_Power, [CA042.AC042_MF_Power] as CA42_Power, [CA043.AC043_MF_Power] as CA43_Power, [CA044.AC044_MF_Power] as 
    CA44_Power, [CA045.AC045_MF_Power] as CA45_Power, [CA046.AC046_MF_Power] as CA46_Power, [CA047.AC047_MF_Power] as 
    CA47_Power, [CA048.AC048_MF_Power] as CA48_Power, [RCEX1.CA1PowerFeedback] as CAEX1_Power, 
    [RCEX1.CA2PowerFeedback] as CAEX2_Power, [RCEX1.CA3PowerFeedback] as CAEX3_Power, [PY.SP1_EXIT_Y1] as 
    Y1_SP1ExitPyro, [PY.SP1_EXIT_Y2] as Y2_SP1ExitPyro, [PY.SP1_EXIT_Y3] as Y3_SP1ExitPyro, [PY.SP1_EXIT_Y4] as 
    Y4_SP1ExitPyro, [PY.SP1_EXIT_Y5] as Y5_SP1ExitPyro, [PY.SP1_EXIT_Y6] as Y6_SP1ExitPyro, [PY.SP1_EXIT_Y7] as 
    Y7_SP1ExitPyro, [COMMON_1.SP1_TransferSpeedAct] as SP1_LS, [PVD01_SP2_EN_Y4.Carrier_ID] as InSitu_CarrID, 
    [PVD01_SP2_EN_Y4.C_RXpos] as InSitu_XPos, [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value] as InSitu_Avg, 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] as InSitu_ChAPkWave FROM OPENQUERY(INSQL,"SELECT DateTime = 
    convert(nvarchar, DateTime, 21), [TRACKING_1.SP1_CarrierTracking_2], [TRACKING_1.SP1_CarrierTracking_5], 
    [EC01_SP1_1170.BONumber], [EC01_SP1_1200.BONumber], [CA041.AC041_MF_Power], [CA042.AC042_MF_Power], 
    [CA043.AC043_MF_Power], [CA044.AC044_MF_Power], [CA045.AC045_MF_Power], [CA046.AC046_MF_Power], 
    [CA047.AC047_MF_Power], [CA048.AC048_MF_Power], [RCEX1.CA1PowerFeedback], [RCEX1.CA2PowerFeedback], 
    [RCEX1.CA3PowerFeedback], [PY.SP1_EXIT_Y1], [PY.SP1_EXIT_Y2], [PY.SP1_EXIT_Y3], [PY.SP1_EXIT_Y4], 
    [PY.SP1_EXIT_Y5], [PY.SP1_EXIT_Y6], [PY.SP1_EXIT_Y7], [COMMON_1.SP1_TransferSpeedAct], 
    [PVD01_SP2_EN_Y4.Carrier_ID], [PVD01_SP2_EN_Y4.C_RXpos], [PVD01_SP2_EN_Y4.Channel_A.Trans.Ave_Value], 
    [PVD01_SP2_EN_Y4.Channel_A.Trans.Pk_Wavelength] FROM WideHistory WHERE wwRetrievalMode = 'delta' AND wwResolution 
    = 1 AND DateTime > CONVERT(nvarchar, Dateadd(mm, -2, GetDate())) order by DateTime");"""
    dt_raw = pyodbc.connect("DSN=OB-WW-VM-HIST;SERVER=OB-WW-VM-HIST;UID=wwuser123;PWD=Password123;")
    df_1_datapull = pd.read_sql_query(strHistQuery, dt_raw)
    empty_gen = [g]
    for n in empty_gen:
        if n == 3:
            BOshift_power = df_1_datapull['SP1_BONum_1200'].shift(210)
            powershift = df_1_datapull['SP1_Carr_Tr_5'].shift(200)
            # for i in range(0,len)
        elif n == 4:
            BOshift_power = df_1_datapull['SP1_BONum_1200'].shift(147)
            powershift = df_1_datapull['SP1_Carr_Tr_5'].shift(140)

    Botracking = df_1_datapull.loc[:,['SP1_BONum_1200']]
    Botracking_gb = Botracking.groupby(['SP1_BONum_1200']).mean()

    Botracking_gb.to_csv('Bo shift.csv')
    df_1_datapull['SP1_Power BO Shift'] = BOshift_power
    df_1_datapull['SP1_Power Shift'] = powershift
    df_1_datapull.to_csv('df_1_datapull.csv')
    df_1_gb_datapull = df_1_datapull.groupby(['SP1_Power Shift'], sort=False).mean()
    # df_1_gb_datapull.sort_values(["DateTimeToMatch"])
    df_1_gb_datapull.to_csv('df_1_gb_datapull.csv')
    dummy = pd.read_csv('df_1_gb_datapull.csv')
    # print(dummy.columns)

    LS = dummy.loc[:, ['SP1_LS']]
    T2 = dummy.loc[:,['InSitu_ChAPkWave']]
    cathodes = dummy.loc[:, ['CA41_Power', 'CA42_Power', 'CA43_Power',
                             'CA44_Power', 'CA45_Power', 'CA46_Power', 'CA47_Power', 'CA48_Power',
                             'CAEX1_Power', 'CAEX2_Power', 'CAEX3_Power']]
    listofcolumns = ["CA41_Power", "CA42_Power", "CA43_Power", "CA44_Power", "CA45_Power", "CA46_Power",
                     "CA47_Power",
                     "CA48_Power", "CAEX1_Power", "CAEX2_Power", "CAEX3_Power"]

    active_cathodes = []

    for j in range(0, len(cathodes)):
        activecathodes = 0
        for i in listofcolumns:
            if cathodes.iloc[j][i] > 20:
                activecathodes += 1
        active_cathodes.append(activecathodes)
    Total_Power = cathodes.sum(axis=1)
    cathodes['Active Cathodes'] = pd.Series(active_cathodes)
    power_average = np.where(cathodes['Active Cathodes'] > 0, Total_Power / cathodes['Active Cathodes'], 0)
    cathodes['T2 Power Coeff'] = np.where(cathodes['Active Cathodes'] > 0, (cathodes['Active Cathodes'] * 9.55) / 11,
                                          6.99)
    cathodes['Carrier Power'] = np.where(cathodes['Active Cathodes'] > 0,
                                         (power_average * 11) / cathodes['Active Cathodes'], 0)
    Takttime = []
    p = ['SP1_LS']
    for l in p:
        for i in range(0, len(LS)):
            if 600 < LS.iloc[i][l] < 750:
                takttime = 300
                Takttime.append(takttime)
            elif 900 < LS.iloc[i][l] < 1050:
                takttime = 210
                Takttime.append(takttime)
            else:
                takttime = 330
                Takttime.append(takttime)
    q = ['InSitu_ChAPkWave']
    Del_T2 = []
    for i in q:
        for l in range(0,len(T2)):
            if g == 3:
                del_t2 = T2.iloc[l][i] - 577
                Del_T2.append(del_t2)
            elif g == 4:
                del_t2 = T2.iloc[l][i] - 590
                Del_T2.append(del_t2)
    Dataset = pd.DataFrame()
    Dataset = pd.concat([Dataset, dummy['SP1_Power Shift']], axis=1)
    Dataset = pd.concat([Dataset, cathodes], axis=1)
    Dataset['Takttime'] = pd.Series(Takttime)
    Dataset['Del_T2'] = pd.Series(Del_T2)

    Dataset = Dataset.tail(8)
    Dataset_1 = Dataset.head(3)
    Dataset.to_csv('Pradeep.csv')
    return Dataset_1

SP1Controls()
stop = timeit.default_timer()
print(stop-start)